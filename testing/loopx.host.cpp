#include "cuda.h"

#include <iostream>
#include <string>

#define cudaCheck(result) cudaCheckFunc((result), __FILE__, __LINE__)

void cudaCheckFunc(CUresult result, std::string fname, unsigned line) {
    if (result != CUDA_SUCCESS) {
        const char* name;
        const char* message;

        std::cerr << fname << '(' << line << "): ";

        cuGetErrorName(result, &name);
        cuGetErrorString(result, &message);
        std::cerr << name << " - " << message << '\n';

        exit(result);
    }
}

template<typename T> CUdeviceptr* asDevicePtrPtr(T& pointer) {
    return reinterpret_cast<CUdeviceptr*>(&pointer);
}

// GPU device init (using device 0)
void cudaSetup(CUcontext& context, CUmodule& module) {
    cudaCheck(cuInit(0));

    int devCount;
    cudaCheck(cuDeviceGetCount(&devCount));

    CUdevice device;
    cudaCheck(cuDeviceGet(&device, 0));

    char name[128];
    cudaCheck(cuDeviceGetName(name, 128, device));
    std::cout << "Using CUDA Device [0]: " << name << '\n';

    int devMajor, devMinor;
    cudaCheck(cuDeviceComputeCapability(&devMajor, &devMinor, device));
    std::cout << "Compute Capability: " << devMajor << '.' << devMinor << '\n';

    if (devMajor < 3) {
        std::cerr << "Device 0 must be SM 3.0 or greater\n";
        exit(1);
    }

    // Create driver context
    cudaCheck(cuCtxCreate(&context, 0, device));

    // Get kernel functions
    cudaCheck(cuModuleLoad(&module, "loopx2.ll.kernel.ptx"));
}

// passing as references to avoid copying or moving
void cudaTeardown(CUcontext& context, CUmodule& module) {
    cudaCheck(cuModuleUnload(module));
    cudaCheck(cuCtxDestroy(context));
}

// Will handle globals later
int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cerr << "Needs array size (at least 886)\n";
        return 1;
    }

    // Fails when type is `int` rather than `CUdeviceptr`
    CUdeviceptr LEN = std::stoi(argv[1]);

    if (LEN < 886) {
        std::cerr << "Array size must be at least 886\n";
        return 1;
    }

    CUcontext context;
    CUmodule module;
    cudaSetup(context, module);

    CUfunction turkeyOne, turkeyTwo, gooser, ducker;
    cudaCheck(cuModuleGetFunction(&turkeyOne, module, "main_for_cond"));
    cudaCheck(cuModuleGetFunction(&turkeyTwo, module, "main_for_cond_18"));
    cudaCheck(cuModuleGetFunction(&gooser,    module, "main_for_cond_35"));
    cudaCheck(cuModuleGetFunction(&ducker,    module, "main_for_cond_51"));

    CUdeviceptr dummy = 0;
    float* turkey;
    float* goose;
    float* duck;  // using as 2D array

    // for ease of use with CUDA driver functions (would it be safer to use
    // unions, or does it not matter as we don't set things through this
    // variable ourselves?)
    CUdeviceptr* devTurkey = asDevicePtrPtr(turkey);
    CUdeviceptr* devGoose  = asDevicePtrPtr(goose);
    CUdeviceptr* devDuck   = asDevicePtrPtr(duck);

    cudaCheck(cuMemAllocManaged(devTurkey, sizeof(float)*LEN,     CU_MEM_ATTACH_GLOBAL));
    cudaCheck(cuMemAllocManaged(devGoose,  sizeof(float)*LEN,     CU_MEM_ATTACH_GLOBAL));
    cudaCheck(cuMemAllocManaged(devDuck,   sizeof(float)*LEN*LEN, CU_MEM_ATTACH_GLOBAL));

    void* turkeyOneParams[] = {
        &dummy, // arg not used but CUDA fails if nullptr supplied
        devTurkey
    };
    cudaCheck(cuLaunchKernel(turkeyOne,
        1, 1, 1,
        LEN, 1, 1,
        0, nullptr, turkeyOneParams, nullptr));
    cudaCheck(cuCtxSynchronize());

    std::cout << "Turkey one " << turkey[93] << '\n';

    void* turkeyTwoParams[] = {
        &dummy, // arg not used but CUDA fails if nullptr supplied
        devTurkey
    };
    cudaCheck(cuLaunchKernel(turkeyTwo,
        1, 1, 1,
        LEN, 1, 1,
        0, nullptr, turkeyTwoParams, nullptr));
    cudaCheck(cuCtxSynchronize());

    std::cout << "Turkey two " << turkey[885] << '\n';

    void* gooserParams[] = {
        &dummy, // arg not used but CUDA fails if nullptr supplied
        devTurkey,
        &LEN,
        devGoose
    };
    cudaCheck(cuLaunchKernel(gooser,
        1, 1, 1,
        LEN, 1, 1,
        0, nullptr, gooserParams, nullptr));
    cudaCheck(cuCtxSynchronize());

    std::cout << "Goose " << goose[114] << '\n';

    void* duckerParams[] = {
        &dummy, // arg not used but CUDA fails if nullptr supplied
        devTurkey,
        devGoose,
        &LEN,
        devDuck
    };
    cudaCheck(cuLaunchKernel(ducker,
        LEN, 1, 1,
        LEN, 1, 1,
        0, nullptr, duckerParams, nullptr));
    cudaCheck(cuCtxSynchronize());

    std::cout << "Duck " << duck[500 * LEN + 500] << '\n';

    cudaCheck(cuMemFree(*devTurkey));
    cudaCheck(cuMemFree(*devGoose));
    cudaCheck(cuMemFree(*devDuck));

    cudaTeardown(context, module);

    return 0;
}
