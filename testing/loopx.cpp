#include <iostream>

// Will handle globals later
int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cerr << "Need array size (at least 886)\n";
        return 1;
    }

    const int LEN = std::stoi(argv[1]);

    if (LEN < 886) {
        std::cerr << "Array size must be at least 886\n";
        return 1;
    }

    float turkey[LEN];
    float goose[LEN];
    float duck[LEN][LEN];

    for (int i = 0; i < LEN; ++i) {
        turkey[i] = (i % 13) * i;
    }

    std::cout << "Turkey one " << turkey[93] << '\n';

    for (int i = 0; i < LEN; ++i) {
        // compiles to almost the same code that `if (i % 2) continue; [...]`
        // does, at least when optimized
        if (i % 2)
            turkey[i] *= 2;
    }

    std::cout << "Turkey two " << turkey[885] << '\n';


    for (int i = 0; i < LEN; ++i) {
        goose[LEN - i - 1] = turkey[i];
    }

    std::cout << "Goose " << goose[114] << '\n';

    for (int i = 0; i < LEN; ++i) {
        for (int j = 0; j < LEN; ++j) {
            duck[i][j] = turkey[i] * goose[j];
        }
    }

    std::cout << "Duck " << duck[500][500] << '\n';

    return 0;
}
