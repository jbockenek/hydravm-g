/// \file
/// Stripped-down test file to isolate issues with kernel launches

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int LEN = 256; // should be big enough
float goose[LEN];

void tostr(char* buffer, const int LEN, int val) {
    if (val == 0) {
        memcpy(buffer, "0", 2);
        return;
    }

    int i;
    for (i = 0; i < LEN-1 && val > 0; ++i, val /= 10) {
        // Using memmove to shift back the digits
        memmove(buffer + 1, buffer, i);

        buffer[0] = '0' + (char)(val % 10);
    }

    buffer[i] = '\0';
}

float divver(float x) {
    return x / 2;
}

float divver2(float x) {
    return divver(x) / 3;
}

float erffer(float x) {
    return erff(x / 2);
}

int main(int argc, char* argv[]) {
    int turkey[LEN];
    char* buffer = malloc(sizeof(char) * LEN);

    for (int i = 0; i < LEN; ++i) {
        turkey[i] = (i % 13) * i;
        goose[i] = erffer(turkey[i]);
//        goose[i] = divver2(turkey[i]);
    }

    tostr(buffer, LEN, turkey[93]);
    puts(buffer);
    tostr(buffer, LEN, goose[93]);
    puts(buffer);

    free(buffer);

    return 0;
}
