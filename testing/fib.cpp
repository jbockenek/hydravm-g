#include <iostream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::stoi;

int main(int argc, char* argv[]) {
    int f1, f2, iterations;

    // Checking for f1, f2, number of iterations
    if (argc < 4) {
        cout << "First value: ";
        cin >> f1;

        cout << "Second value: ";
        cin >> f2;

        cout << "Number of iterations: ";
        cin >> iterations;
    } else {
        f1 = stoi(argv[1]);
        f2 = stoi(argv[2]);
        iterations = stoi(argv[3]);
    }

    cout << endl << f1 << endl << f2 << endl;

    int next;
    for (int i = 0; i < iterations; i++) {
        next = f1 + f2;

        cout << next << endl;
        f1 = f2;
        f2 = next;
    }

    return 0;
}
