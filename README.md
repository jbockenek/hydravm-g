This project was developed using LLVM 3.8.0 (SVN rev 257094, git commit
f45267526ead95cb8aba903f97c03e031e069bd0); newer versions may work but the code
may require updating.

# Other Notes/etc.

Instruction lifting/sinking with appropriate guard conditions is likely
necessary to deal with loose nesting where the calculations cannot just be
performed in each thread (this may just end up as part of future transformations
for dependency handling).

If we can assume strict aliasing, it'll make analysis a lot easier...

Mark converted memory calls as tail if the originals were marked as such
^- may not be able to as CUDA allocation functions require a location to store
the address of allocated memory to be passed in

# Build Notes

Use `cmake -DCMAKE_C_COMPILER=clang .` to set up the build system for the
project if you want to build with clang(++) on Linux/Windows as cmake's
generated build files only default to clang on OSX.
(-DCMAKE_CXX_COMPILER=clang++ may be required if the project is explicitly
marked as CXX, but leaving that out the project will be treated as both and
CMake will check both the C and CXX compilers.)

`-DCMAKE_BUILD_TYPE=Debug` is also a good idea so you can actually see the
pass source when you need to run a debugger.

## Setup

Building LLVM in whatever folder (using default install location, `/usr/local`);
this assumes the LLVM source exists in `/usr/local/src` and you've already
installed clang to `/usr/local/bin` (absolute path needed to avoid compiler_rt
build failure), but you can use gcc or a prebuilt version of clang installed
manually or through a package manager instead (you may even be able to use VC++,
but no guarantees there; you'd have to tweak CMakeLists.txt to do that anyway):

    $ cd whatever
    $ cmake -DCMAKE_C_COMPILER=/usr/local/bin/clang \
    >       -DCMAKE_BUILD_TYPE=Release \
    >       -DLLVM_ENABLE_ASSERTIONS=On \
    >       -DLLVM_BINUTILS_INCDIR=<path_containing_binutils_plugin-api.h> \
    >       /usr/local/src/llvm
    $ make -j<N>          # or `cmake --build . -- -j<N>` for portability (though -j isn't portable)
    $ sudo make install   # or `sudo cmake --build . --target install`

## Various build steps

Example call to produce IR

(I'm not sure what happened to the text here, sorry.)
but due to if you want to emit bitcode, you'll need to use
. Unfortunately, if your default `ld` is not a
symlink to `ld.gold` and/or , you'll also have to explicitly )

    $ clang++ -std=c++11 -emit-llvm -S foo.cpp [-o foo.ll]

### Linker-related Stuff

This is useful when embedding the HydraVM-G toolchain in a preexisting one as
you don't have to add in usage of `llvm-link`.

You can add `-flto` when using LLVMgold for link-time optimization, assuming
your default linker [usually `/usr/bin/ld`] is (currently) a symlink to
`/usr/bin/ld.gold` (built with LTO and plugin support!) and that `LLVMgold.so`
is present and exists in `/usr/bin/bfd-plugins`. if gold is not the symlinked
linker, you can add `-fuse-ld=gold` to the compile command instead, but if using
gold as your primary linker (or using [update]-alternatives to switch between
linkers) is not a problem then it may be the better choice as doing so can
sometimes reduce the number of necessary command-line options you need/the
issues that occur. Note that, if you want to emit bitcode (useful if you have
static libraries you want to be included as part of your module so they can be
hopefully inlined/converted to GPGPU kernels; unfortunately, this will require
you to have compiled those static libraries into bitcode format first), you also
have to append `-Wl,-plugin-opt=emit-llvm`, e.g.

    $ clang -flto [-fuse-ld=gold] -static -lsomestaticlib -Wl,-plugin-opt=emit-llvm in.c [-o in.bc]

Mixing static and dynamic libraries can be a pain, but sometimes it needs to be
done, in which case you have to do something like

    $ clang -flto [-fuse-ld=gold] -Wl,-Bstatic -lstatic -Wl,-Bdynamic -ldynamic -Wl,-plugin-opt=emit-llvm in.c [-o in.bc]

(`-static` can't be used here because it tries to link all listed libraries
statically, even those after `-Wl,-Bdynamic`; symmetrically, you don't want to
use `-Wl,-Bstatic` without a following `-Wl,-Bdynamic`, as `-static` seems to
apply only to the libraries you list but `-Wl,-Bstatic` makes the linker try to
statically link all libraries passed to it, not just the ones explicitly listed)

You probably do not need to append `--as-needed` to any of the lists of linker
options as `-flto` should take care of eliminating most of the unused symbols in
the generated module.

### Other

You need nvidia_uvm for Unified (Virtual) Memory to work (included in
nvidia-modprobe package)

    $ nvcc -arch=sm_30 -std=c++11 test.cu

Basic out-of-source pass execution:

    $ opt -load <lib> -<passname> [-analyze] [-stats] [-S <for IR>] <ll_or_bc_file> [> outfile | -o outfile]

-scalarrepl, and -mem2reg to reduce explicit memory allocations and ensure
indvar canonicalization as phi node when possible (-argpromotion could be used
for more improvements, but it only works on internalized modules and
-internalize causes the HydraVM-G passes to fail due to hiding main())

-dce so we won't have loops with computable trip counts of 0

probably want to use -lcssa eventually, but that's only valuable for making
stuff like loop unswitching easier when values are live out of a loop (but it
seems to get executed at some point by one of the below passes anyway [probably
loop-simplify] so I guess we don't need to worry about it)

    $ opt -load lib/HydraGPGPUPasses.so -inline -scalarrepl -mem2reg -loop-simplify -dce -indvars -convert-allocs -extract-loops -simplifycfg -S build/loopx.ll > build/loopx2.ll

-licm may be useful for singly-nested loops with loop invariants/nested loops
with invariants for all levels

For converting kernel functions to GPGPU form

    $ opt -load lib/HydraGPGPUPasses.so -mark -convert-loops -dce -simplifycfg -S build/loopx.ll.kernel.ll > build/loopx2.ll.kernel.ll

For compiling kernel to PTX

    $ llc -mcpu=sm_30 build/loopx2.ll.kernel.ll -o build/loopx.ll.kernel.ptx

For compiling manual host (second one is to view the IR)

    $ clang++ -std=c++11 -g -lcuda testing/loopx.host.cpp -o build/loopx.host
    $ clang++ -std=c++11 -g -lcuda -emit-llvm -S testing/loopx.host.cpp -o build/loopx.host.ll

Run `test-toolchain.sh` to perform most of the steps at once

## Static Analysis

    $ clang++ -std=c++11 --analyze -Xanalyzer -analyzer-output=html -o html-dir <filename>.cpp

using -analyzer-output=text works for console-only but doesn't look as nice

there's also scan-build