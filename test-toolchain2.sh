#!/bin/bash

# The results of each step could be piped to the next rather than written to
# files, but using intermediate files allows us to examine them if anything goes
# wrong; this is also why -S is used (it provides human-readable IR rather than bitcode)

PASS_LIB=../lib/HydraGPGPUPasses.so
# excluding -inline so we can test conversion of helper functions
HOST_OPTS='-scalarrepl -mem2reg -loop-simplify -dce -indvars -convert-allocs -extract-loops -simplifycfg -S'
KERNEL_OPTS='-mark -convert-loops -dce -simplifycfg -S'
LIBDEVICE=/usr/lib/nvidia-cuda-toolkit/libdevice/libdevice.compute_30.10.bc # match up compute version with -mcpu flag
NVVM_OPTS='-nvvm-reflect -device-internalize -globaldce -dce -S'

# Don't want to continue build process if any steps fail
set -e

mkdir -p build

cd build
clang -std=c99 -emit-llvm -S ../testing/loopxx.c -o loopxx.ll
opt -load $PASS_LIB $HOST_OPTS loopxx.ll > loopxx.opt.ll

opt -load $PASS_LIB $KERNEL_OPTS loopxx.ll.kernel > loopxx.ll.kernel.opt
llvm-link -S loopxx.ll.kernel.opt $LIBDEVICE -o loopxx.ll.kernel.linked # only want libdevice after we've performed kernel marking
opt -load $PASS_LIB $NVVM_OPTS loopxx.ll.kernel.linked -o loopxx.ll.kernel.linked.opt
llc -mcpu=sm_30 -mattr=ptx40 loopxx.ll.kernel.linked.opt -o loopxx.ll.kernel.ptx

clang -std=c99 -lcuda -lm loopxx.opt.ll -o loopxx
