//===- KernelArgsGetter.cpp - Getter class for kernel args ----------------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
///
//===----------------------------------------------------------------------===//

#include "KernelArgsGetter.h"

#include "HydraGPGPU.h"

#include "llvm/ADT/SmallVector.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Value.h"

using namespace llvm;

hydra::gpgpu::KernelArgsGetter::KernelArgsGetter(
  Function* LoopFunction,
  Instruction* InsertPoint
) :
  LoopFunction(LoopFunction),
  Builder(InsertPoint),
  VoidPtrTy(Builder.getInt8PtrTy())
{
  // Sanity check
  assert(LoopFunction->hasOneUse() && "Loop-wrapper func has multiple uses!");
}

Value* hydra::gpgpu::KernelArgsGetter::getKernelArgs() {
  auto Caller = cast<CallInst>(LoopFunction->user_back());
  SmallVector<Value*, BASE_SMALLVECTOR_SIZE> ArgAllocas;
  unsigned ArgCount = Caller->getNumArgOperands();

  // Get extra space as needed
  ArgAllocas.reserve(ArgCount);

  // iterate over argument operands, build up the allocas
  for (Value* Arg : Caller->arg_operands()) {
    ArgAllocas.push_back(createArgAlloca(Arg));
  }

  return createArgArray(ArgAllocas);
}

Value* hydra::gpgpu::KernelArgsGetter::createArgAlloca(Value* Arg) {
  auto ArgType = Arg->getType();
  auto AI = createAllocaInEntryBlock(Builder, ArgType, "kernel.arg.alloca");

  // Storing the argument in the created alloca
  Builder.CreateStore(Arg, AI);

  // Alloca must be treated as i8* for
  return Builder.CreateBitCast(AI, VoidPtrTy);
}

Value* hydra::gpgpu::KernelArgsGetter::createArgArray(ArrayRef<Value*> Args) {
  // create an array of the alloca pointers (will require its own alloca); type
  // is [array of void*] to match kernel launch parameters
  std::size_t ArgCount = Args.size();
  Value* ArgArray = createAllocaInEntryBlock(
    Builder,
    VoidPtrTy,
    Builder.getInt64(ArgCount),
    "kernel.args.array"
  );

  for (std::size_t i = 0; i < ArgCount; ++i) {
    auto ElementPointer = Builder.CreateConstInBoundsGEP1_64(ArgArray, i);
    Builder.CreateStore(Args[i], ElementPointer);
  }

  return ArgArray;
}
