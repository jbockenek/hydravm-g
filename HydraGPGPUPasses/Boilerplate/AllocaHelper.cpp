//===- AllocaHelper.cpp - Tools for allocation conversion -----------------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Implementation of the AllocaHelper class.
///
//===----------------------------------------------------------------------===//

#include "AllocationConverters.h"

#include "HydraGPGPU.h"

#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Value.h"

using namespace llvm;

hydra::gpgpu::AllocaHelper::AllocaHelper(AllocConverter* Conv) :
  AllocaGlobalHelperBase(Conv)
{}

void hydra::gpgpu::AllocaHelper::setInsertPoint(AllocaInst* Alloca) {
  // insert just before alloca
  getBuilder().SetInsertPoint(Alloca);
}

AllocaInst* hydra::gpgpu::AllocaHelper::createDevicePtrPtr(AllocaInst* Alloca) {
  return createAllocaInEntryBlock(Alloca, CUdeviceptr);
}

Value* hydra::gpgpu::AllocaHelper::getMemAllocSize(AllocaInst* Alloca) {
  IRBuilder<>& Builder = getBuilder();
  Value* NumElements = Alloca->getArraySize();
  Type* ElementType = Alloca->getAllocatedType();

  // Size of the type (in bytes), _including alignment padding_.
  auto RawElementSize = ModuleDataLayout.getTypeAllocSize(ElementType);
  Value* ElementSize = Builder.getInt64(RawElementSize);

  // Pre-casting to ensure proper multiplication (the result should not exceed
  // 64 bits anyway due to size_t limitations). CreateIntCast is a no-op when
  // the types are the same, so should be safe to ensure consistency for the
  // size argument; cast is unsigned because allocation sizes are (supposed to
  // be) unsigned.
  NumElements = Builder.CreateIntCast(NumElements, Builder.getInt64Ty(), false);

  // seeming signed-wrapping can occur even when the values are just fine
  // unsigned, so we only want to say there's no wrapping for the unsigned
  // representation
  return Builder.CreateNUWMul(NumElements, ElementSize);
}

Function* hydra::gpgpu::AllocaHelper::getFunction(AllocaInst* Alloca) {
  return Alloca->getFunction();
}

void hydra::gpgpu::AllocaHelper::postProcessing(
  AllocaInst* OldAlloca,
  AllocaInst* DevicePtrPtr
) {
  IRBuilder<>& Builder = getBuilder();

  // Allocation result is expected to be a pointer, not pointer to pointer, so
  // obtaining that value, converting to pointer of the right type, and
  // replacing the old allocation result. (The cast is more general than just
  // IntToPtr in case Nvidia switches from ints to actual pointers given that,
  // with 64-bit systems, the size is guaranteed to be valid in both device and
  // host memory spaces.)
  Value* DevicePtr = Builder.CreateLoad(DevicePtrPtr);
  DevicePtr = Builder.CreateBitOrPointerCast(DevicePtr, OldAlloca->getType());

  // Some final cleanup so that the device allocation pointer completely
  // replaces the original allocation.
  DevicePtr->takeName(OldAlloca);
  OldAlloca->replaceAllUsesWith(DevicePtr);
  OldAlloca->eraseFromParent();
}
