//===- GVHelper.cpp - Tools for allocation conversion ---------------------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Implementation of the GVHelper class.
///
//===----------------------------------------------------------------------===//

#include "AllocationConverters.h"

#include "CUDABoilerplate.h"
#include "CUDAFunctions.h"
#include "HydraGPGPU.h"
#include "Utils/ConstantExprConverter.h"

#include "llvm/ADT/StringRef.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Use.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Value.h"

using namespace llvm;

hydra::gpgpu::GVHelper::GVHelper(AllocConverter* Conv) :
  AllocaGlobalHelperBase(Conv)
{}

Value* hydra::gpgpu::GVHelper::getMemAllocSize(GlobalVariable* GV) {
  // Size of the underlying type (in bytes), including alignment padding.
  auto RawSize = ModuleDataLayout.getTypeAllocSize(GV->getValueType());

  return getBuilder().getInt64(RawSize);
}

Function* hydra::gpgpu::GVHelper::getFunction(GlobalVariable* GV) {
  Module* M = GV->getParent();
  assert(M && "Global variable does not have a parent module!");

  Function* Main = M->getFunction("main");
  assert(Main && !Main->isDeclaration() && "Cannot operate on non-main module");

  return Main;
}

void hydra::gpgpu::GVHelper::setInsertPoint(GlobalVariable* GV) {
  // Inserting the new function call near the start of the program, after any
  // allocas in main()
  getBuilder().SetInsertPoint(getFirstNonAllocaInsertionPt(getFunction(GV)));
}

GlobalVariable* hydra::gpgpu::GVHelper::createDevicePtrPtr(GlobalVariable* GV) {
  return createGlobalVariable(*GV->getParent(), CUdeviceptr);
}

// Replacing the use of the old global with a load of the device address;
// placing in the instruction's function's entry block to avoid any reference to
// the global inside loops so that we don't have to worry about directly mapping
// non-constant globals to function arguments.
//
// This may introduce excess loads if an instruction uses the same Value
// multiple times, but LLVM should be able to deal with that as we aren't using
// atomic loads or marking them as volatile. (Given the intent of conversion of
// sequential programs, we assume for now that atomic/volatile variables will
// not be in use to begin with.)
void hydra::gpgpu::GVHelper::postProcessing(
  GlobalVariable* OldGlobalVariable,
  GlobalVariable* DevicePtrPtr
) {
  // Currently positioned right after CUDA call
  IRBuilder<>& Builder = getBuilder();

  // Need to copy initial value from old GV
  Value* DevicePtr = getDevicePtr(OldGlobalVariable, DevicePtrPtr);
  Builder.CreateStore(OldGlobalVariable->getInitializer(), DevicePtr);

  // Storing Builder IP so we can set to that if Inst is in the same function
  // (as otherwise we'd end up inserting before the initialization code!)
  IRBuilderBase::InsertPoint MainIP = Builder.saveIP();
  Function* InitFunc = MainIP.getBlock()->getParent();

  // Need to replace all ConstantExprs that use the old global with regular
  // insts so we can substitute new non-const loads
  CEConverter.convertUsers(OldGlobalVariable);

  while (!OldGlobalVariable->use_empty()) {
    auto& U = *OldGlobalVariable->use_begin();

    if (auto Inst = dyn_cast<Instruction>(U.getUser())) {
      Function* InstFunc = Inst->getFunction();

      if (InstFunc == InitFunc) {
        Builder.restoreIP(MainIP);
      } else {
        Builder.SetInsertPoint(getFirstNonAllocaInsertionPt(InstFunc));
      }

      // Necessary to keep calling getDevicePtr() due to builder IP change
      U = getDevicePtr(OldGlobalVariable, DevicePtrPtr);
    } else {
      // can't do anything with non-instruction uses
      U = nullptr;
    }
  }

  DevicePtrPtr->setName(OldGlobalVariable->getName() + ".addr");
  OldGlobalVariable->eraseFromParent();
}

Value* hydra::gpgpu::GVHelper::getDevicePtr(
  GlobalVariable* OldGlobalVariable,
  GlobalVariable* DevicePtrPtr
) {
  IRBuilder<>& Builder = getBuilder();
  StringRef Name = OldGlobalVariable->getName();
  Type* ValueTy = OldGlobalVariable->getType();

  Value* DevicePtr = Builder.CreateLoad(DevicePtrPtr, Name + ".load");

  return Builder.CreateBitOrPointerCast(DevicePtr, ValueTy, Name);
}
