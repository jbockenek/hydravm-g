//===- InsertErrorCheck.cpp - defines insertCudaCheckErrorCode() ----------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains the definition and helpers for
/// insertCudaCheckErrorCode(), declared in CUDABoilerplate.h
///
//===----------------------------------------------------------------------===//

#include "CUDABoilerplate.h"

#include "CUDAFunctions.h"
#include "HydraGPGPU.h"

#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/IR/Attributes.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Value.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/BuildLibCalls.h"

using namespace llvm;

// MAYBE: also provide function and basic block names for the failure location,
// maybe even the index of the instruction? (If names don't work for anonymous
// blocks(/functions), maybe use indices for those too. That may not be
// reliable either due to the possibility of additional IR manipulation, so the
// best solution may be a unique name or value manually supplied to each error
// check function call)

// Using hydra::gpgpu instead of anonymous namespace as the class is technically
// closely connected to the hydra::gpgpu functions it calls and I'd rather not
// do `using namespace hydra::gpgpu`.
namespace hydra {
namespace gpgpu {
  // Doesn't need to be exposed as only insertCudaCheckErrorCode() uses it right
  // now. That may change later, though.
  class OnCUDAErrorExitInserter : public hydra::gpgpu::FailureExitInserter {
    /// \brief necessary for overridden \c preExitHandling()
    const TargetLibraryInfo& TLI;

    Value* ErrorNamePtr;
    Value* ErrorStringPtr;

    /// \brief performs the actual output
    void putErr(Value* Name, Value* String);

    /// \brief Simplifies EmitPutS as we use the same Builder/TLI each time
    Value* putS(Value* CString);

    /// \brief Simplifies EmitPutChar as we use the same Builder/TLI each time
    Value* putC(Value* Char);

    Value* createStringPtrAlloca(const Twine& name="");
    void preExitDefaultsHelper(Value* ErrorNameGetResult);
  protected:
    /// \brief Inserts outputting of CUDA error message meaning
    void preExitHandling(Value* ErrorVal) override;

    Type* getErrorHandlerParamType() override;
    AttributeSet getErrorHandlerAttrs() override;
    Value* getErrorHandlerCond(Value* V) override;
  public:
    OnCUDAErrorExitInserter(IRBuilder<>& Builder, const TargetLibraryInfo& TLI);
  };
}
}

// Inserting the following in code is non-portable (not just the struct bodies;
// the names and declaration of stderr may be non-portable as well). Making it
// portable would require generating the code differently for every target that
// generates the code differently, which is beyond our scope right now, while
// we don't need to worry about the actual type of FILE when using putc/puts.
// Unfortunately, we can't output filename or line number as those need the
// original source code to work right.
//
// %struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*,
//   i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8,
//   [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
// %struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }
//
// @stderr = external global %struct._IO_FILE*, align 8
//
// ; Function Attrs: nounwind
// declare i32 @fputs(i8* nocapture readonly, %struct._IO_FILE* nocapture) #1
//
// ; Function Attrs: nounwind
// declare i32 @fputc(i32, %struct._IO_FILE* nocapture) #1
void hydra::gpgpu::insertCudaCheckErrorCode(
  IRBuilder<>& Builder,
  Value* ValToCheck
) {
  auto M = Builder.GetInsertBlock()->getModule();
  TargetLibraryInfo TLI(TargetLibraryAnalysis().run(*M));

  OnCUDAErrorExitInserter(Builder, TLI).insert(ValToCheck);
}

hydra::gpgpu::FailureExitInserter::FailureExitInserter(
  IRBuilder<>& Builder,
  StringRef ErrFunName
) :
  Mod(Builder.GetInsertPoint()->getModule()),
  Builder(Builder),
  ErrFunName(ErrFunName)
{}

hydra::gpgpu::FailureExitInserter::FailureExitInserter(IRBuilder<>& Builder) :
  FailureExitInserter(Builder, "_ZN5hydra5gpgpu13exitOnFailureEjj")
{}

void hydra::gpgpu::FailureExitInserter::insert(Value* ErrorVal) {
  auto Handler = getErrorHandler();
  auto ErrorType = getErrorHandlerParamType();

  // Create zext, trunc, or bitcast if necessary
  ErrorVal = Builder.CreateIntCast(ErrorVal, ErrorType, false);

  Builder.CreateCall(Handler, ErrorVal);
}

Function* hydra::gpgpu::FailureExitInserter::getErrorHandler() {
  auto FTy = FunctionType::get(
    Builder.getVoidTy(),
    getErrorHandlerParamType(),
    false // Non-variadic
  );
  AttributeSet AS = getErrorHandlerAttrs();
  auto Handler = cast<Function>(Mod->getOrInsertFunction(ErrFunName, FTy, AS));

  if (Handler->empty()) {  // Function did not previously exist
    fillErrorHandler(Handler);
  }

  return Handler;
}

Type* hydra::gpgpu::FailureExitInserter::getErrorHandlerParamType() {
  return getExit(Mod)->getFunctionType()->getParamType(0);
}

AttributeSet hydra::gpgpu::FailureExitInserter::getErrorHandlerAttrs() {
  return AttributeSet::get(Builder.getContext(), AttributeSet::FunctionIndex, {
    Attribute::NoUnwind,
    Attribute::ReadNone,  // Base version only operates on supplied value
    Attribute::UWTable
  });
}

Value* hydra::gpgpu::FailureExitInserter::getErrorHandlerCond(Value* ErrorVal) {
  return Builder.CreateIsNotNull(ErrorVal);
}

void hydra::gpgpu::FailureExitInserter::preExitHandling(
  Value* /*ErrorVal*/
) {}

void hydra::gpgpu::FailureExitInserter::fillErrorHandler(Function* Handler) {
  LLVMContext& Context = Builder.getContext();

  // Named arguments look nicer
  Value* ErrorVal = &*Handler->arg_begin();
  ErrorVal->setName("result");

  auto EntryBlock = BasicBlock::Create(Context, "entry", Handler);
  auto FailureBlock = BasicBlock::Create(Context, "if.failure", Handler);
  auto SuccessBlock = BasicBlock::Create(Context, "if.success", Handler);

  // I don't know if it's possible to reobtain the original insert point when
  // using just as an instruction due to the possibility of the insert point
  // being set to the end of a(n empty) basic block, meaning the instruction
  // will just be the end() value and may not be able to provide info about the
  // block.
  IRBuilderBase::InsertPoint OldInsertPoint = Builder.saveIP();
  Builder.SetInsertPoint(EntryBlock);

  Value* Cond = getErrorHandlerCond(ErrorVal);

  Builder.CreateCondBr(Cond, FailureBlock, SuccessBlock);
  Builder.SetInsertPoint(FailureBlock);

  Function* ExitFunc = getExit(Mod);
  auto ExitParamType = ExitFunc->getFunctionType()->getParamType(0);

  // Create zext, trunc, or bitcast if necessary
  ErrorVal = Builder.CreateIntCast(ErrorVal, ExitParamType, false);

  // Print out error messages, etc.
  preExitHandling(ErrorVal);

  // Program flow ends with this call
  Builder.CreateCall(ExitFunc, {ErrorVal});
  Builder.CreateUnreachable();

  // We just return if program flow gets here
  Builder.SetInsertPoint(SuccessBlock);
  Builder.CreateRetVoid();

  Builder.restoreIP(OldInsertPoint);
}

hydra::gpgpu::OnCUDAErrorExitInserter::OnCUDAErrorExitInserter(
  IRBuilder<>& Builder,
  const TargetLibraryInfo& TLI
) : FailureExitInserter(
  Builder,
  "_ZN5hydra5gpgpu14cudaErrorCheckE14cudaError_enum"
), TLI(TLI), ErrorNamePtr(nullptr), ErrorStringPtr(nullptr) {}

Type* hydra::gpgpu::OnCUDAErrorExitInserter::getErrorHandlerParamType() {
  return getCUresultTy(Mod);
}

AttributeSet hydra::gpgpu::OnCUDAErrorExitInserter::getErrorHandlerAttrs() {
  return AttributeSet::get(Builder.getContext(), AttributeSet::FunctionIndex, {
    Attribute::NoUnwind,
    Attribute::UWTable
  });
}

Value* hydra::gpgpu::OnCUDAErrorExitInserter::getErrorHandlerCond(Value* V) {
  return Builder.CreateICmpNE(V, getCudaSuccess(Mod));
}

void hydra::gpgpu::OnCUDAErrorExitInserter::preExitHandling(Value* ErrorVal) {
  // As the alloca results will be of type i8**, we can pass them directly to
  // the cuGet functions.
  ErrorNamePtr = createStringPtrAlloca("cuda.error.name.addr");
  ErrorStringPtr = createStringPtrAlloca("cuda.error.string.addr");

  Function* CuGetErrorName = getCuGetErrorName(Mod);
  Function* CuGetErrorString = getCuGetErrorString(Mod);

  // We only need to test the first call's result value for success
  Value* Result = Builder.CreateCall(CuGetErrorName, {ErrorVal, ErrorNamePtr});
  Builder.CreateCall(CuGetErrorString, {ErrorVal, ErrorStringPtr});

  preExitDefaultsHelper(Result);

  Value* ErrorName = Builder.CreateLoad(ErrorNamePtr, "cuda.error.name");
  Value* ErrorString = Builder.CreateLoad(ErrorStringPtr, "cuda.error.string");

  putErr(ErrorName, ErrorString);
}

void hydra::gpgpu::OnCUDAErrorExitInserter::preExitDefaultsHelper(
  Value* ErrorNameGetResult
) {
  Value* UnknownError = Builder.CreateICmpNE(
    ErrorNameGetResult,
    getCudaSuccess(Mod),
    "unknown.error"
  );

  // insert branch for defaults
  LLVMContext& Context = Builder.getContext();
  Function* Handler = Builder.GetInsertBlock()->getParent();

  auto UnknownBlock = BasicBlock::Create(Context, "if.unknown.error", Handler);
  auto EndBlock = BasicBlock::Create(Context, "if.failure.end", Handler);
  Builder.CreateCondBr(UnknownError, UnknownBlock, EndBlock);

  Builder.SetInsertPoint(UnknownBlock);

  Value* DefaultName = Builder.CreateGlobalStringPtr(
    "(Unknown CUDA error)",
    "cuda.error.unknown.name"
  );
  Value* DefaultString = Builder.CreateGlobalStringPtr(
    "invalid error code",
    "cuda.error.unknown.string"
  );

  Builder.CreateStore(DefaultName, ErrorNamePtr);
  Builder.CreateStore(DefaultString, ErrorStringPtr);

  // Finish up in the last block (which will be continued outside this function)
  Builder.CreateBr(EndBlock);
  Builder.SetInsertPoint(EndBlock);
}

Value* hydra::gpgpu::OnCUDAErrorExitInserter::createStringPtrAlloca(
    const Twine& name
) {
  auto CStringTy = Builder.getInt8PtrTy();

  return createAllocaInEntryBlock(Builder, CStringTy, nullptr, name);
}

void hydra::gpgpu::OnCUDAErrorExitInserter::putErr(Value* Name, Value* String) {
  auto Asterisk = Builder.getInt8('*');
  auto Space = Builder.getInt8(' ');

  // This prints to stdout rather than stderr, but that's okay for now
  // format: name + "\n* " + message + '\n' (putS adds newline)
  putS(Name);
  putC(Asterisk);
  putC(Space);
  putS(String);
}

Value* hydra::gpgpu::OnCUDAErrorExitInserter::putS(Value* CString) {
  Value* Call = EmitPutS(CString, Builder, &TLI);

  // fallback if library builtin not available
  if (!Call) {
    Call = Builder.CreateCall(getPutS(Mod), CString, "puts");
  }

  return Call;
}

Value* hydra::gpgpu::OnCUDAErrorExitInserter::putC(Value* Char) {
  Value* Call = EmitPutChar(Char, Builder, &TLI);

  // fallback if library builtin not available
  if (!Call) {
    Function* PutChar = getPutChar(Mod);
    auto CharTy = PutChar->getFunctionType()->getParamType(0);

    // apparently putchar takes signed input
    Char = Builder.CreateIntCast(Char, CharTy, true, "chari");
    Call = Builder.CreateCall(PutChar, Char, "putchar");
  }

  return Call;
}
