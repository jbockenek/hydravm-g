//===- AllocaGlobalHelperBase.cpp - Base class for Alloca & Global helper -===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
///
//===----------------------------------------------------------------------===//

#include "AllocationConverters.h"

#include "CUDABoilerplate.h"
#include "CUDAFunctions.h"
#include "HydraGPGPU.h"

#include "llvm/IR/Function.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Value.h"

using namespace llvm;

namespace {
  /// \brief Inserts an appropriate cuMemFree call before every visited
  /// \c ReturnInst
  class MemFreeInserter : public InstVisitor<MemFreeInserter> {
    Function* Func;
    Value* AllocToFreeAddr;
    bool AddErrorChecking;
    IRBuilder<> Builder;
    Function* CuMemFree;
  public:
    MemFreeInserter(
      Function* Func,
      Value* AllocToFreeAddr,
      bool AddErrorChecking=true
    );

    void visitReturnInst(ReturnInst& InsertionPoint);
    void insert();
  };
}

MemFreeInserter::MemFreeInserter(
  Function* Func,
  Value* AllocToFreeAddr,
  bool AddErrorChecking
) :
  Func(Func),
  AllocToFreeAddr(AllocToFreeAddr),
  AddErrorChecking(AddErrorChecking),
  Builder(Func->getContext()),
  CuMemFree(hydra::gpgpu::getCuMemFree(Func->getParent()))
{}

void MemFreeInserter::visitReturnInst(ReturnInst& InsertionPoint) {
  Builder.SetInsertPoint(&InsertionPoint);

  Value* AllocToFree = Builder.CreateLoad(AllocToFreeAddr);
  Value* FreeResult = Builder.CreateCall(CuMemFree, AllocToFree);

  if (AddErrorChecking) {
    hydra::gpgpu::insertCudaCheckErrorCode(Builder, FreeResult);
  }
}

void MemFreeInserter::insert() {
  visit(Func);
}

template<typename T>
hydra::gpgpu::AllocaGlobalHelperBase<T>::AllocaGlobalHelperBase(
  hydra::gpgpu::AllocConverter* Conv
) :
  Conv(Conv),
  CuMemAllocManaged(hydra::gpgpu::getCuMemAllocManaged(Conv->CurModule)),
  CUdeviceptr(getCUdeviceptrTy(Conv->CurModule)),
  ModuleDataLayout(Conv->CurModule)
{}

// vtable anchor
template<typename T>
hydra::gpgpu::AllocaGlobalHelperBase<T>::~AllocaGlobalHelperBase() {}

template<typename T>
IRBuilder<>& hydra::gpgpu::AllocaGlobalHelperBase<T>::getBuilder() {
  return Conv->Builder;
}

template<typename T>
const TargetLibraryInfo&
hydra::gpgpu::AllocaGlobalHelperBase<T>::getTargetLibraryInfo() {
  return Conv->LibraryInfo;
}

template<typename T>
Module* hydra::gpgpu::AllocaGlobalHelperBase<T>::getModule() {
  return Conv->CurModule;
}

template<typename T>
void hydra::gpgpu::AllocaGlobalHelperBase<T>::convertToCUDACall(
  AllocaInst* OldAlloca,
  AllocaInst* DevicePtrPtr,
  Value* MemAllocSize
) {
  Conv->convertToCUDACall(OldAlloca, CuMemAllocManaged, {
    DevicePtrPtr,
    MemAllocSize,
    getBuilder().getInt32(CU_MEM_ATTACH_GLOBAL)
  });
}

template<typename T>
void hydra::gpgpu::AllocaGlobalHelperBase<T>::convertToCUDACall(
  GlobalVariable* /*Dummy*/,
  GlobalVariable* DevicePtrPtr,
  Value* MemAllocSize
) {
  Conv->convertToCUDACall(CuMemAllocManaged, {
    DevicePtrPtr,
    MemAllocSize,
    getBuilder().getInt32(CU_MEM_ATTACH_GLOBAL)
  });
}

template<typename T>
void hydra::gpgpu::AllocaGlobalHelperBase<T>::convert(T* Object) {
  setInsertPoint(Object);

  // CUDA mallocs use an output argument, so we need to create something to
  // store it in.
  T* DevicePtrPtr = createDevicePtrPtr(Object);
  Value* MemAllocSize = getMemAllocSize(Object);

  convertToCUDACall(Object, DevicePtrPtr, MemAllocSize);

  // Frees the device pointer before any returns in the specified function (DOES
  // NOT HANDLE FREEING DURING STACK UNWINDING; exit() should be fine without
  // explicit freeing, though)
  MemFreeInserter(getFunction(DevicePtrPtr), DevicePtrPtr).insert();
  postProcessing(Object, DevicePtrPtr);
}

template class hydra::gpgpu::AllocaGlobalHelperBase<AllocaInst>;
template class hydra::gpgpu::AllocaGlobalHelperBase<GlobalVariable>;
