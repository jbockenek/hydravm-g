//===- CUDABoilerplate.cpp - CUDA driver API boilerplate ------------------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains definitions of functions and data structures for
/// inserting all the setup/teardown boilerplate and kernel launching calls
/// necessary for CUDA execution.
///
//===----------------------------------------------------------------------===//

#include "CUDABoilerplate.h"

#include "CUDAFunctions.h"
#include "HydraGPGPU.h"
#include "KernelLauncher.h"

#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/ADT/Twine.h" // for Twine concat functionality
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Value.h"
#include "llvm/Support/CommandLine.h"

#include <string>

// forward decls
namespace llvm {
  class MDNode;
}

using namespace llvm;
using namespace hydra::gpgpu;

cl::opt<std::string> KernelPath(
  "kernel-path",
  cl::value_desc("kernel_path"),
  cl::desc(
    "Location of the kernel module; defaults to `.'."
  ),
  cl::init(".")
);

// private helper stuff
namespace {
  class SetupInserter {
    IRBuilder<> Builder;
    std::string KernelModuleName;
    ArrayRef<KernelMarker> KernelMarkers;
    bool AddErrorChecking;

    Module* InsMod;
    Type* i32;
    Value* Zero32;

    GPGPUData insertDeviceSetup();
    void insertDeviceCountCheck();
    Value* insertDeviceGet();
    void insertDeviceComputeCapabilityCheck(Value* Device);
    Value* insertCUDAContextCreation(Value* Device);
    Value* insertKernelModuleLoad();
    Value* insertKernelGet(Value* KernelModule, StringRef KernelName);

    // Need to supply the global pointer as the marked insertion point may not
    // be in the main function; we'll load the global's value locally.
    void insertKernelLaunch(Value* KernelFuncPtr, KernelMarker& Marker);
    void insertKernelLaunchCleanup(Instruction* LaunchResult);
  public:
    SetupInserter(
      Instruction* InsertionPoint,
      const Twine& KernelModuleName,
      ArrayRef<KernelMarker> KernelMarkers,
      bool AddErrorChecking
    );

    GPGPUData insert();
  };

  /// \brief Inserts teardown code before every visited \c ReturnInst
  class TeardownReturnVisitor : public InstVisitor<TeardownReturnVisitor> {
    GPGPUData Data;
    bool AddErrorChecking;
  public:
    TeardownReturnVisitor(GPGPUData Data, bool AddErrorChecking);

    void visitReturnInst(ReturnInst& InsertionPoint);
  };

  class TeardownInserter {
    IRBuilder<> Builder;
    GPGPUData Data;
    bool AddErrorChecking;

    Module* InsMod;
  public:
    TeardownInserter(
      Instruction* InsertionPoint,
      GPGPUData Data,
      bool AddErrorChecking
    );

    void insert();
  };

  void addToSkipMetadata(Module& M, GlobalVariable* GV) {
    auto MDGlobalWrapper = M.getOrInsertNamedMetadata(MD_GLOBALS);
    auto MetaGV = ConstantAsMetadata::get(GV);

    // NamedMDNodes can only hold MDNodes, no leaf metadata, so we have to use
    // an in-between MDNode for the list of globals (it doesn't make sense for
    // each global to have an individual MDNode either)
    auto GVWrapper = MDTuple::get(M.getContext(), MetaGV);

    if (MDGlobalWrapper->getNumOperands() == 0) { // No old list, simple case
      MDGlobalWrapper->addOperand(GVWrapper);
    } else { // we have to merge the new with the old
      MDNode* MDGlobals = MDGlobalWrapper->getOperand(0);
      MDGlobals = MDNode::concatenate(MDGlobals, GVWrapper);

      MDGlobalWrapper->setOperand(0, MDGlobals);
    }
  }
}

GPGPUData hydra::gpgpu::insertGPGPUSetup(
  Instruction* InsertionPoint,
  const Twine& KernelModuleName,
  ArrayRef<KernelMarker> KernelMarkers,
  bool AddErrorChecking
) {
  assert(
    InsertionPoint->getModule() &&
    "Insertion point instruction is not in a function belonging to a module"
  );

  return SetupInserter(
    InsertionPoint,
    KernelModuleName,
    KernelMarkers,
    AddErrorChecking
  ).insert();
}

SetupInserter::SetupInserter(
  Instruction* InsertionPoint,
  const Twine& KernelModuleName,
  ArrayRef<KernelMarker> KernelMarkers,
  bool AddErrorChecking
) :
  Builder(InsertionPoint),
  KernelModuleName(KernelPath + "/" + KernelModuleName.str()),
  KernelMarkers(KernelMarkers),
  AddErrorChecking(AddErrorChecking),
  InsMod(InsertionPoint->getModule()),
  i32(Builder.getInt32Ty()),
  Zero32(Builder.getInt32(0))
{}

GPGPUData SetupInserter::insert() {
  Value* InitResult = Builder.CreateCall(getCuInit(InsMod), Zero32);

  if (AddErrorChecking) {
    insertCudaCheckErrorCode(Builder, InitResult);
  }

  return insertDeviceSetup();
}

GPGPUData SetupInserter::insertDeviceSetup() {
  insertDeviceCountCheck();

  Value* Device = insertDeviceGet();
  insertDeviceComputeCapabilityCheck(Device);

  Value* CUDAContext = insertCUDAContextCreation(Device);
  Value* KernelModule = insertKernelModuleLoad();

  // Can't use & because ArrayRef elements are const
  for (KernelMarker Marker : KernelMarkers) {
    // No need to change the name as the function referenced by Marker is not in
    // the host module.
    Value* KernelPtr = insertKernelGet(KernelModule, Marker.Kernel->getName());

    insertKernelLaunch(KernelPtr, Marker);
  }

  return {CUDAContext, KernelModule};
}

void SetupInserter::insertDeviceCountCheck() {
  // CUDA functions use output args all over, so we have to rely on allocas
  Value* DCP = createAllocaInEntryBlock(Builder, i32, nullptr, "devcount.ptr");
  Value* CountResult = Builder.CreateCall(getCuDeviceGetCount(InsMod), DCP);

  if (AddErrorChecking) {
    insertCudaCheckErrorCode(Builder, CountResult);
  }

  // Can't do anything if no GPUs are available, so terminate the program if so
  Value* DeviceCount = Builder.CreateLoad(DCP, "devcount");
  Value* NoDevices = Builder.CreateIsNull(DeviceCount);
  FailureExitInserter(Builder).insert(NoDevices);
}

Value* SetupInserter::insertDeviceGet() {
  // Currently only utilizing device 0; allow specifying from the command line
  Value* DP = createAllocaInEntryBlock(Builder, i32, nullptr, "device.ptr");
  Function* CuDeviceGet = getCuDeviceGet(InsMod);
  Value* DeviceGetResult = Builder.CreateCall(CuDeviceGet, {DP, Zero32});

  if (AddErrorChecking) {
    insertCudaCheckErrorCode(Builder, DeviceGetResult);
  }

  return Builder.CreateLoad(DP, "device");
}

void SetupInserter::insertDeviceComputeCapabilityCheck(Value* Device) {
  Value* DeviceMajorPtr =
      createAllocaInEntryBlock(Builder, i32, nullptr, "device.major.ptr");
  Value* DeviceMinorPtr =
      createAllocaInEntryBlock(Builder, i32, nullptr, "device.minor.ptr");
  Function* DeviceCCFunc = getCuDeviceComputeCapability(InsMod);
  Value* DeviceCCResult = Builder.CreateCall(DeviceCCFunc, {
    DeviceMajorPtr,
    DeviceMinorPtr, // unused, but necessary to flesh out the call
    Device
  });

  if (AddErrorChecking) {
    insertCudaCheckErrorCode(Builder, DeviceCCResult);
  }

  Value* DeviceMajor = Builder.CreateLoad(DeviceMajorPtr, "device.major");

  // A signed comparison of compute capability doesn't really make sense as the
  // version won't be negative
  Value* CCTooLow = Builder.CreateICmpULT(
    DeviceMajor,
    Builder.getInt32(COMPUTE_CAPABILITY_MAJOR_VERSION_MIN)
  );
  FailureExitInserter(Builder).insert(CCTooLow);
}

Value* SetupInserter::insertCUDAContextCreation(Value* Device) {
  // CUcontext -> %struct.CUctx_st*
  Value* CUDAContextPtr = createAllocaInEntryBlock(
    Builder,
    getCUctxTy(InsMod)->getPointerTo(),
    nullptr,
    "context.ptr"
  );

  // Using non-implicit-blocking context for now; all blocking will be done
  // explicitly with cuCtxSynchronize().
  Value* CtxCreateResult = Builder.CreateCall(getCuCtxCreate(InsMod), {
    CUDAContextPtr,
    Zero32,    // CU_CTX_SCHED_AUTO
    Device
  });

  if (AddErrorChecking) {
    insertCudaCheckErrorCode(Builder, CtxCreateResult);
  }

  return Builder.CreateLoad(CUDAContextPtr, "context");
}

Value* SetupInserter::insertKernelModuleLoad() {
  // CUmodule -> %struct.CUmod_st*
  Value* KernelModulePtr = createAllocaInEntryBlock(
    Builder,
    getCUmodTy(InsMod)->getPointerTo(),
    nullptr,
    "module.ptr"
  );
  Value* ModuleLoadResult = Builder.CreateCall(getCuModuleLoad(InsMod), {
    KernelModulePtr,
    Builder.CreateGlobalStringPtr(KernelModuleName, "kernel.module.name")
  });

  if (AddErrorChecking) {
    insertCudaCheckErrorCode(Builder, ModuleLoadResult);
  }

  return Builder.CreateLoad(KernelModulePtr, "module");
}

Value* SetupInserter::insertKernelGet(
  Value* KernelModule,
  StringRef KernelName
) {
  // CUfunction -> %struct.CUfunc_st*
  PointerType* Ty = getCUfuncTy(InsMod)->getPointerTo();
  Value* KernelFuncPtr = createGlobalVariable(*InsMod, Ty, KernelName + ".ptr");

  Value* GetResult = Builder.CreateCall(getCuModuleGetFunction(InsMod), {
    KernelFuncPtr,
    KernelModule,
    Builder.CreateGlobalStringPtr(KernelName, KernelName + ".name")
  });

  if (AddErrorChecking) {
    insertCudaCheckErrorCode(Builder, GetResult);
  }

  return KernelFuncPtr;
}

void SetupInserter::insertKernelLaunch(
  Value* KernelFuncPtr,
  KernelMarker& Marker
) {
  StringRef KernelName = Marker.Kernel->getName();

  KernelLauncher Launcher(
    Marker.FallbackFunction,
    Marker.LaunchLocation,
    KernelFuncPtr,
    KernelName,
    Marker.Dims
  );

  // Not worrying about shared memory, stream association, or extras right now
  Instruction* LaunchResult = Launcher.insert(
    Marker.ConditionForLaunch,
    KernelName + ".call"
  );

  insertKernelLaunchCleanup(LaunchResult);
}

void SetupInserter::insertKernelLaunchCleanup(Instruction* LaunchResult) {
  // kernel launch may be in a different block, need to move builder there
  auto OldIP = Builder.saveIP();
  Instruction* InsertPoint = LaunchResult->getParent()->getTerminator();
  assert(InsertPoint && "Launch result in malformed basic block!");

  Builder.SetInsertPoint(InsertPoint);

  if (AddErrorChecking) {
    insertCudaCheckErrorCode(Builder, LaunchResult);
  }

  // For now, synchronizing after every launch
  Value* SyncResult = Builder.CreateCall(getCuCtxSynchronize(InsMod));

  if (AddErrorChecking) {
    insertCudaCheckErrorCode(Builder, SyncResult);
  }

  // Going back to old insert point just in case
  Builder.restoreIP(OldIP);
}

void hydra::gpgpu::insertGPGPUTeardown(
  Function* Main,
  GPGPUData Data,
  bool AddErrorChecking
) {
  assert(Main->getParent() && "Main function does not belong to a module");

  TeardownReturnVisitor(Data, AddErrorChecking).visit(Main);
}

TeardownReturnVisitor::TeardownReturnVisitor(
  GPGPUData Data,
  bool AddErrorChecking
) :
  Data(Data),
  AddErrorChecking(AddErrorChecking)
{}

// Technically, C++ programs don't require main() to have a return statement
// for successful completion, but we don't have very good C++ support at the
// moment anyway so whatever.
void TeardownReturnVisitor::visitReturnInst(ReturnInst& InsertionPoint) {
  // C/C++ won't have instructions with the musttail attribute, so no need to
  // check for those in the end.
  TeardownInserter(&InsertionPoint, Data, AddErrorChecking).insert();
}

TeardownInserter::TeardownInserter(
  Instruction* InsertionPoint,
  GPGPUData Data,
  bool AddErrorChecking
) :
  Builder(InsertionPoint),
  Data(Data),
  AddErrorChecking(AddErrorChecking),
  InsMod(InsertionPoint->getModule())
{}

void TeardownInserter::insert() {
  Value* UnloadResult = Builder.CreateCall(
    getCuModuleUnload(InsMod),
    Data.Module
  );

  if (AddErrorChecking) {
    insertCudaCheckErrorCode(Builder, UnloadResult);
  }

  Value* DestroyResult = Builder.CreateCall(
    getCuCtxDestroy(InsMod),
    Data.Context
  );

  if (AddErrorChecking) {
    insertCudaCheckErrorCode(Builder, DestroyResult);
  }
}

GlobalVariable* hydra::gpgpu::createGlobalVariable(
  Module& M,
  Type* Ty,
  const Twine& Name
) {
  // The global variable should only be deleted when its parent module gets
  // destroyed.
  auto GV = new GlobalVariable(
    M,
    Ty,
    false,                        // not constant (because late-initialized)
    GlobalValue::PrivateLinkage,  // only using the variable in host module
    Constant::getNullValue(Ty),   // null works well as the "uninitialized" val
    Name
  );
  addToSkipMetadata(M, GV);

  return GV;
}

GlobalVariable* hydra::gpgpu::createKernelConstGlobal(
  Module& KernelModule,
  GlobalVariable* HostGlobal
) {
  return new GlobalVariable(
    KernelModule,
    HostGlobal->getValueType(),    // same type as host
    true,                          // constant global, of course
    GlobalValue::InternalLinkage,  // seems to be the appropriate choice
    HostGlobal->getInitializer(),  // same value as host
    HostGlobal->getName(),         // same name as host
    nullptr,                       // inserting at end of module's GV list
    GlobalValue::NotThreadLocal,
    ADDRSPACE_GLOBAL
  );
}

Function* hydra::gpgpu::createKernelFuncDecl(
  Module& KernelModule,
  const Function* HostFunction
) {
  auto F = Function::Create(
    HostFunction->getFunctionType(),
    HostFunction->getLinkage(),
    "__nv_" + HostFunction->getName(), // need libdevice version
    &KernelModule
  );

  // getting attributes (readnone, noreturn, etc.)
  //
  // Note that we don't currently copy personality functions over, so those
  // should be avoided for now (though we don't yet handle C++ stuff anyway so
  // whatever).
  F->copyAttributesFrom(HostFunction);

  // using same metadata just in case
  SmallVector<std::pair<unsigned, MDNode*>, BASE_SMALLVECTOR_SIZE> vec;
  HostFunction->getAllMetadata(vec);

  for (auto& MDPair : vec) {
    F->setMetadata(MDPair.first, MDPair.second);
  }

  return F;
}
