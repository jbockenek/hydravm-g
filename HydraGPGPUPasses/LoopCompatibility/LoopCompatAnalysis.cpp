//===- LoopCompatAnalysis.cpp - Analysis group for loop compat ------------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// See header file for details.
///
//===----------------------------------------------------------------------===//

#include "LoopCompatAnalysis.h"

#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Pass.h"

using namespace hydra::gpgpu;
using namespace llvm;

static RegisterAnalysisGroup<LoopCompatAnalysis>
X("Loop Compatibility Analysis");

char LoopCompatAnalysis::ID = 0;
LoopCompatAnalysis::LoopCompatAnalysis(LoopCompatAnalysisKind K) : Kind(K) {}

LoopCompatAnalysis::~LoopCompatAnalysis() {}

LoopCompatAnalysis::LoopCompatAnalysisKind LoopCompatAnalysis::getKind() const {
  return Kind;
}

void LoopCompatAnalysis::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
  AU.addRequired<ScalarEvolutionWrapperPass>();

  // All loop compatibility analyses chain (except the default)
  AU.addRequired<LoopCompatAnalysis>();

  // Analysis pass group
  AU.setPreservesAll();
}
