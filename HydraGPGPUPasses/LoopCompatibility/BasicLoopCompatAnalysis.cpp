//===- BasicLoopCompatAnalysis.cpp - Basic loop compat analysis -----------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Allows top-level loops that do not contain function calls (inlined function
/// calls are okay), have computible trip counts, and have one or fewer
/// subloops; uses of `new` may be handlable but are currently not actively
/// supported; however, they are not actively denied either, so this setup is
/// best used with just C programs for now.
///
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "basic-lca"

#include "CUDAMathFunctions.h"
#include "LoopCompatAnalysis.h"

#include "TemplatedUtils.h"

#include "llvm/ADT/Statistic.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/CallSite.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"

#include <utility>

STATISTIC(ExtractibleLoopCount, "Loops that can be extracted");

namespace llvm {
  class FunctionType;
}

using namespace hydra::gpgpu;
using namespace llvm;

namespace {
  class BasicLoopCompatAnalysis : public ModulePass, LoopCompatAnalysis {
    FuncWithLoopHeadersListType ExtractibleLoops;
    FuncsToConvertType FuncsToConvert;
    FuncsToConvertType IncompatibleFuncs;
    FuncsToConvertType CurrentFuncs;
    FuncsToConvertType CompatibleFuncDecls;
  public:
    static char ID; // Pass identification

    static bool classof(const LoopCompatAnalysis* LCA);

    BasicLoopCompatAnalysis();

    /// This default does not chain to other analysis passes so does not call
    /// through to its parent getAnalysisUsage().
    void getAnalysisUsage(AnalysisUsage& AU) const override;

    void* getAdjustedAnalysisPointer(AnalysisID PI) override;
    void print(raw_ostream& O, const Module* M) const override;

    /// \brief Resets the pass data.
    ///
    /// This doesn't actually free the memory used, but does allow us to reuse
    /// it.
    void releaseMemory() override;

    bool runOnModule(Module& M) override;
    bool isConvertible(ScalarEvolution& SE, const Loop* L);

    /// \brief Analyzes any calls/invokes in the supplied loop and recursively
    /// processes the indicated functions
    ///
    /// \return \c true if there are no call/invoke issues, \c false otherwise
    ///
    /// This only allows direct calls, indirect ones via non-constant pointers
    /// are forbidden.
    bool analyzeCallsAndInvokes(const Loop* L);

    /// \brief Recursively analyzes any calls/invokes in the supplied function
    ///
    /// \return \c true if all calls/invokes could be recursively converted,
    /// \c false otherwise
    ///
    /// Called functions can't have loops or recursion, nor can they reference
    /// non-constant globals. The main issue with contained loops is that it
    /// could get very difficult to check whether or not the loop trip count is
    /// invariant with respect to the caller, and it would be inefficient to
    /// allow the GPU to execute loops with varying trip counts in the same
    /// warps. Annoyingly, we can't just check for the existence of
    /// Attribute::ArgMemOnly on the function as we want to allow constant
    /// globals (and while that attribute may be determinable for functions that
    /// call other functions defined in the current module, it obviously won't
    /// work for declarations unless those declarations are themselves marked
    /// with ArgMemOnly; in the end, it's simplest to just check everything
    /// directly).
    ///
    /// This may result in some functions being added to the list for kernel
    /// conversion that will end up not being utilized in kernel form, which
    /// could increase kernel module load time at startup but otherwise should
    /// have minimal effect.
    bool analyzeCallsAndInvokes(const Function* F);

    /// \brief Analyzes any calls/invokes in the supplied basic block and
    /// recursively processes the indicated functions.
    bool analyzeCallsAndInvokes(const BasicBlock* F);

    FuncWithLoopHeadersListType& getExtractibleLoops() override;
    FuncsToConvertType& getFunctionsToConvert() override;
    FuncsToConvertType& getCompatFuncDecls() override;
  };

  /// \brief Class to simplify the returns in analyzeCallsAndInvokes()
  class SetPusher {
    FuncsToConvertType& Funcs;
    const Function* F;
  public:
    SetPusher(FuncsToConvertType& Funcs, const Function* F) :
      Funcs(Funcs),
      F(F)
    {}

    ~SetPusher() {
      Funcs.erase(F);
    }
  };
}

// non-CFG analysis pass
static RegisterPass<BasicLoopCompatAnalysis> DLCAPass(
  "basic-lca",
  "Basic loop compatibility analysis (default impl)",
  false,
  true
);

// Declaring that we implement the LoopCompatAnalysis interface and are the
// default
static RegisterAnalysisGroup<LoopCompatAnalysis, true> X(DLCAPass);

char BasicLoopCompatAnalysis::ID = 0;
BasicLoopCompatAnalysis::BasicLoopCompatAnalysis() :
  ModulePass(ID),
  LoopCompatAnalysis(LCAK_Basic)
{}

bool BasicLoopCompatAnalysis::classof(const LoopCompatAnalysis* LCA) {
  return LCA->getKind() == LCAK_Basic;
}

void* BasicLoopCompatAnalysis::getAdjustedAnalysisPointer(AnalysisID PI) {
  if (PI == &LoopCompatAnalysis::ID)
    return cast<LoopCompatAnalysis>(this);
  return this;
}

void BasicLoopCompatAnalysis::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
  AU.addRequired<ScalarEvolutionWrapperPass>();

  // analysis pass
  AU.setPreservesAll();
}

void BasicLoopCompatAnalysis::print(
  raw_ostream& O,
  const Module* /*M*/
) const {
  auto FuncCount = ExtractibleLoops.size();

  O << "Count of functions with extractible loops: " << FuncCount << '\n';
  O << "Number of extractible loops: " << ExtractibleLoopCount << '\n';
}

void BasicLoopCompatAnalysis::releaseMemory() {
  ExtractibleLoops.clear();
  FuncsToConvert.clear();
  IncompatibleFuncs.clear();
  CurrentFuncs.clear(); // may be unneeded as execution will clear it naturally
}

bool BasicLoopCompatAnalysis::runOnModule(Module& M) {
  // Resetting the loop list as passes are basically singletons.
  releaseMemory();

  // checking which functions contain loops first; this way, we have all the
  // info we need to avoid passes with loops in the recursive function checking.
  for (Function& F : M.functions()) {
    // Function passes often fail on declarations
    if (F.isDeclaration()) {
      IncompatibleFuncs.insert(&F);
      continue;
    }

    auto& LI = getAnalysis<LoopInfoWrapperPass>(F).getLoopInfo();

    if (!LI.empty()) {
      // There are loops, this function cannot itself be converted
      IncompatibleFuncs.insert(&F);
    }
  }

  // The repeat of loop analysis is annoying, but it must be done as internal
  // pass data is essentially singletonian so we can't just hold on to the loop
  // info from the last runthrough.
  for (Function& F : M.functions()) {
    if (F.isDeclaration())
      continue;

    auto& LI = getAnalysis<LoopInfoWrapperPass>(F).getLoopInfo();
    auto& SE = getAnalysis<ScalarEvolutionWrapperPass>(F).getSE();

    LoopHeaderListType LoopHeaders;
    for (Loop* L : LI) { // iterating over top-level loops
      if (!isConvertible(SE, L)) {
        continue;
      }

      ++ExtractibleLoopCount;
      LoopHeaders.push_back(L->getHeader());
    }

    ExtractibleLoops.emplace_back(&F, std::move(LoopHeaders));
  }

  // Analysis-only
  return false;
}

// We can't convert loops that have non-computable trip counts or multiple
// nested loops on the same level to GPU kernels (we might be able to separate
// or combine the subloops somehow, though).
//
// TODO: determine if a loop's trip count is invariant for parent loops as well
// (necessary for kernels created from nested loops that do not utilize dynamic
// parallelism)
bool BasicLoopCompatAnalysis::isConvertible(
  ScalarEvolution& SE, const Loop* L
) {
  // in case the loop-simplify pass didn't work
  if (!L->isLoopSimplifyForm())
    return false;

  if (!SE.hasLoopInvariantBackedgeTakenCount(L))
    return false;

  if (!L->getCanonicalInductionVariable())
    return false;

  // Not convertible if more than one subloop
  const std::vector<Loop*>& subLoops = L->getSubLoops();
  auto size = subLoops.size();
  if (size > 1) return false;

  // Only checking calls/invokes for outermost loop as it also checks the
  // instructions for inner loops; this check can take longer than the subloop
  // count check, so placing after to slightly reduce amortized cost.
  if (L->getLoopDepth() == 1 && !analyzeCallsAndInvokes(L))
    return false;

  // bypass recursion if no (more) subloops
  if (size == 0) return true;

  return isConvertible(SE, subLoops[0]);
}

// InstVisitor could be useful here
bool BasicLoopCompatAnalysis::analyzeCallsAndInvokes(const Loop* L) {
  for (const BasicBlock* BB : L->blocks()) {
    if (!analyzeCallsAndInvokes(BB))
      return false;
  }

  return true;
}

bool BasicLoopCompatAnalysis::analyzeCallsAndInvokes(const BasicBlock* BB) {
  for (const Instruction& I : BB->getInstList()) {
    // intrinsics are fine, LLVM will handle those itself
    if (isa<IntrinsicInst>(I))
      continue;

     // CallSite allows supplying non-call/-invoke; if so, it just stores null
    const ImmutableCallSite CS(&I);
    if (CS && !analyzeCallsAndInvokes(CS.getCalledFunction()))
      return false;
  }

  return true;
}

bool BasicLoopCompatAnalysis::analyzeCallsAndInvokes(const Function* F) {
  // rejecting indirect calls
  if (!F)
    return false;

  if (F->isDeclaration()) {
    // Assuming that if a function declaration's name matches a supported CUDA
    // function and has the same type (return type, arg count, arg types), we
    // can let it be executed as that function. Right now only exact matches are
    // be supported, but support for mapping from C/C++ math functions not in
    // the CUDA math libraries to their CUDA versions would be great.
    FunctionType* FTy = getCUDAMathFunTy(F->getName(), F->getContext());
    if (F->getFunctionType() == FTy) {
      CompatibleFuncDecls.insert(F);
      return true;
    }

    // TODO: abstract away the underlying architecture so we can use this with
    // OpenCL, etc. eventually

    return false;
  }

  // rejecting already-checked bad functions and recursion
  if (IncompatibleFuncs.count(F) > 0 || CurrentFuncs.count(F) > 0)
    return false;

  // skipping functions already known to be convertible (reduces processing time
  // in the case of commonly-used helper functions)
  if (FuncsToConvert.count(F) > 0)
    return true;

  SetPusher X(CurrentFuncs, F);
  for (const BasicBlock& BB : F->getBasicBlockList()) {
    if (!analyzeCallsAndInvokes(&BB)) {
      IncompatibleFuncs.insert(F);
      return false;
    }
  }

  FuncsToConvert.insert(F);
  return true;
}

FuncWithLoopHeadersListType& BasicLoopCompatAnalysis::getExtractibleLoops() {
  return ExtractibleLoops;
}

FuncsToConvertType& BasicLoopCompatAnalysis::getFunctionsToConvert() {
  return FuncsToConvert;
}

FuncsToConvertType& BasicLoopCompatAnalysis::getCompatFuncDecls() {
  return CompatibleFuncDecls;
}
