//===- BasicACAFunctionPass.cpp - Function pass for Basic ACA -------------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Definitions for BasicACAFunctionPass.h
///
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "basic-aca"

#include "BasicACAFunctionPass.h"

#include "HydraGPGPUConstants.h"
#include "TemplatedUtils.h"

#include "llvm/ADT/Statistic.h"
#include "llvm/Analysis/MemoryBuiltins.h"
#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/IR/CallSite.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/IR/Type.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/raw_ostream.h"

#include <algorithm>

STATISTIC(NumAllocas, "number of allocas reported");
STATISTIC(NumMallocs, "number of malloc-likes reported");
STATISTIC(NumCallocs, "number of calloc-likes reported");
STATISTIC(NumReallocs, "number of reallocs reported");
STATISTIC(NumReallocfs, "number of reallocfs reported");
STATISTIC(NumNews, "number of (may-throw) news reported");
STATISTIC(NumNothrowNews, "number of (nothrow) news reported");
STATISTIC(NumFrees, "number of frees reported");
STATISTIC(NumDeletes, "number of (may-throw) deletes reported");
STATISTIC(NumNothrowDeletes, "number of (nothrow) deletes reported");
STATISTIC(NumStrdups, "number of strdups reported");
STATISTIC(NumStrndups, "number of strndups reported");

using namespace hydra::gpgpu;
using namespace llvm;

/// \c AllocType, \c AllocFnsTy, \c AllocationFnData, \c getCalledFunction(),
/// and \c getAllocationData() were copied almost verbatim from
/// MemoryBuiltins.cpp as they are inaccessible outside of that compilation
/// unit but are needed to reimplement \c isOperatorNewLikeFn() and
/// \c isReallocLikeFn() easily (and potentially other functions from
/// MemoryBuiltins.h if the developers keep removing this stuff; if it does get
/// all removed eventually, here's a version from a commit that still had that
/// code: https://github.com/llvm-mirror/llvm/blob/3edb0ec2294648ab5d8779eb9e16f28b6e3e5ce2/lib/Analysis/MemoryBuiltins.cpp
namespace {
  enum AllocType : uint8_t {
    OpNewLike   = 1<<0, // allocates; never returns null
    ReallocLike = 1<<3  // reallocates
  };

  struct AllocFnsTy {
    LibFunc::Func Func;
    AllocType AllocTy;
    unsigned char NumParams;
    // First and Second size parameters (or -1 if unused)
    signed char FstParam, SndParam;
  };

  const AllocFnsTy AllocationFnData[] = {
    {LibFunc::Znwj,                    OpNewLike,   1, 0,  -1}, // new(unsigned int)
    {LibFunc::Znwm,                    OpNewLike,   1, 0,  -1}, // new(unsigned long)
    {LibFunc::Znaj,                    OpNewLike,   1, 0,  -1}, // new[](unsigned int)
    {LibFunc::Znam,                    OpNewLike,   1, 0,  -1}, // new[](unsigned long)
    {LibFunc::msvc_new_int,            OpNewLike,   1, 0,  -1}, // new(unsigned int)
    {LibFunc::msvc_new_longlong,       OpNewLike,   1, 0,  -1}, // new(unsigned long long)
    {LibFunc::msvc_new_array_int,      OpNewLike,   1, 0,  -1}, // new[](unsigned int)
    {LibFunc::msvc_new_array_longlong, OpNewLike,   1, 0,  -1}, // new[](unsigned long long)
    {LibFunc::realloc,                 ReallocLike, 2, 1,  -1},
    {LibFunc::reallocf,                ReallocLike, 2, 1,  -1}
  };

  Function* getCalledFunction(const Value* V, bool LookThroughBitCast) {
    if (LookThroughBitCast)
      V = V->stripPointerCasts();

    CallSite CS(const_cast<Value*>(V));
    if (!CS.getInstruction())
      return nullptr;

    Function *Callee = CS.getCalledFunction();
    if (!Callee || !Callee->isDeclaration()) // we can't replace defined funcs
      return nullptr;

    return Callee;
  }

/// \brief Returns the allocation data for the given value if it is a call to a
/// known allocation function, and NULL otherwise.
const AllocFnsTy* getAllocationData(
  const Value* V,
  AllocType AllocTy,
  const TargetLibraryInfo* TLI,
  bool LookThroughBitCast=false
) {
  // Skip intrinsics
  if (isa<IntrinsicInst>(V))
    return nullptr;

  Function* Callee = getCalledFunction(V, LookThroughBitCast);
  if (!Callee)
    return nullptr;

  // Make sure that the function is available.
  StringRef FnName = Callee->getName();
  LibFunc::Func TLIFn;
  if (!TLI || !TLI->getLibFunc(FnName, TLIFn) || !TLI->has(TLIFn))
    return nullptr;

  auto End = std::end(AllocationFnData);
  const AllocFnsTy* FnData = std::find_if(
    std::begin(AllocationFnData),
    End,
    [TLIFn](const AllocFnsTy& Fn) { return Fn.Func == TLIFn; }
  );

  if (FnData == End)
    return nullptr;

  if ((FnData->AllocTy & AllocTy) != FnData->AllocTy)
    return nullptr;

  // Check function prototype.
  int FstParam = FnData->FstParam;
  int SndParam = FnData->SndParam;
  FunctionType* FTy = Callee->getFunctionType();

  if (
    FTy->getReturnType() == Type::getInt8PtrTy(FTy->getContext()) &&
    FTy->getNumParams() == FnData->NumParams && (
      FstParam < 0 || (
        FTy->getParamType(FstParam)->isIntegerTy(32) ||
        FTy->getParamType(FstParam)->isIntegerTy(64)
      )
    ) && (
      SndParam < 0 ||
      FTy->getParamType(SndParam)->isIntegerTy(32) ||
      FTy->getParamType(SndParam)->isIntegerTy(64)
    )
  ) {
    return FnData;
  }

  return nullptr;
}

  /// \brief Tests if a value is a call or invoke to a library function that
  /// allocates memory and never returns null (such as operator new).
  ///
  /// This was removed from MemoryBuiltins.h so we had to reimplement it. If
  /// they remove too much of the public stuff, we may have to just copy over
  /// all the background stuff to simplify these functions.
  bool isOperatorNewLikeFn(
    const Value* V,
    const TargetLibraryInfo* TLI,
    bool LookThroughBitCast=false
  ) {
    return getAllocationData(V, OpNewLike, TLI, LookThroughBitCast);
  }

  /// \brief Tests if a value is a call or invoke to a library function that
  /// reallocates memory (such as realloc).
  ///
  /// This was removed from MemoryBuiltins.h so we had to reimplement it.
  bool isReallocLikeFn(
    const Value* V,
    const TargetLibraryInfo* TLI,
    bool LookThroughBitCast=false
  ) {
    return getAllocationData(V, ReallocLike, TLI, LookThroughBitCast);
  }

  class CallSiteVisitor : public InstVisitor<CallSiteVisitor> {
    BasicACAFunctionPass& DACA;
  public:
    CallSiteVisitor(BasicACAFunctionPass& DACA);

    void visitAllocaInst(AllocaInst &AI);
    void visitCallSite(CallSite CS);
  };

  enum class FreeLike {
    Free,
    Delete,
    NothrowDelete,
    None
  };

  bool isInsertedAlloca(AllocaInst& AI) {
    return AI.getMetadata(hydra::gpgpu::MD_ALLOCA) != nullptr;
  }

  /// \brief Modified version of MemoryBuiltin's isFreeCall; we make a couple
  /// assumptions based on the context (i.e. not an intrinsic, we have a
  /// call site for it, etc.)
  ///
  /// \return the type of free-like function (FreeLike::None for not free-like)
  FreeLike isFreeLikeFn(CallSite CS, const TargetLibraryInfo* TLI) {
    Function *Callee = CS.getCalledFunction();
    if (Callee == nullptr) // no indirect calls
      return FreeLike::None;

    StringRef FnName = Callee->getName();
    LibFunc::Func TLIFn;

    // MAYBE: may need to find some other way of doing this given TLI seems to
    // be for builtins specifically, not general library functions.
    if (!TLI->getLibFunc(FnName, TLIFn) || !TLI->has(TLIFn))
      return FreeLike::None;

    unsigned ExpectedNumParams;
    switch (TLIFn) {
      case LibFunc::free:
      case LibFunc::ZdlPv:               // operator delete(void*)
      case LibFunc::ZdaPv:               // operator delete[](void*)
        ExpectedNumParams = 1;
        break;
      case LibFunc::ZdlPvj:              // delete(void*, uint)
      case LibFunc::ZdlPvm:              // delete(void*, ulong)
      case LibFunc::ZdlPvRKSt9nothrow_t: // delete(void*, nothrow)
      case LibFunc::ZdaPvj:              // delete[](void*, uint)
      case LibFunc::ZdaPvm:              // delete[](void*, ulong)
      case LibFunc::ZdaPvRKSt9nothrow_t: // delete[](void*, nothrow)
        ExpectedNumParams = 2;
        break;
      default:
        return FreeLike::None;
    }

    // Check free prototype.
    FunctionType *FTy = Callee->getFunctionType();
    if (!FTy->getReturnType()->isVoidTy())
      return FreeLike::None;

    if (FTy->getNumParams() != ExpectedNumParams)
      return FreeLike::None;

    if (FTy->getParamType(0) != Type::getInt8PtrTy(Callee->getContext()))
      return FreeLike::None;

    // a bit redundant, but this way we specify the type.

    switch (TLIFn) {
      case LibFunc::free:
        return FreeLike::Free;
      case LibFunc::ZdlPv:               // operator delete(void*)
      case LibFunc::ZdaPv:               // operator delete[](void*)
      case LibFunc::ZdlPvj:              // delete(void*, uint)
      case LibFunc::ZdlPvm:              // delete(void*, ulong)
      case LibFunc::ZdaPvj:              // delete[](void*, uint)
      case LibFunc::ZdaPvm:              // delete[](void*, ulong)
        return FreeLike::Delete;
      case LibFunc::ZdlPvRKSt9nothrow_t: // delete(void*, nothrow)
      case LibFunc::ZdaPvRKSt9nothrow_t: // delete[](void*, nothrow)
        return FreeLike::NothrowDelete;
      default:
        llvm_unreachable(
          "Proper type should have been determined by this point."
        );
        return FreeLike::None;
    }
  }
}

// non-CFG analysis pass
static RegisterPass<BasicACAFunctionPass> X(
  "basic-aca-func",
  "Basic allocation conversion analysis function pass",
  false,
  true
);

char BasicACAFunctionPass::ID = 0;
BasicACAFunctionPass::BasicACAFunctionPass() : FunctionPass(ID) {}

void BasicACAFunctionPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<TargetLibraryInfoWrapperPass>();
  AU.setPreservesAll(); // analysis pass
}

bool BasicACAFunctionPass::runOnFunction(Function& F) {
  // Resetting the lists as passes are basically singletons.
  releaseMemory();

  // For correctness purposes, the fallback functions must not have any
  // contained allocations analyzed.
  if (F.hasFnAttribute(HYDRA_FALLBACK_ATTR))
    return false;

  CallSiteVisitor CSV(*this);
  CSV.visit(F);

  // analysis pass, no changes made
  return false;
}

void BasicACAFunctionPass::print(
  raw_ostream& O,
  const Module* /*M*/
) const {
  O << "Allocas: " << Allocas << '\n';
  O << "Mallocs: " << Mallocs << '\n';
  O << "Callocs: " << Callocs << '\n';
  O << "Reallocs: " << Reallocs << '\n';
  O << "Reallocsf: " << Reallocfs << '\n';
  O << "News: " << News << '\n';
  O << "NothrowNews: " << NothrowNews << '\n';
  O << "Frees: " << Frees << '\n';
  O << "Deletes: " << Deletes << '\n';
  O << "NothrowDeletes: " << NothrowDeletes << '\n';
  O << "Strdups: " << Strdups << '\n';
  O << "Strndups: " << Strndups << '\n';
}

void BasicACAFunctionPass::releaseMemory() {
  Allocas.clear();
  Mallocs.clear();
  Callocs.clear();
  Reallocs.clear();
  Reallocfs.clear();
  News.clear();
  NothrowNews.clear();
  Frees.clear();
  Deletes.clear();
  NothrowDeletes.clear();
  Strdups.clear();
  Strndups.clear();
}

AllocaInstListType& BasicACAFunctionPass::getAllocas() {
  return Allocas;
}

CallSiteListType& BasicACAFunctionPass::getMallocs() {
  return Mallocs;
}

CallSiteListType& BasicACAFunctionPass::getCallocs() {
  return Callocs;
}

CallSiteListType& BasicACAFunctionPass::getReallocs() {
  return Reallocs;
}

CallSiteListType& BasicACAFunctionPass::getReallocfs() {
  return Reallocfs;
}

CallSiteListType& BasicACAFunctionPass::getNews() {
  return News;
}

CallSiteListType& BasicACAFunctionPass::getNothrowNews() {
  return NothrowNews;
}

CallSiteListType& BasicACAFunctionPass::getFrees() {
  return Frees;
}

CallSiteListType& BasicACAFunctionPass::getDeletes() {
  return Deletes;
}

CallSiteListType& BasicACAFunctionPass::getNothrowDeletes() {
  return NothrowDeletes;
}

CallSiteListType& BasicACAFunctionPass::getStrdups() {
  return Strdups;
}

CallSiteListType& BasicACAFunctionPass::getStrndups() {
  return Strndups;
}

CallSiteVisitor::CallSiteVisitor(BasicACAFunctionPass& DACA) : DACA(DACA) {}

void CallSiteVisitor::visitAllocaInst(AllocaInst& AI) {
  if (isInsertedAlloca(AI))
    return;

  ++NumAllocas;
  DACA.getAllocas().push_back(&AI);
}

void CallSiteVisitor::visitCallSite(CallSite CS) {
  const auto TLI = &DACA.getAnalysis<TargetLibraryInfoWrapperPass>().getTLI();
  const auto I = CS.getInstruction();

  // Intrinsics aren't builtins.
  if (isa<IntrinsicInst>(I))
    return;

  switch (isFreeLikeFn(CS, TLI)) {
    case FreeLike::Free:
      ++NumFrees;

      DACA.getFrees().push_back(CS);
      return;
    case FreeLike::Delete:
      ++NumDeletes;

      DACA.getDeletes().push_back(CS);
      return;
    case FreeLike::NothrowDelete:
      ++NumNothrowDeletes;

      DACA.getNothrowDeletes().push_back(CS);
      return;
    case FreeLike::None: // not a FreeLike function
      break;
  }

  if (isOperatorNewLikeFn(I, TLI)) {
    ++NumNews;

    DACA.getNews().push_back(CS);
  } else if (isMallocLikeFn(I, TLI)) {
    if (CS.getFunctionType()->getNumParams() == 2) { // nothrow new
      ++NumNothrowNews;

      DACA.getNothrowNews().push_back(CS);
    } else {
      ++NumMallocs;

      DACA.getMallocs().push_back(CS);
    }
  } else if (isCallocLikeFn(I, TLI)) {
    ++NumCallocs;

    DACA.getCallocs().push_back(CS);
  } else if (isReallocLikeFn(I, TLI)) {
    auto F = CS.getCalledFunction();
    assert(F && "Can't be an indirect call if it's realloc-like.");

    // C library functions get the C name even if prepended with std:: in C++
    if (F->getName() == "reallocf") {
      ++NumReallocfs;

      DACA.getReallocfs().push_back(CS);
    } else {
      ++NumReallocs;

      DACA.getReallocs().push_back(CS);
    }
  } else if (isAllocLikeFn(I, TLI)) { // must be strdup/strndup
    auto F = CS.getCalledFunction();
    assert(F && "Can't be an indirect call if it's alloc-like.");

    StringRef Name = F->getName();
    if (Name == "strdup") {
      ++NumStrdups;

      DACA.getStrdups().push_back(CS);
    } else if (Name == "strndup") {
      ++NumStrndups;

      DACA.getStrndups().push_back(CS);
    } else {
      llvm_unreachable("Found an unknown AllocLike function");
    }
  }
}
