//===- ConstantExprConverter.cpp - Converts ConstantExpr to Inst ----------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Constant::destroyConstant() may be useful here at some point, but it may not
/// be worthwhile given it requires all users to also be constants. It may be
/// useful for eliminating CEs that have had all their uses removed, though, so
/// that we don't need EliminatedExprs (and can thus reduce memory usage).
///
//===----------------------------------------------------------------------===//

// MAYBE: test if using destroyConstant() will achieve the same behavior as
// the current usage with EliminatedExprs.

#include "Utils/ConstantExprConverter.h"

#include "llvm/ADT/SmallSet.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Use.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Value.h"

using namespace llvm;

void hydra::gpgpu::ConstantExprConverter::reset() {
  Exprs.clear();
  EliminatedExprs.clear();
}

void hydra::gpgpu::ConstantExprConverter::convertUsers(Value* RootValue) {
  reset();
  addExprsToStack(RootValue);

  while (!Exprs.empty()) {
    ConstantExpr* CE = Exprs.top();

    if (!addExprsToStack(CE)) {
      // All ConstantExpr users of the current expression, if any, have been
      // converted to instructions; we can now do the same for the current
      // expression.
      Exprs.pop();
      replaceUses(CE);
    }
  }
}

bool hydra::gpgpu::ConstantExprConverter::addExprsToStack(Value* Val) {
  UniqueUsers.clear(); // unique-user set resets for each value checked
  bool added = false;
  bool needsCheck = true;

  while (needsCheck) {
    needsCheck = false;

    for (Use& U : Val->uses()) {
      User* Us = U.getUser();
      if (UniqueUsers.count(Us) > 0) // skipping if we already checked the user
        continue;

      UniqueUsers.insert(Us);

      if (auto CE = dyn_cast<ConstantExpr>(Us)) {
        if (!EliminatedExprs.count(CE) && Exprs.push(CE)) {
          added = true;
        } else { // we have a back- or broken edge! need to get rid of the use
          U = nullptr;       // setting this means changing the for loop;
          needsCheck = true; // thus, we need to restart the loop
          break;
        }
      } else {
        assert(!isa<Constant>(Us) && "Users cannot be non-expr constants");
      }
    }
  }

  return added;
}

void hydra::gpgpu::ConstantExprConverter::replaceUses(ConstantExpr* CE) {
  EliminatedExprs.insert(CE);

  while (!CE->use_empty()) {
    auto& U = *CE->use_begin();
    auto UsingInst = cast<Instruction>(U.getUser());
    auto NewInst = CE->getAsInstruction();

    // We can't update value handles or metadata properly as the single CE is
    // being replaced by multiple instructions rather than just one, so we
    // just won't worry about it.

    NewInst->insertBefore(UsingInst);
    U = NewInst;
  }
}
