//===- BasicAllocConvAnalysis.cpp - Basic allocation analysis -------------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// See header file for details.
///
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "basic-aca"

#include "BasicAllocConvAnalysis.h"

#include "BasicACAFunctionPass.h"
#include "HydraGPGPUConstants.h"
#include "TemplatedUtils.h"

#include "llvm/ADT/SmallSet.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/raw_ostream.h"

#include <set>
#include <string>

STATISTIC(NumGlobals, "number of globals reported");
STATISTIC(NumConstGlobals, "number of constant globals reported");

using namespace hydra::gpgpu;
using namespace llvm;

namespace {
  using GVSet = SmallSet<GlobalVariable*, BASE_SMALLSET_SIZE>;

  GVSet getGVsToSkip(const Module& M) {
    GVSet GVsToSkip;
    NamedMDNode* MDGlobalWrapper = M.getNamedMetadata(MD_GLOBALS);

    if (MDGlobalWrapper) { // skip if no list of globals to skip
      // NamedMDNodes can only hold MDNodes, no leaf metadata
      MDNode* MDGlobals = MDGlobalWrapper->getOperand(0);

      // Unwrap global variables and insert into set
      for (Metadata* Op : MDGlobals->operands()) {
        Constant* Global = cast<ConstantAsMetadata>(Op)->getValue();
        GVsToSkip.insert(cast<GlobalVariable>(Global));
      }
    }

    return GVsToSkip;
  }

  std::set<std::string> IgnorableGlobals{
    "_ZSt4cerr",
    "_ZSt4cout",
    "_ZSt3cin",
    "stdout",
    "stderr",
    "stdin",
    "__dso_handle"
  };

  // There are a number of globals we cannot convert due to being external but
  // are used or implicit in many C++ programs, and even in many C programs (
  // std::c{out,err,in} and __dso_handle for C++, std{out,err,in} for C);
  // luckily, as most of those would end up being passed to functions anyway
  // and indirectly accessing such globals in a way that requires their value
  // would be absurd, we can just ignore them. (Of course, someone could use
  // the same names as the standard file handles for their own variables, but
  // that would also be an issue during regular compilation so we won't worry
  // about that).
  bool isIgnorableGlobal(const GlobalVariable& GV) {
    return IgnorableGlobals.count(GV.getName()) > 0;
  }
}

// non-CFG analysis pass
static RegisterPass<BasicAllocConvAnalysis> DACAPass(
  "basic-aca",
  "Basic allocation conversion analysis (default impl)",
  false,
  true
);

// Declaring that we implement the AllocConvAnalysis interface and are the
// default
static RegisterAnalysisGroup<AllocConvAnalysis, true> X(DACAPass);

char BasicAllocConvAnalysis::ID = 0;
BasicAllocConvAnalysis::BasicAllocConvAnalysis() :
  ModulePass(ID),
  AllocConvAnalysis(ACAK_Basic),
  LastFunction(nullptr),
  AnalysisResult(nullptr)
{}

bool BasicAllocConvAnalysis::classof(const AllocConvAnalysis* ACA) {
  return ACA->getKind() == ACAK_Basic;
}

void* BasicAllocConvAnalysis::getAdjustedAnalysisPointer(AnalysisID PI) {
  if (PI == &AllocConvAnalysis::ID)
    return cast<AllocConvAnalysis>(this);
  return this;
}

void BasicAllocConvAnalysis::getAnalysisUsage(AnalysisUsage& AU) const {
  // addRequiredTransitive() is not needed for on-the-fly passes as they get
  // recalculated on the fly anyway; addRequired(), however, is.
  AU.addRequired<BasicACAFunctionPass>();

  // analysis pass
  AU.setPreservesAll();
}

bool BasicAllocConvAnalysis::runOnModule(Module& M) {
  // Resetting the lists as passes are basically singletons.
  releaseMemory();

  // Don't want to convert global variables added to the module by GPGPU
  // extraction/conversion passes to device allocations
  GVSet GVsToSkip = getGVsToSkip(M);

  for (GlobalVariable& GV : M.getGlobalList()) {
    if (isIgnorableGlobal(GV))
      continue;

    assert(
      GV.hasInitializer() &&
      "We can't deal with declaration-only, external globals right now"
    );

    if (GVsToSkip.count(&GV))
      continue;

    // Globals lists hold pointers to GVs
    if (GV.isConstant()) {
      ++NumConstGlobals;
      ConstGlobals.push_back(&GV);
    } else {
      ++NumGlobals;
      Globals.push_back(&GV);
    }
  }

  // Analysis-only
  return false;
}

void BasicAllocConvAnalysis::print(
  raw_ostream& O,
  const Module* /*M*/
) const {
  O << "Globals: " << Globals << '\n';
  O << "Constant globals: " << ConstGlobals << '\n';
}

void BasicAllocConvAnalysis::releaseMemory() {
  Globals.clear();
  ConstGlobals.clear();
}

void BasicAllocConvAnalysis::updateAnalysisIfNeeded(Function& NextFunction) {
  if (LastFunction != &NextFunction) {
    LastFunction = &NextFunction;
    AnalysisResult = &getAnalysis<BasicACAFunctionPass>(NextFunction);
  }
}

AllocaInstListType& BasicAllocConvAnalysis::getAllocas(Function& F) {
  updateAnalysisIfNeeded(F);

  return AnalysisResult->getAllocas();
}

CallSiteListType& BasicAllocConvAnalysis::getMallocs(Function& F) {
  updateAnalysisIfNeeded(F);

  return AnalysisResult->getMallocs();
}

CallSiteListType& BasicAllocConvAnalysis::getCallocs(Function& F) {
  updateAnalysisIfNeeded(F);

  return AnalysisResult->getCallocs();
}

CallSiteListType& BasicAllocConvAnalysis::getReallocs(Function& F) {
  updateAnalysisIfNeeded(F);

  return AnalysisResult->getReallocs();
}

CallSiteListType& BasicAllocConvAnalysis::getReallocfs(Function& F) {
  updateAnalysisIfNeeded(F);

  return AnalysisResult->getReallocfs();
}

CallSiteListType& BasicAllocConvAnalysis::getNews(Function& F) {
  updateAnalysisIfNeeded(F);

  return AnalysisResult->getNews();
}

CallSiteListType& BasicAllocConvAnalysis::getNothrowNews(Function& F) {
  updateAnalysisIfNeeded(F);

  return AnalysisResult->getNothrowNews();
}

CallSiteListType& BasicAllocConvAnalysis::getFrees(Function& F) {
  updateAnalysisIfNeeded(F);

  return AnalysisResult->getFrees();
}

CallSiteListType& BasicAllocConvAnalysis::getDeletes(Function& F) {
  updateAnalysisIfNeeded(F);

  return AnalysisResult->getDeletes();
}

CallSiteListType& BasicAllocConvAnalysis::getNothrowDeletes(Function& F) {
  updateAnalysisIfNeeded(F);

  return AnalysisResult->getNothrowDeletes();
}

CallSiteListType& BasicAllocConvAnalysis::getStrdups(Function& F) {
  updateAnalysisIfNeeded(F);

  return AnalysisResult->getStrdups();
}

CallSiteListType& BasicAllocConvAnalysis::getStrndups(Function& F) {
  updateAnalysisIfNeeded(F);

  return AnalysisResult->getStrndups();
}

GlobalVariableListType& BasicAllocConvAnalysis::getGlobals() {
  return Globals;
}

GlobalVariableListType& BasicAllocConvAnalysis::getConstGlobals() {
  return ConstGlobals;
}
