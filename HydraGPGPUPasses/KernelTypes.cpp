//===- KernelTypes.cpp - CUDA kernel info & launch data types -------------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains definitions of various helper structs and classes
/// utilized for kernel characterization and launching.
///
//===----------------------------------------------------------------------===//

#include "KernelTypes.h"

#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/ScalarEvolutionExpander.h"
#include "llvm/IR/Attributes.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/ValueSymbolTable.h"

using namespace llvm;

namespace {
    class CheckFuncBuilderHelper {
    IRBuilder<> Builder;
    Value* GridX;
    Value* GridY;
    Value* GridZ;
    Value* BlockX;
    Value* BlockY;
    Value* BlockZ;
    Constant* GridXMax;
    Constant* GridYMax;
    Constant* GridZMax;
    Constant* BlockXMax;
    Constant* BlockYMax;
    Constant* BlockZMax;
    Constant* PerBlockThreadCountMax;

    Value* insertTooLargeCheck();
    Value* insertTooSmallCheck();
  public:
    CheckFuncBuilderHelper(BasicBlock* BB, ValueSymbolTable& VST);

    IRBuilder<>& build();
  };
}

CheckFuncBuilderHelper::CheckFuncBuilderHelper(
  BasicBlock* BB,
  ValueSymbolTable& VST
) :
  Builder(BB),
  GridX(VST.lookup("GridX")),
  GridY(VST.lookup("GridY")),
  GridZ(VST.lookup("GridZ")),
  BlockX(VST.lookup("BlockX")),
  BlockY(VST.lookup("BlockY")),
  BlockZ(VST.lookup("BlockZ")),
  GridXMax(Builder.getInt32(hydra::gpgpu::KernelDims::GridXMax)),
  GridYMax(Builder.getInt32(hydra::gpgpu::KernelDims::GridYMax)),
  GridZMax(Builder.getInt32(hydra::gpgpu::KernelDims::GridZMax)),
  BlockXMax(Builder.getInt32(hydra::gpgpu::KernelDims::BlockXMax)),
  BlockYMax(Builder.getInt32(hydra::gpgpu::KernelDims::BlockYMax)),
  BlockZMax(Builder.getInt32(hydra::gpgpu::KernelDims::BlockZMax)),
  PerBlockThreadCountMax(
    Builder.getInt32(hydra::gpgpu::KernelDims::PerBlockThreadCountMax)
  )
{}

// Actually builds the function
IRBuilder<>& CheckFuncBuilderHelper::build() {
  // Upper bounds check
  Value* NotTooLarge = insertTooLargeCheck();

  // Lower bounds check
  Value* NotTooSmall = insertTooSmallCheck();

  // Dims are in bounds if both above checks result in `true`
  Value* InBounds = Builder.CreateAnd(NotTooLarge, NotTooSmall, "inBounds");

  // Finishing touch, returning the bounds check result
  Builder.CreateRet(InBounds);

  // returning the builder (by reference) for potential external usage
  return Builder;
}

Value* CheckFuncBuilderHelper::insertTooLargeCheck() {
  // Individual greater-than checks
  Value* CmpGx = Builder.CreateICmpUGT(GridX, GridXMax, "cmp.lg.gx");
  Value* CmpGy = Builder.CreateICmpUGT(GridY, GridYMax, "cmp.lg.gy");
  Value* CmpGz = Builder.CreateICmpUGT(GridZ, GridZMax, "cmp.lg.gz");
  Value* CmpBx = Builder.CreateICmpUGT(BlockX, BlockXMax, "cmp.lg.bx");
  Value* CmpBy = Builder.CreateICmpUGT(BlockY, BlockYMax, "cmp.lg.by");
  Value* CmpBz = Builder.CreateICmpUGT(BlockZ, BlockZMax, "cmp.lg.bz");

  // BlockX * BlockY * BlockZ > PerBlockThreadCountMax
  Value* MulBxy = Builder.CreateMul(BlockX, BlockY, "mul.bx.by");
  Value* MulBxyz = Builder.CreateMul(MulBxy, BlockZ, "mul.bxy.bz");
  Value* CmpT = // T for total thread count
    Builder.CreateICmpUGT(MulBxyz, PerBlockThreadCountMax, "cmp.lg.bt");

  // OR all the Cmps together
  Value* OrGxy = Builder.CreateOr(CmpGx, CmpGy, "or.lg.gx.gy");
  Value* OrGxyz = Builder.CreateOr(OrGxy, CmpGz, "or.lg.gxy.gz");
  Value* OrGxyzBx = Builder.CreateOr(OrGxyz, CmpBx, "or.lg.gxyz.bx");
  Value* OrGxyzBxy = Builder.CreateOr(OrGxyzBx, CmpBy, "or.lg.gxyzbx.by");
  Value* OrGxyzBxyz = Builder.CreateOr(OrGxyzBxy, CmpBz, "or.lg.gxyzbxy.bz");
  Value* OrGxyzBxyzT = Builder.CreateOr(OrGxyzBxyz, CmpT, "or.lg.gxyzbxyz.bt");

  return Builder.CreateIsNull(OrGxyzBxyzT, "notTooLarge");
}

Value* CheckFuncBuilderHelper::insertTooSmallCheck() {
  // Zero-checks
  Value* CmpGx = Builder.CreateIsNull(GridX, "cmp.sm.gx");
  Value* CmpGy = Builder.CreateIsNull(GridY, "cmp.sm.gy");
  Value* CmpGz = Builder.CreateIsNull(GridZ, "cmp.sm.gz");
  Value* CmpBx = Builder.CreateIsNull(BlockX, "cmp.sm.bx");
  Value* CmpBy = Builder.CreateIsNull(BlockY, "cmp.sm.by");
  Value* CmpBz = Builder.CreateIsNull(BlockZ, "cmp.sm.bz");

  // OR all the Cmps together
  Value* OrGxy = Builder.CreateOr(CmpGx, CmpGy, "or.sm.gx.gy");
  Value* OrGxyz = Builder.CreateOr(OrGxy, CmpGz, "or.sm.gxy.gz");
  Value* OrGxyzBx = Builder.CreateOr(OrGxyz, CmpBx, "or.sm.gxyz.bx");
  Value* OrGxyzBxy = Builder.CreateOr(OrGxyzBx, CmpBy, "or.sm.gxyzbx.by");
  Value* OrGxyzBxyz = Builder.CreateOr(OrGxyzBxy, CmpBz, "or.sm.gxyzbxy.bz");

  return Builder.CreateIsNull(OrGxyzBxyz, "notTooSmall");
}

// default dims are constant i32 1
hydra::gpgpu::KernelDimsSCEV::KernelDimsSCEV(ScalarEvolution& SE) :
  GridX(SE.getConstant(Type::getInt32Ty(SE.getContext()), 1)),
  GridY(GridX),
  GridZ(GridX),
  BlockX(GridX),
  BlockY(GridX),
  BlockZ(GridX)
{}

hydra::gpgpu::KernelDims::KernelDims(
  const hydra::gpgpu::KernelDimsSCEV& Dims,
  SCEVExpander& Exp,
  Instruction* Pos
) :
  Exp(Exp),
  Pos(Pos),
  M(Pos->getModule()),
  Context(M->getContext()),
  i32(Type::getInt32Ty(Context)),
  GridX(expandCodeFor(Dims.GridX)),
  GridY(expandCodeFor(Dims.GridY)),
  GridZ(expandCodeFor(Dims.GridZ)),
  BlockX(expandCodeFor(Dims.BlockX)),
  BlockY(expandCodeFor(Dims.BlockY)),
  BlockZ(expandCodeFor(Dims.BlockZ))
{}

Value* hydra::gpgpu::KernelDims::expandCodeFor(const SCEV* SH) {
  Value* V = Exp.expandCodeFor(SH, SH->getType(), Pos);

  if (V->getType() == i32)
    return V;

  // The src/dst types are treatable as unsigned; loop trip counts cannot be
  // negative for obvious reasons, and if program flow reaches this point,
  // V should be unsigned and will not exceed the range where signed and
  // unsigned values of the dst type have different representations.
  auto Opcode = CastInst::getCastOpcode(V, false, i32, false);

  return CastInst::Create(Opcode, V, i32, V->getName() + ".cast", Pos);
}

Value* hydra::gpgpu::KernelDims::insertBoundsCheck() {
  // Can't set function linkage to internal as the function would be recreated
  // each time this is called
  Function* F = getBoundsCheckFunc();

  ArrayRef<Value*> Args = {GridX, GridY, GridZ, BlockX, BlockY, BlockZ};
  return CallInst::Create(F, Args, "inBounds", Pos);
}

// zeroext i1 @kernelDimsInBounds(i32, i32, i32, i32, i32, i32) nounwind
//   readnone uwtable
Function* hydra::gpgpu::KernelDims::getBoundsCheckFunc() {
  AttributeSet AS = AttributeSet::get(Context, AttributeSet::FunctionIndex, {
    Attribute::NoUnwind,
    Attribute::ReadNone,
    Attribute::UWTable
  });
  AS = AS.addAttribute(Context, AttributeSet::ReturnIndex, Attribute::ZExt);

  auto i1 = Type::getInt1Ty(Context);
  auto FuncTy = FunctionType::get(i1, {i32, i32, i32, i32, i32, i32}, false);

  // Using a clang-mangled name to avoid collisions just in case
  //
  // MAYBE: Allow supplying as external parameter just in case a collision does
  // occur with the default name?
  StringRef MangledName("_ZN5hydra5gpgpu18kernelDimsInBoundsEjjjjjj");

  // We always use the same name and type for this function, so it's guaranteed
  // to not be wrapped in a constexpr cast (barring any malicious behavior by
  // other programmers)
  auto F = cast<Function>(M->getOrInsertFunction(MangledName, FuncTy, AS));

  // Need to fill the function if we just inserted it
  if (F->empty()) {
    // Naming the arguments for easy reference
    auto Arg = F->arg_begin();
    (Arg++)->setName("GridX");
    (Arg++)->setName("GridY");
    (Arg++)->setName("GridZ");
    (Arg++)->setName("BlockX");
    (Arg++)->setName("BlockY");
    Arg->setName("BlockZ");

    auto BB = BasicBlock::Create(Context, "entry", F);

    // It's easiest to insert a whole bunch of instructions at one spot using a
    // builder
    CheckFuncBuilderHelper BH(BB, F->getValueSymbolTable());

    BH.build();
  }

  return F;
}

hydra::gpgpu::KernelDimsData::KernelDimsData(
  const hydra::gpgpu::KernelDims& Dims
) :
  GridX(Dims.GridX),
  GridY(Dims.GridY),
  GridZ(Dims.GridZ),
  BlockX(Dims.BlockX),
  BlockY(Dims.BlockY),
  BlockZ(Dims.BlockZ)
{}
