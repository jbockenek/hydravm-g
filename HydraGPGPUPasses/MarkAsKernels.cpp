//===- MarkAsKernels.cpp - Pass to mark functions as GPGPU kernels --------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Mark kernel functions as such (currently marks all defined functions as
/// kernels, need support for exclusions).
///
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "mark-as-kernels"

#include "HydraGPGPUConstants.h"

#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"

using namespace llvm;

// (Simple) passes are self-contained, only need anonymous namespace for one
// file
namespace {
  class MarkAsKernels : public ModulePass {
    Module::FunctionListType& getKernelFunctionList(Module& M);
  public:
    static char ID; // Pass identification

    MarkAsKernels();

    bool runOnModule(Module& M) override;
    void getAnalysisUsage(AnalysisUsage& AU) const override;
  };
}

// non-CFG non-analysis pass
static RegisterPass<MarkAsKernels> X(
  "mark",
  "Mark functions in a module as GPGPU kernels",
  false,
  false
);

char MarkAsKernels::ID = 0;
MarkAsKernels::MarkAsKernels() : ModulePass(ID) {}

bool MarkAsKernels::runOnModule(Module& M) {
  bool Modified = false;

  LLVMContext& C = M.getContext();
  NamedMDNode* NVVMAnnotations =
    M.getOrInsertNamedMetadata(hydra::gpgpu::NVVM_META);
  auto& Kernels = getKernelFunctionList(M);

  // NVVM kernel annotations are of the form
  // `metadata !{<function-ref>, metadata !"kernel", i32 1}`, so we need those
  // boilerplate values.
  auto KernelStr = MDString::get(C, "kernel");

  auto ConstOne = ConstantInt::get(Type::getInt32Ty(C), 1);
  auto One = ConstantAsMetadata::get(ConstOne);

  for (Function& F : Kernels) {
    if (F.isDeclaration() || F.hasInternalLinkage())
      continue;

    // Functions are Constants
    auto MDF = ConstantAsMetadata::get(&F);
    auto Tuple = MDTuple::get(C, {MDF, KernelStr, One});
    NVVMAnnotations->addOperand(Tuple);

    // We modified the module if there were any kernels to annotate for
    Modified = true;
  }

  return Modified;
}

Module::FunctionListType& MarkAsKernels::getKernelFunctionList(Module& M) {
  return M.getFunctionList();
}

// Changing NVVM metadata shouldn't invalidate any previous passes, so
// preserving for now
void MarkAsKernels::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesAll();
}
