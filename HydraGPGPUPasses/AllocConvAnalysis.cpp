//===- AllocConvAnalysis.cpp - Alloc analysis for GPGPU params ------------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// See header file for details.
///
//===----------------------------------------------------------------------===//

#include "AllocConvAnalysis.h"

#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/Pass.h"

using namespace hydra::gpgpu;
using namespace llvm;

static RegisterAnalysisGroup<AllocConvAnalysis>
X("Allocation Conversion Analysis");

char AllocConvAnalysis::ID = 0;
AllocConvAnalysis::AllocConvAnalysis(AllocConvAnalysisKind K) : Kind(K) {}

AllocConvAnalysis::~AllocConvAnalysis() {}

AllocConvAnalysis::AllocConvAnalysisKind AllocConvAnalysis::getKind() const {
  return Kind;
}

void AllocConvAnalysis::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<AllocConvAnalysis>();   // All ACAs chain (except the default)

   // We need this to determine what instructions use library allocation
   // functions
  AU.addRequired<TargetLibraryInfoWrapperPass>();

  AU.setPreservesAll(); // analysis pass group

  // MAYBE: Use addRequiredTransitive if we want to be lazy and call through to
  // previous passes when supplying results rather than calculating everything
  // in the runOn functions.
}
