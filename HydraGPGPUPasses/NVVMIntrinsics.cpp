//===- NVVMIntrinsics.cpp - Getters for NVVM Intrinsics -------------------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains definitions of functions to obtain references to the
/// various intrinsics used in NVVM IR. (See
/// http://llvm.org/docs/NVPTXUsage.html#nvptx-intrinsics)
///
//===----------------------------------------------------------------------===//

#include "NVVMIntrinsics.h"

#include "llvm/IR/Intrinsics.h"

using namespace llvm;

Function* hydra::gpgpu::getThreadIdXFunc(Module& M) {
  return Intrinsic::getDeclaration(&M, Intrinsic::nvvm_read_ptx_sreg_tid_x);
}

Function* hydra::gpgpu::getThreadIdYFunc(Module& M) {
  return Intrinsic::getDeclaration(&M, Intrinsic::nvvm_read_ptx_sreg_tid_y);
}

Function* hydra::gpgpu::getThreadIdZFunc(Module& M) {
  return Intrinsic::getDeclaration(&M, Intrinsic::nvvm_read_ptx_sreg_tid_z);
}

Function* hydra::gpgpu::getBlockIdXFunc(Module& M) {
  return Intrinsic::getDeclaration(&M, Intrinsic::nvvm_read_ptx_sreg_ctaid_x);
}

Function* hydra::gpgpu::getBlockIdYFunc(Module& M) {
  return Intrinsic::getDeclaration(&M, Intrinsic::nvvm_read_ptx_sreg_ctaid_y);
}

Function* hydra::gpgpu::getBlockIdZFunc(Module& M) {
  return Intrinsic::getDeclaration(&M, Intrinsic::nvvm_read_ptx_sreg_ctaid_z);
}

Function* hydra::gpgpu::getBlockDimXFunc(Module& M) {
  return Intrinsic::getDeclaration(&M, Intrinsic::nvvm_read_ptx_sreg_ntid_x);
}

Function* hydra::gpgpu::getBlockDimYFunc(Module& M) {
  return Intrinsic::getDeclaration(&M, Intrinsic::nvvm_read_ptx_sreg_ntid_y);
}

Function* hydra::gpgpu::getBlockDimZFunc(Module& M) {
  return Intrinsic::getDeclaration(&M, Intrinsic::nvvm_read_ptx_sreg_ntid_z);
}

Function* hydra::gpgpu::getGridDimXFunc(Module& M) {
  return Intrinsic::getDeclaration(&M, Intrinsic::nvvm_read_ptx_sreg_nctaid_x);
}

Function* hydra::gpgpu::getGridDimYFunc(Module& M) {
  return Intrinsic::getDeclaration(&M, Intrinsic::nvvm_read_ptx_sreg_nctaid_y);
}

Function* hydra::gpgpu::getGridDimZFunc(Module& M) {
  return Intrinsic::getDeclaration(&M, Intrinsic::nvvm_read_ptx_sreg_nctaid_z);
}

Function* hydra::gpgpu::getWarpSizeFunc(Module& M) {
  return Intrinsic::getDeclaration(&M, Intrinsic::nvvm_read_ptx_sreg_warpsize);
}
