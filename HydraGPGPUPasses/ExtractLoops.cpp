//===- ExtractLoops.cpp - Pass to extract loops as GPGPU kernel functions -===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Extracts loops supplied from a previous analysis pass.
///
//===----------------------------------------------------------------------===//

#include "AllocConvAnalysis.h"
#include "CUDABoilerplate.h"
#include "CUDAFunctions.h"
#include "HydraGPGPU.h"
#include "LoopCompatAnalysis.h"
#include "TemplatedUtils.h"

#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/Twine.h" // for Twine concat functionality
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/ScalarEvolutionExpander.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Value.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/Transforms/Utils/ValueMapper.h"

#include <algorithm>
#include <string>
#include <system_error>
#include <vector>

using namespace llvm;
using namespace hydra::gpgpu;

cl::opt<std::string> KernelName(
  "kernel-name",
  cl::value_desc("kernel_name"),
  cl::desc(
    "GPGPU kernel name (if not present, will use <module_id>.kernel, which may "
    "not work if the IR module doesn't have a proper identifier); the expected "
    "PTX name is <kernel_name>.ptx"
  )
);

// (Simple) passes are self-contained, only need anonymous namespace for one
// file
namespace {
  using KernelMarkerVector = SmallVector<KernelMarker, BASE_SMALLVECTOR_SIZE>;

  using LoopVector = SmallVector<const Loop*, MAX_LOOPS>;
  using DimsVector = SmallVector<const SCEV**, MAX_LOOPS>;

  class ExtractLoops : public ModulePass {
    KernelDimsSCEV getKernelDimsSCEV(ScalarEvolution& SE, const Loop* L);
    DimsVector orderDims(LoopVector::size_type LoopCount, KernelDimsSCEV& Dims);

    /// \brief moves instructions after function call in codeRepl block to
    /// successor
    ///
    /// This ensures the instructions are executed even when the fallback block
    /// is not used, as the kernel launch only replaces the function call
    /// itself.
    void sinkPostLaunchInsts(BranchInst* BranchToCodeRepl);

    // trip count == backedge taken count + 1
    const SCEV* getTripCount(ScalarEvolution& SE, const Loop* L) const;

    void fillConstGVMap(Module& KernelM, ValueToValueMapTy& GVPMap);
    void convertFuncsUsedInLoops(Module& KernelM, ValueToValueMapTy& GVPMap);
    KernelMarkerVector extractLoops(
      Module& KernelM,
      FuncWithLoopHeadersListType& FuncsWithLoopHeaders
    );
    KernelMarker extractLoop(
      DominatorTree& DT,
      ScalarEvolution& SE,
      Loop* L,
      ValueToValueMapTy& GVMap
    );
    void addGPGPUGlue(
      Module& M,
      const Twine& KernelModuleName,
      ArrayRef<KernelMarker> KernelMarkers
    );
    void printModuleToFile(Module& M);
  public:
    static char ID; // Pass identification

    ExtractLoops();

    void getAnalysisUsage(AnalysisUsage& AU) const override;
    bool runOnModule(Module& M) override;
  };

  StringRef rename(Function* Old, Function* New) {
    // PTX doesn't like functions with periods in the names
    std::string Name(New->getName());
    std::replace(Name.begin(), Name.end(), '.', '_');
    New->setName(Name);

    // To get Twine concat functionality
    StringRef NameSR = New->getName();

    // will be using the old function as the fallback function eventually
    Old->setName(NameSR + ".fallback");

    return NameSR;
  }
}

// non-CFG non-analysis pass
static RegisterPass<ExtractLoops> X(
  "extract-loops",
  "Extract loops supplied from a previous analysis pass",
  false,
  false
);

char ExtractLoops::ID = 0;
ExtractLoops::ExtractLoops() : ModulePass(ID) {}

void ExtractLoops::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<DominatorTreeWrapperPass>();
  AU.addRequired<LoopInfoWrapperPass>();
  AU.addRequired<ScalarEvolutionWrapperPass>();
  AU.addRequired<AllocConvAnalysis>();
  AU.addRequired<LoopCompatAnalysis>();

  // What can we preserve? It's simplest to just let the analysis passes
  // re-execute given all our changes, but that takes more time and it would be
  // more efficient to just update the analyses directly when possible.
}

bool ExtractLoops::runOnModule(Module& M) {
  auto& LCA = getAnalysis<LoopCompatAnalysis>();
  auto& FuncsWithLoopHeaders = LCA.getExtractibleLoops();

  // Can't do anything with no loops to extract.
  if (FuncsWithLoopHeaders.empty()) {
      return false;
  }

  if (KernelName.empty()) {
    KernelName = M.getModuleIdentifier() + ".kernel";
  }

  Module KernelM(KernelName, M.getContext());

  // TODO: abstract away device specifics
  KernelM.setTargetTriple("nvptx64-nvidia-cuda");
  KernelM.setDataLayout(
    "e-p:64:64-"
    "i1:8-i8:8-i16:16-i32:32-i64:64-"
    "f32:32-f64:64-"
    "v16:16-v32:32-v64:64-v128:128-"
    "n16:32:64"
  );

  KernelMarkerVector KernelMarkers =
    extractLoops(KernelM, FuncsWithLoopHeaders);

  // Module name indicates the kernel file loaded by the generated host
  addGPGPUGlue(M, KernelName + ".ptx", KernelMarkers);

  // get rid of explicit default initializers for class/struct members in
  // constructor lists, if any; they aren't actually needed (I guess they
  // provide a bit of semantic info, though).

  return true;
}

void ExtractLoops::addGPGPUGlue(
  Module& M,
  const Twine& KernelModuleName,
  ArrayRef<KernelMarker> KernelMarkers
) {
  // Due to the issues with memory handling, we only operate on modules with
  // main() (and assume all libraries that can be bitcode-linked are)
  Function* Main = M.getFunction("main");
  assert(Main && !Main->isDeclaration() && "Cannot operate on non-main module");

  // TODO: for extensibility, will probably want to abstract away calls to
  // device-specific functions eventually, not sure of the best way to do
  // that yet.
  Instruction* SetupInsertionPoint = getFirstNonAllocaInsertionPt(Main);

  GPGPUData Data = insertGPGPUSetup(
    SetupInsertionPoint,
    KernelModuleName,
    KernelMarkers
  );

  insertGPGPUTeardown(Main, Data);
}

void ExtractLoops::fillConstGVMap(Module& KernelM, ValueToValueMapTy& GVMap) {
  auto& ACAnalysis = getAnalysis<AllocConvAnalysis>();
  auto& LCAnalysis = getAnalysis<LoopCompatAnalysis>();

  for (GlobalVariable* GV : ACAnalysis.getConstGlobals()) {
    GVMap.insert({GV, createKernelConstGlobal(KernelM, GV)});
  }

  for (const Function* F : LCAnalysis.getCompatFuncDecls()) {
    GVMap.insert({F, createKernelFuncDecl(KernelM, F)});
  }
}

void ExtractLoops::convertFuncsUsedInLoops(
  Module& KernelM,
  ValueToValueMapTy& GVMap
) {
  auto& LCAnalysis = getAnalysis<LoopCompatAnalysis>();
  auto& FuncsToConvert = LCAnalysis.getFunctionsToConvert();
  auto& KernelFunctionList = KernelM.getFunctionList();

  // We first need to clone the specified functions, updating Values as
  // necessary. Unfortunately, any internal calls to defined functions will not
  // be updated yet.
  for (const Function* F : FuncsToConvert) {
    // Don't need to change names for these; we do need to keep track of the
    // cloned functions in the value map, though.
    Function* NewF = CloneFunction(F, GVMap, true);

    NewF->setLinkage(GlobalValue::InternalLinkage); // b/c only used by kernels
    KernelFunctionList.push_back(NewF);
    GVMap.insert({F, NewF});
  }

  // Now we can update any calls in the cloned functions (doing it this way is
  // probably inefficient, but I'm not sure what a better way would be)
  for (const Function* F : FuncsToConvert) {
    auto NewF = dyn_cast<Function>(GVMap[F]);

    for (BasicBlock& BB : *NewF) {
      for (Instruction& I : BB) {
        // I don't know why CloneFunction doesn't appear to use
        // RF_IgnoreMissingEntries internally, but the documentation for
        // RemapInstruction appears to require it for our purposes so including.
        RemapInstruction(&I, GVMap, RF_IgnoreMissingEntries);
      }
    }
  }
}

KernelMarkerVector ExtractLoops::extractLoops(
  Module& KernelM,
  FuncWithLoopHeadersListType& FuncsWithLoopHeaders
) {
  KernelMarkerVector KernelMarkers;
  auto& KernelFunctionList = KernelM.getFunctionList();

  ValueToValueMapTy GVMap;
  fillConstGVMap(KernelM, GVMap);

  // Using this here so the GVMap gets all the functions to update references to
  convertFuncsUsedInLoops(KernelM, GVMap);

  for (auto& FuncHeadersPair : FuncsWithLoopHeaders) {
    Function* F = FuncHeadersPair.first;

    auto& DT = getAnalysis<DominatorTreeWrapperPass>(*F).getDomTree();
    auto& LI = getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
    auto& SE = getAnalysis<ScalarEvolutionWrapperPass>(*F).getSE();

    for (BasicBlock* LoopHeader : FuncHeadersPair.second) {
      Loop* L = LI.getLoopFor(LoopHeader);
      KernelMarker Marker = extractLoop(DT, SE, L, GVMap);

      KernelFunctionList.push_back(Marker.Kernel);
      KernelMarkers.push_back(Marker);
    }
  }

  // print KernelM to file when pass is used with opt; we can't just attach
  // the new module to the pass instance as the pass manager doesn't support
  // addRequired/getAnalysis with transform passes. That methodology would
  // probably work when managing pass execution manually/combining steps into
  // single passes, though.
  printModuleToFile(KernelM);

  return KernelMarkers;
}

// Extract loop to add to new module for GPGPU conversion, keep loop in
// extracted function as fallback
KernelMarker ExtractLoops::extractLoop(
  DominatorTree& DT,
  ScalarEvolution& SE,
  Loop *L,
  ValueToValueMapTy& GVMap
) {
  KernelDimsSCEV DimsSCEV = getKernelDimsSCEV(SE, L);

  BasicBlock* Preheader = L->getLoopPreheader();
  assert(Preheader && "Loop does not have a preheader");

  // If the terminator for the loop preheader is not an unconditional branch,
  // LoopBase is broken.
  auto PreheaderBranch = cast<BranchInst>(Preheader->getTerminator());
  assert(
    PreheaderBranch->isUnconditional() &&
    "Loop preheader terminator not an unconditional branch"
  );

  // Need to convert all the SCEV dims into usable expressions
  auto M = Preheader->getModule();
  SCEVExpander DimsExpander(SE, M->getDataLayout(), "DimsExpander");
  KernelDims Dims(DimsSCEV, DimsExpander, PreheaderBranch);

  // Insert bounds checking for kernel dimensions; we'll use this check to
  // determine if the fallback should be used or not.
  //
  // TODO: Add more advanced analysis eventually
  Value* InBounds = Dims.insertBoundsCheck();

  Function* F = CodeExtractor(DT, *L).extractCodeRegion();
  sinkPostLaunchInsts(PreheaderBranch);

  // Make a copy of the function to put in the new module (copy will start with
  // the same name as the copied) with any appropriate kernel globals in use
  Function* NewF = CloneFunction(F, GVMap, true);

  // New function has internal linkage (because CodeExtractor creates internal
  // functions), but we need external
  NewF->setLinkage(GlobalValue::ExternalLinkage);

  // Changing function names to our liking
  rename(F, NewF);

  // Need a way to indicate to analysis and possibly to certain transform passes
  // to not operate on the fallback functions.
  //
  // TODO: change to metadata as this is pass-specific data not actually related
  // to the behavior of the function.
  F->addFnAttr(HYDRA_FALLBACK_ATTR);

  // Unlike LoopInfo, ScalarEvolution requires explicit updating when a loop
  // or value will be changed even when the results are not preserved.
  SE.forgetLoop(L);

  // KernelDims is not copyable/movable, so we have another class for just the
  // data (the KernelDims behavior should probably be refactored eventually)
  //
  // Note that KernelDims itself can easily be copied/moved using
  // compiler-generated ctors, but as some of the data it references is managed
  // externally the pointers/references will become invalid once the referenced
  // data goes out of scope.
  return {
    F,
    NewF,
    PreheaderBranch,
    InBounds,
    KernelDimsData(Dims)
  };
}

void ExtractLoops::sinkPostLaunchInsts(BranchInst* BranchToCodeRepl) {
  BasicBlock* CodeRepl = BranchToCodeRepl->getSuccessor(0);
  BasicBlock* PostCodeRepl = CodeRepl->getSingleSuccessor();

  assert(PostCodeRepl && "CodeRepl block should have a single successor");

  // Removing PHI nodes; using while loop and advancing PI early to avoid trying
  // to advance invalid iterators after replacement and erasing.
  //
  // FoldSingleEntryPHINodes() from BasicBlockUtils.h serves a similar purpose,
  // but does not sink the affected instructions.
  auto PI = PostCodeRepl->begin();
  while (PHINode* PN = dyn_cast<PHINode>(&*PI)) {
    assert(PN->getNumIncomingValues() == 1 && "Not just one predecessor!");
    ++PI;

    Value* Replacement = PN->getIncomingValue(0);
    assert(Replacement != PN && "Self-referential PHI node!");

    PN->replaceAllUsesWith(Replacement);

    if (auto ReplInst = dyn_cast<Instruction>(Replacement)) {
      ReplInst->moveBefore(PN);
    }

    PN->eraseFromParent();
  }
}

// TODO: remapping loop nests (might require polly)?
KernelDimsSCEV ExtractLoops::getKernelDimsSCEV(
  ScalarEvolution& SE,
  const Loop* L
) {
  KernelDimsSCEV Dims(SE);
  LoopVector Loops = getNestedLoopVector(L);
  LoopVector::size_type LoopCount = Loops.size();
  DimsVector DimsMembers = orderDims(LoopCount, Dims);

  // Now that both loops and dims are in the right order, we can fill them in.
  for (LoopVector::size_type i = 0; i < LoopCount; ++i) {
    *DimsMembers[i] = getTripCount(SE, Loops[i]);
  }

  return Dims;
}

// Getting a SmallVector with pointers to members of Dims in an appropriate
// order for the number of loops. Using the same order as for indvar mapping,
// but with respect to block/grid dims rather than thread/block IDs:
// one loop    -> bx
// two loops   -> gx bx
// three loops -> gy gx bx
// four loops  -> gz gy gx bx
// five loops  -> gz gy gx by bx
// six loops   -> gz gy gx bz by bx
DimsVector ExtractLoops::orderDims(
  LoopVector::size_type LoopCount,
  KernelDimsSCEV& Dims
) {
  DimsVector DimsMembers;

  if (LoopCount >= 4) {
    DimsMembers.push_back(&Dims.GridZ);
    DimsMembers.push_back(&Dims.GridY);
    DimsMembers.push_back(&Dims.GridX);
  } else {
    if (LoopCount == 3) {
      DimsMembers.push_back(&Dims.GridY);
    }

    if (LoopCount >= 2) {
      DimsMembers.push_back(&Dims.GridX);
    }
  }

  if (LoopCount == 6) {
    DimsMembers.push_back(&Dims.BlockZ);
  }

  if (LoopCount >= 5) {
    DimsMembers.push_back(&Dims.BlockY);
  }

  DimsMembers.push_back(&Dims.BlockX);

  return DimsMembers;
}

const SCEV*
ExtractLoops::getTripCount(ScalarEvolution& SE, const Loop* L) const {
  const SCEV* BTC = SE.getBackedgeTakenCount(L);
  assert(!isa<SCEVCouldNotCompute>(BTC) && "Incomputable backedge count");

  // Not going to worry about wrap flags For now
  return BTC;
}

void ExtractLoops::printModuleToFile(Module& M) {
  std::error_code EC;
  const std::string& MID = M.getModuleIdentifier();

  // MAYBE: allow supplying path on command line so we don't always write the
  // extra module to the current working directory.
  raw_fd_ostream FO(MID, EC, sys::fs::OpenFlags::F_None);

  if (EC) {
    errs() << "Error writing " << MID << ": " << EC.message() << '\n';
    return;
  }

  M.print(FO, nullptr);
  // autoclose
}
