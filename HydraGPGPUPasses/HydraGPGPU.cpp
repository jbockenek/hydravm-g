//===- HydraGPGPU.cpp - Global-ish helper functions, etc. -----------------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains definitions for general helper functions declared in the
/// \c hydra::gpgpu namespace.
///
//===----------------------------------------------------------------------===//

#include "HydraGPGPU.h"

#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/Type.h"

using namespace llvm;

namespace {
  AllocaInst* baseCreateAlloca(
    Function* Func,
    Type* Ty,
    Value* ArraySize,
    const Twine& Name
  ) {
    BasicBlock& EntryBlock = Func->getEntryBlock();
    Instruction* AllocaInsertPoint = &*EntryBlock.getFirstInsertionPt();

    auto AI = new AllocaInst(Ty, ArraySize, Name, AllocaInsertPoint);

    // Need to specify that the alloca is an inserted one, not to be converted.
    auto& C = Func->getContext();
    auto MDN = MDNode::get(C, {}); // using empty node, only existence matters
    AI->setMetadata(hydra::gpgpu::MD_ALLOCA, MDN);

    return AI;
  }
}

AllocaInst* hydra::gpgpu::createAllocaInEntryBlock(
  Function* Func,
  Type* Ty,
  Value* ArraySize,
  const Twine& Name
) {
  return baseCreateAlloca(Func, Ty, ArraySize, Name);
}

AllocaInst* hydra::gpgpu::createAllocaInEntryBlock(
  Function* Func,
  Type* Ty,
  const Twine& Name
) {
  return createAllocaInEntryBlock(Func, Ty, nullptr, Name);
}

AllocaInst* hydra::gpgpu::createAllocaInEntryBlock(
  IRBuilder<>& Builder,
  Type* Ty,
  Value* ArraySize,
  const Twine& Name
) {
  BasicBlock* CurBlock = Builder.saveIP().getBlock();
  assert(CurBlock && "Builder not set to insert in a block!");

  Function* CurFunc = CurBlock->getParent();
  assert(CurFunc && "Basic block not in a function!");

  return baseCreateAlloca(CurFunc, Ty, ArraySize, Name);
}

AllocaInst* hydra::gpgpu::createAllocaInEntryBlock(
  IRBuilder<>& Builder,
  Type* Ty,
  const Twine& Name
) {
  return createAllocaInEntryBlock(Builder, Ty, nullptr, Name);
}

AllocaInst* hydra::gpgpu::createAllocaInEntryBlock(
  Instruction* CurrentInstruction,
  Type* Ty,
  Value* ArraySize,
  const Twine& Name
) {
  // Getting to the spot to place the alloca
  BasicBlock* CurBlock = CurrentInstruction->getParent();
  assert(CurBlock && "Instruction not in a basic block!");

  Function* CurFunc = CurBlock->getParent();
  assert(CurFunc && "Basic block not in a function!");

  return baseCreateAlloca(CurFunc, Ty, ArraySize, Name);
}

AllocaInst* hydra::gpgpu::createAllocaInEntryBlock(
  Instruction* CurrentInstruction,
  Type* Ty,
  const Twine& Name
) {
  return createAllocaInEntryBlock(CurrentInstruction, Ty, nullptr, Name);
}

Instruction* hydra::gpgpu::getFirstNonAllocaInsertionPt(Function* Func) {
  return getFirstNonAllocaInsertionPt(&Func->getEntryBlock());
}

Instruction* hydra::gpgpu::getFirstNonAllocaInsertionPt(BasicBlock* Block) {
  BasicBlock::iterator IT = Block->getFirstInsertionPt();

  while (isa<AllocaInst>(*IT))
    ++IT;

  return &*IT;
}

Instruction* hydra::gpgpu::getFirstNonAllocaInsertionPt(Instruction* Inst) {
  return getFirstNonAllocaInsertionPt(Inst->getParent());
}
