//===- ConvertLoops.cpp - Pass to convert loops to GPGPU kernel form ------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Converts all sets of loops in a module into single iterations, replacing
/// induction variables with thread and block IDs.
///
//===----------------------------------------------------------------------===//

#include "HydraGPGPU.h"
#include "NVVMIntrinsics.h"
#include "TemplatedUtils.h"

#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/ValueSymbolTable.h"

#include <cassert>
#include <iterator>

#define DEBUG_TYPE "convert-loops"

using namespace hydra::gpgpu;
using namespace llvm;

// (Simple) passes are self-contained, only need anonymous namespace for one
// file
namespace {
  using LoopVector = SmallVector<Loop*, MAX_LOOPS>;
  using FunctionVector = SmallVector<Function*, MAX_LOOPS>;

  // Needs to be module pass as we add function prototypes
  class ConvertLoops : public ModulePass {
    void runOnFunction(Function& F);
    void convertLoop(Loop* L, Module& M);
    FunctionVector orderIntrinsicsToCall(
      LoopVector::size_type LoopCount,
      Module& M
    );
    void convertSingleLoop(Loop* L, Function* IdIntrinsic);
    void replaceIndVar(Loop* L, Function* IdIntrinsic);
    void breakBackEdge(Loop* L);
    void unconditionalizeHeaderBranch(Loop* L);
  public:
    static char ID; // Pass identification

    ConvertLoops();

    void getAnalysisUsage(AnalysisUsage& AU) const override;
    bool runOnModule(Module& M) override;
  };
}

// non-CFG non-analysis pass
static RegisterPass<ConvertLoops> X(
  "convert-loops",
  "Convert the loops in a module's functions to kernel form",
  false,
  false
);

char ConvertLoops::ID = 0;
ConvertLoops::ConvertLoops() : ModulePass(ID) {}

void ConvertLoops::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
}

bool ConvertLoops::runOnModule(Module& M) {
  bool Modified = false;

  for (Function& F : M.getFunctionList()) {
    // Declarations can't contain any loops as they're empty, so skipping them.
    if (F.isDeclaration())
      continue;

    // any failures will be assert failures
    runOnFunction(F);
    Modified = true;
  }

  return Modified;
}

void ConvertLoops::runOnFunction(Function& F) {
  auto& LI = getAnalysis<LoopInfoWrapperPass>(F).getLoopInfo();

  // Newer LoopInfo doesn't have size() for some reason
  auto LoopCount = std::distance(LI.begin(), LI.end());

  // Not converting loops for kernel functions without them; perhaps base
  // decision on function existence in nvvm.annotations metadata instead? That
  // would make usage of this pass more flexible, especially if we decide to
  // allow specific loops in kernel functions.
  if (LoopCount > 0) {
    assert(
      LoopCount == 1 &&
      "Kernel functions should have only one top-level loop"
    );
    convertLoop(*LI.begin(), *F.getParent());
  }
}

// May want to put this function elsewhere, but it works here for now. The
// hardcoded stuff is a pain, but for the moment the behavior is too closely
// linked to the limitations of the CUDA model, which is much less likely to
// change than the hardware resource limitations (e.g. max thread
// count/block size, etc.).
void ConvertLoops::convertLoop(Loop* L, Module& M) {
  // It's easier to start with the innermost loop, then go down from the top
  LoopVector Loops = getNestedLoopVector(L);
  LoopVector::size_type LoopCount = Loops.size();
  FunctionVector ToCall = orderIntrinsicsToCall(LoopCount, M);

  for (LoopVector::size_type i = 0; i < LoopCount; ++i) {
    convertSingleLoop(Loops[i], ToCall[i]);
  }

  // TODO: figure out how to compute IndVar result for those cases where the indvar is used after the end of the loop, too
  // ^ -indvar/IndVarSimplify pass handles this (but does it work when trip count is only computable at runtime?)!

}

/// Indvar mapping, outermost loop to innermost; going by
/// http://stackoverflow.com/questions/22120785, indexing should be thought of
/// as laid out in a multidimensional array arr[bz][by][bx][tz][ty][tx]; this
/// may not be as important for block IDs as blocks don't necessarily get the
/// same sort of linear layout as the threads in those blocks, but being
/// consistent is easier.
/// one loop    -> tx
/// two loops   -> bx tx
/// three loops -> by bx tx
/// four loops  -> bz by bx tx
/// five loops  -> bz by bx ty tx
/// six loops   -> bz by bx tz ty tx
FunctionVector ConvertLoops::orderIntrinsicsToCall(
  LoopVector::size_type LoopCount,
  Module& M
) {
  FunctionVector IntrinsicsToCall;

  if (LoopCount >= 4) {
    IntrinsicsToCall.push_back(getBlockIdZFunc(M));
    IntrinsicsToCall.push_back(getBlockIdYFunc(M));
    IntrinsicsToCall.push_back(getBlockIdXFunc(M));
  } else {
    if (LoopCount == 3) {
      IntrinsicsToCall.push_back(getBlockIdYFunc(M));
    }

    if (LoopCount >= 2) {
      IntrinsicsToCall.push_back(getBlockIdXFunc(M));
    }
  }

  if (LoopCount == 6) {
      IntrinsicsToCall.push_back(getThreadIdZFunc(M));
  }

  if (LoopCount >= 5) {
    IntrinsicsToCall.push_back(getThreadIdYFunc(M));
  }

  IntrinsicsToCall.push_back(getThreadIdXFunc(M));

  return IntrinsicsToCall;
}

void ConvertLoops::convertSingleLoop(Loop* L, Function* IdIntrinsic) {
  // performs the actual induction-variable-to-id-intrinsic transformation
  replaceIndVar(L, IdIntrinsic);

  // Breaking the loop's back edge allows for easier CFG simplification
  breakBackEdge(L);

  // Making the header branch unconditionally to the loop body kills the branch
  // condition instructions, meaning we can get rid of them with DCE
  unconditionalizeHeaderBranch(L);
}

// Replacing induction variable with call to NVPTX (NVVM?) intrinsic
void ConvertLoops::replaceIndVar(Loop* L, Function* IdIntrinsic) {
  PHINode* IndVar = L->getCanonicalInductionVariable();
  assert(IndVar && "Loop lacks canonical induction variable");

  Instruction* CallOrCastInst = CallInst::Create(IdIntrinsic, None, "");
  CallOrCastInst->takeName(IndVar);

  // The NVPTX (NVVM?) instrinsics all seem to use tail calls
  cast<CallInst>(CallOrCastInst)->setTailCall();

  // Need to insert the call into the function's entry block (but after any
  // allocas, just in case)
  auto CurFunc = IndVar->getFunction();
  Instruction* InsertionPoint = getFirstNonAllocaInsertionPt(CurFunc);
  CallOrCastInst->insertBefore(InsertionPoint);

  // Need to map call to same type as indvar if it differs
  Type* IVType = IndVar->getType();
  if (CallOrCastInst->getType() != IVType) {
    // Canonical indvars are integers that start at 0 and increase, so we just
    // need an unsigned integer cast of some sort right after the intrinsic call
    // (i.e. right before the original first-non-phinode/landingpad-instruction)
    CallOrCastInst = CastInst::CreateIntegerCast(
      CallOrCastInst,
      IVType,
      false, // unsigned
      CallOrCastInst->getName() + ".cast",
      InsertionPoint
    );
  }

  IndVar->replaceAllUsesWith(CallOrCastInst);

  // Erasing IndVar as it's now dead
  IndVar->eraseFromParent();
}

// Changing all latches to unconditionally branch to the exit block rather than
// the header (we're only dealing with loops having a single unique exit block);
// loop-simplify guarantees a single backedge.
void ConvertLoops::breakBackEdge(Loop* L) {
  BasicBlock* ExitBlock = L->getUniqueExitBlock();
  assert(ExitBlock && "Loop has multiple exit blocks");

  // most loops shouldn't have more than one or two latches
  SmallVector<BasicBlock*, 4> Latches;
  L->getLoopLatches(Latches);
  // sanity check
  assert(Latches.size() == 1 && "Loop does not have a single back edge");

  BasicBlock* Latch = Latches.front();
  TerminatorInst* TI = Latch->getTerminator();
  assert(
    TI->getNumSuccessors() == 1 &&
    "Latch terminator should have a single successor"
  );

  TI->setSuccessor(0, ExitBlock);
}

void ConvertLoops::unconditionalizeHeaderBranch(Loop* L) {
  BasicBlock* Header = L->getHeader();
  TerminatorInst* TI = Header->getTerminator();

  assert(
    TI->getNumSuccessors() > 1 &&
    "Loop Header should have some form of conditional terminator"
  );
  assert(
    TI->getNumSuccessors() == 2 &&
    "Loop header terminator has more than two possible paths"
  );

  // Figure out which successor block is the start of the body of the loop
  BasicBlock* Successor0 = TI->getSuccessor(0);
  BasicBlock* Successor1 = TI->getSuccessor(1);
  BasicBlock* ExitBlock = L->getUniqueExitBlock();
  BasicBlock* BodyBlock = nullptr;

  if (Successor0 == ExitBlock) {
    BodyBlock = Successor1;
  } else if (Successor1 == ExitBlock) {
    BodyBlock = Successor0;
  }

  assert(BodyBlock && "Loop header does not have exit block as a successor");

  // Replace the original terminator with an unconditional branch to the start
  // of the body of the loop
  BranchInst::Create(BodyBlock, TI);
  TI->eraseFromParent();
}
