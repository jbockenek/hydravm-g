//===- AllocationConverters.cpp - Tools for allocation conversion ---------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Implementations for AllocationConverters.h.
///
//===----------------------------------------------------------------------===//

#include "AllocationConverters.h"

#include "CUDABoilerplate.h"
#include "CUDAFunctions.h"
#include "HydraGPGPU.h"

#include "llvm/ADT/Twine.h" // for Twine concat functionality
#include "llvm/IR/CallSite.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Value.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/BuildLibCalls.h"

using namespace llvm;
using hydra::gpgpu::CallSiteListType;

hydra::gpgpu::AllocConverter::AllocConverter(Module& M, bool AddErrorChecking) :
  CurModule(&M),
  Context(M.getContext()),
  Builder(Context), // starting off with no position indicated
  LibraryInfo(TargetLibraryAnalysis().run(M)),
  AddErrorChecking(AddErrorChecking)
{}

bool hydra::gpgpu::AllocConverter::convert(AllocConvAnalysis& ACAnalysis) {
  bool Modified = false; // To let passes know if we changed anything

  for (Function& F : CurModule->getFunctionList()) {
    // Dominator tree pass fails on declarations
    if (F.isDeclaration()) continue;

    auto& Allocas = ACAnalysis.getAllocas(F);
    if (!Allocas.empty()) {
      Modified = true;
      convertAllocas(Allocas);
    }

    // This repetitive code could probably be extracted to a helper function
    // somehow but I don't feel like trying it right now
    auto& Mallocs = ACAnalysis.getMallocs(F);
    if (!Mallocs.empty()) {
      Modified = true;
      convertMallocs(Mallocs);
    }

    auto& Callocs = ACAnalysis.getCallocs(F);
    if (!Callocs.empty()) {
      Modified = true;
      convertCallocs(Callocs);
    }

    auto& Strdups = ACAnalysis.getStrdups(F);
    if (!Strdups.empty()) {
      Modified = true;
      convertStrdups(Strdups);
    }

    auto& Strndups = ACAnalysis.getStrndups(F);
    if (!Strndups.empty()) {
      Modified = true;
      convertStrndups(Strndups);
    }

    auto& Frees = ACAnalysis.getFrees(F);
    if (!Frees.empty()) {
      Modified = true;
      convertFrees(Frees);
    }
  }

  auto& Globals = ACAnalysis.getGlobals();
  if (!Globals.empty()) {
    Modified = true;
    convertGlobals(Globals);
  }

  return Modified;
}

void hydra::gpgpu::AllocConverter::convertAllocas(AllocaInstListType& Allocas) {
  AllocaHelper AH(this);

  for (auto& Alloca : Allocas) {
    AH.convert(Alloca);
  }
}

void hydra::gpgpu::AllocConverter::convertMallocs(CallSiteListType& Mallocs) {
  MallocHelper MH(this);

  for (auto& Malloc : Mallocs) {
    MH.convert(Malloc);
  }
}

void hydra::gpgpu::AllocConverter::convertCallocs(CallSiteListType& Callocs) {
  CallocHelper CH(this);

  for (auto& Calloc : Callocs) {
    CH.convert(Calloc);
  }
}

void hydra::gpgpu::AllocConverter::convertStrdups(CallSiteListType& Strdups) {
  StrdupHelper SH(this);

  for (auto& Strdup : Strdups) {
    SH.convert(Strdup);
  }
}

void hydra::gpgpu::AllocConverter::convertStrndups(CallSiteListType& Strndups) {
  StrndupHelper SNH(this);

  for (auto& Strndup : Strndups) {
    SNH.convert(Strndup);
  }
}

void hydra::gpgpu::AllocConverter::convertFrees(CallSiteListType& Frees) {
  for (auto& Free : Frees) {
    convertFree(Free);
  }
}

void hydra::gpgpu::AllocConverter::convertGlobals(
  GlobalVariableListType& Globals
) {
  GVHelper GVH(this);

  for (auto& GV : Globals) {
    GVH.convert(GV);
  }
}


CallInst* hydra::gpgpu::AllocConverter::createCall(
  Function* ToCall,
  ArrayRef<Value*> Args
) {
  return Builder.CreateCall(ToCall, Args);
}

InvokeInst* hydra::gpgpu::AllocConverter::createInvoke(
  Function* ToInvoke,
  InvokeInst* OldInvoke,
  ArrayRef<Value*> Args
) {
  return Builder.CreateInvoke(
    ToInvoke,
    OldInvoke->getNormalDest(),
    OldInvoke->getUnwindDest(),
    Args
  );
}

void hydra::gpgpu::AllocConverter::convertToCUDACall(
  CallSite CS,
  Function* NewFunc,
  ArrayRef<Value*> Args
) {
  // Inserting the new function call at the same point as the old one
  auto Inst = CS.getInstruction();
  Builder.SetInsertPoint(Inst);

  Instruction* ConvertedResult;
  if (CS.isCall()) {
    ConvertedResult = createCall(NewFunc, Args);
  } else if (CS.isInvoke()) {
    ConvertedResult = createInvoke(NewFunc, cast<InvokeInst>(Inst), Args);
  } else {
    llvm_unreachable("Null instruction!");
  }

  if (AddErrorChecking) {
    insertCudaCheckErrorCode(Builder, ConvertedResult);
  }
}

void hydra::gpgpu::AllocConverter::convertToCUDACall(
  AllocaInst* Alloca,
  Function* NewFunc,
  ArrayRef<Value*> Args
) {
  // Inserting the new function call near the same point as the old inst
  Builder.SetInsertPoint(getFirstNonAllocaInsertionPt(Alloca));

  Instruction* ConvertedResult = createCall(NewFunc, Args);

  if (AddErrorChecking) {
    insertCudaCheckErrorCode(Builder, ConvertedResult);
  }
}

void hydra::gpgpu::AllocConverter::convertToCUDACall(
  Function* NewFunc,
  ArrayRef<Value*> Args
) {
  // Inserting the new function call near the start of the program, after any
  // allocas in main()
  Function* Main = CurModule->getFunction("main");
  assert(Main && !Main->isDeclaration() && "Cannot operate on non-main module");

  Builder.SetInsertPoint(getFirstNonAllocaInsertionPt(Main));
  Instruction* ConvertedResult = createCall(NewFunc, Args);

  if (AddErrorChecking) {
    insertCudaCheckErrorCode(Builder, ConvertedResult);
  }
}

void hydra::gpgpu::AllocConverter::convertToCUDACallWithRemove(
  CallSite CS,
  Function* NewFunc,
  ArrayRef<Value*> Args
) {
  convertToCUDACall(CS, NewFunc, Args);

  // We don't want to keep the original call around anymore (not worrying about
  // value replacement here as that differs between types of calls).
  CS.getInstruction()->eraseFromParent();
}

void hydra::gpgpu::AllocConverter::convertFree(CallSite Free) {
  Function* CuMemFree = getCuMemFree(CurModule);

  assert(Free.arg_size() == 1 && "free() should have only 1 argument");
  Value* ValToFree = Free.getArgument(0);

  // cuMemFree() needs a CUdeviceptr value, but we obscure the used CUdeviceptrs
  // so that things don't look any different to the host code; the easiest way
  // to reobtain the right type is a simple noop cast.
  Builder.SetInsertPoint(Free.getInstruction()); // insert just before call
  ValToFree = Builder.CreateBitOrPointerCast(
    ValToFree,
    getCUdeviceptrTy(CurModule)
  );

  convertToCUDACallWithRemove(Free, CuMemFree, ValToFree);
}

hydra::gpgpu::ConversionHelper::ConversionHelper(AllocConverter* Conv) :
  Conv(Conv),
  CuMemAllocManaged(getCuMemAllocManaged(Conv->CurModule)),
  CUdeviceptr(getCUdeviceptrTy(Conv->CurModule)),
  ModuleDataLayout(Conv->CurModule)
{}

// vtable anchor
hydra::gpgpu::ConversionHelper::~ConversionHelper() {}

IRBuilder<>& hydra::gpgpu::ConversionHelper::getBuilder() {
  return Conv->Builder;
}

const TargetLibraryInfo& hydra::gpgpu::ConversionHelper::getTargetLibraryInfo(){
  return Conv->LibraryInfo;
}

Module* hydra::gpgpu::ConversionHelper::getModule() {
  return Conv->CurModule;
}

void hydra::gpgpu::ConversionHelper::postProcessing(
  CallSite /*CS*/,
  Value* /*DevicePtr*/,
  Value* /*MemAllocSize*/
) {}

void hydra::gpgpu::ConversionHelper::convert(CallSite CS) {
  auto Allocation = CS.getInstruction();
  IRBuilder<>& Builder = getBuilder();
  Builder.SetInsertPoint(Allocation); // insert just before call

  // CUDA mallocs use an output argument
  AllocaInst* DevicePtrPtr = createAllocaInEntryBlock(Allocation, CUdeviceptr);
  Value* MemAllocSize = getMemAllocSize(CS);

  // Using global visibility for cuMemAllocManaged
  Conv->convertToCUDACall(CS, CuMemAllocManaged, {
    DevicePtrPtr,
    MemAllocSize,
    Builder.getInt32(CU_MEM_ATTACH_GLOBAL)
  });

  // Allocation result is expected to be a pointer, not pointer to pointer, so
  // obtaining that value, converting to pointer of the right type, and
  // replacing the old allocation result. (The cast is more general than just
  // IntToPtr in case Nvidia switches from ints to actual pointers given that,
  // with 64-bit systems, the size is guaranteed to be valid in both device and
  // host memory spaces.)
  Value* DevicePtr = Builder.CreateLoad(DevicePtrPtr);
  DevicePtr = Builder.CreateBitOrPointerCast(
    DevicePtr,
    Allocation->getType()
  );

  // Different allocation functions require different or additional behavior
  // after the actual allocation
  postProcessing(CS, DevicePtr, MemAllocSize);

  // Some final cleanup so that the device allocation pointer completely
  // replaces the original allocation.
  DevicePtr->takeName(Allocation);
  Allocation->replaceAllUsesWith(DevicePtr);
  Allocation->eraseFromParent();
}

Value* hydra::gpgpu::ConversionHelper::getMemAllocSize(CallSite CS) {
  Value* AllocSize = getMemAllocSizeWithoutCast(CS);
  IRBuilder<>& Builder = getBuilder();

  // CreateIntCast is a no-op when the types are the same, so should be safe to
  // ensure consistency for malloc size argument; cast is unsigned because
  // allocation sizes are (supposed to be) unsigned.
  return Builder.CreateIntCast(AllocSize, Builder.getInt64Ty(), false);
}

Value* hydra::gpgpu::ConversionHelper::getStrLenWithoutNull(CallSite CS) {
  Value* StringPtr = CS.getArgument(0);
  IRBuilder<>& Builder = getBuilder();
  Value* Call = EmitStrLen(
    StringPtr,
    Builder,
    ModuleDataLayout,
    &getTargetLibraryInfo()
  );

  if (!Call) {
    // Library emit did't work, trying adding strlen manually; the program will
    // just fail to link if strlen() isn't present at all.
    Function* StrLen = getStrLen(getModule());
    Call = Builder.CreateCall(StrLen, StringPtr, "strlen");
  }

  return Call;
}

Value* hydra::gpgpu::MallocHelper::getMemAllocSizeWithoutCast(CallSite Malloc) {
  assert(Malloc.arg_size() == 1 && "malloc() should have only 1 argument");

  return Malloc.getArgument(0);
}

Value* hydra::gpgpu::CallocHelper::getMemAllocSizeWithoutCast(CallSite Calloc) {
  assert(Calloc.arg_size() == 2 && "calloc() should have exactly 2 arguments");

  Value* ItemCount = Calloc.getArgument(0);

  // Figure out the best way to pad this in the case of alignment restrictions
  // for structs of odd sizes? May not be an issue; if sizeof() was used to
  // determine the allocation size, then there's no worries as it includes any
  // padding specified by the target data layout.
  Value* ObjectSize = Calloc.getArgument(1);

  // Scaling (unsigned) to ensure multiplied value is not wrapped
  IRBuilder<>& Builder = getBuilder();
  ItemCount = Builder.CreateIntCast(ItemCount, Builder.getInt64Ty(), false);
  ObjectSize = Builder.CreateIntCast(ObjectSize, Builder.getInt64Ty(), false);

  // seeming signed-wrapping can occur even when the values are just fine
  // unsigned, so we only want to say there's no wrapping for the unsigned
  // representation
  return Builder.CreateNUWMul(ItemCount, ObjectSize);
}

void hydra::gpgpu::CallocHelper::postProcessing(
  CallSite /*CS*/,
  Value* DevicePtr,
  Value* MemAllocSize
) {
  IRBuilder<>& Builder = getBuilder();

  // DevicePtr is converted to i8* for memset so we can use i8 for the init 0
  // and won't worry about alignment.
  Builder.CreateMemSet(DevicePtr, Builder.getInt8(0), MemAllocSize, 1);
}

Value* hydra::gpgpu::StrdupHelper::getMemAllocSizeWithoutCast(CallSite Strdup) {
  assert(Strdup.arg_size() == 1 && "strdup() should have only 1 argument");

  Value* StringLengthWithoutNull = getStrLenWithoutNull(Strdup);
  IRBuilder<>& Builder = getBuilder();

  // We want to allocate strlen + 1 due to the expected null terminator; string
  // length can be assumed to not be so large that it will result in a wrap so
  // we can mark this add as non-wrapping.
  return Builder.CreateAdd(
    StringLengthWithoutNull,
    Builder.getInt64(1),
    "",
    true,
    true
  );
}

void hydra::gpgpu::StrdupHelper::postProcessing(
  CallSite CS,
  Value* DevicePtr,
  Value* MemAllocSize
) {
  // Assuming alignment 1 as we're copying chars
  getBuilder().CreateMemCpy(DevicePtr, CS.getArgument(0), MemAllocSize, 1);
}

Value* hydra::gpgpu::StrndupHelper::getMemAllocSizeWithoutCast(
  CallSite Strndup
) {
  assert(
    Strndup.arg_size() == 2 &&
    "strndup() should have exactly 2 arguments"
  );

  Value* StringLengthWithoutNull = getStrLenWithoutNull(Strndup);
  Value* NumCharactersToCopy = getConditionalMemAllocSize(
    StringLengthWithoutNull,
    Strndup.getArgument(1)
  );
  IRBuilder<>& Builder = getBuilder();

  // We want to allocate strlen + 1 due to the expected null terminator; string
  // length can be assumed to not be so large that it will result in a wrap so
  // we can mark this add as non-wrapping.
  return Builder.CreateAdd(
    NumCharactersToCopy,
    Builder.getInt64(1),
    "",
    true,
    true
  );
}

PHINode* hydra::gpgpu::StrndupHelper::getConditionalMemAllocSize(
  Value* StrLen,
  Value* N
) {
  IRBuilder<>& Builder = getBuilder();
  Value* Cond = Builder.CreateICmpUGE(StrLen, N); // StrLen >= N
  TerminatorInst* StrLenBiggerTerm = nullptr;
  TerminatorInst* NBiggerTerm = nullptr;

  Instruction* InsertPoint = &*Builder.GetInsertPoint();

  SplitBlockAndInsertIfThenElse(
    Cond,
    InsertPoint,
    &StrLenBiggerTerm,
    &NBiggerTerm
  );

  assert(InsertPoint == Builder.GetInsertPoint() && "Insert point changed");

  // Two branches to merge from; choose N if StrLen is bigger, otherwise choose
  // StrLen.
  PHINode* MemAllocSize = Builder.CreatePHI(StrLen->getType(), 2);
  MemAllocSize->addIncoming(N, StrLenBiggerTerm->getParent());
  MemAllocSize->addIncoming(StrLen, NBiggerTerm->getParent());

  return MemAllocSize;
}

void hydra::gpgpu::StrndupHelper::postProcessing(
  CallSite CS,
  Value* DevicePtr,
  Value* MemAllocSize
) {
  IRBuilder<>& Builder = getBuilder();

  // Only want to copy MemAllocSize-1 as we don't know if the final character is
  // actually '\0' or not so we have to set it ourselves. Luckily it should be
  // non-wrapping either way.
  Value* CharsToCopy = Builder.CreateSub(
    MemAllocSize,
    Builder.getInt64(1),
    "",
    true,
    true
  );
  Value* NullTerminationPoint = Builder.CreateInBoundsGEP(
    DevicePtr->getType(),
    DevicePtr,
    CharsToCopy
  );

  // Assuming alignment 1 as we're copying chars
  Builder.CreateMemCpy(DevicePtr, CS.getArgument(0), CharsToCopy, 1);
  Builder.CreateStore(Builder.getInt8(0), NullTerminationPoint);
}
