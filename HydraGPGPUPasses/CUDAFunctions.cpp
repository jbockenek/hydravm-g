//===- CUDAFunctions.cpp - Getters for CUDA driver API functions ----------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains definitions of functions to get declarations for
/// functions in the CUDA driver API (see
/// http://docs.nvidia.com/cuda/cuda-driver-api/index.html). Getters for named
/// (opaque) CUDA structs are also provided for function type clarity. Helpers
/// for functions that aren't actually part of the CUDA driver API but are still
/// necessary for us have also been included.
///
//===----------------------------------------------------------------------===//

#include "CUDAFunctions.h"

#include "HydraGPGPUConstants.h"

#include "llvm/IR/Attributes.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"

namespace llvm {
  template<typename T> class ArrayRef;
  class StringRef;
}

using namespace llvm;

namespace {
  // All CUDA functions return CUresult, so we can let this function handle
  // that. Not including attribute sets as most of the attrs for these seem to
  // be generated by LLVM passes anyway.
  Function* getCuFunction(Module* M, StringRef Name, ArrayRef<Type*> ArgTypes) {
    auto ErrCodeType = hydra::gpgpu::getCUresultTy(M);
    auto FT = FunctionType::get(ErrCodeType, ArgTypes, false); // non-variadic

    // If we get a constexpr bitcast instead, that means our function type is
    // borked and we should fail rather than attempting to continue.
    return cast<Function>(M->getOrInsertFunction(Name, FT));
  }

  StructType* structGetterBase(Module* M, StringRef StructName) {
    StructType* ST = M->getTypeByName(StructName);

    if (!ST) {
      ST = StructType::create(M->getContext(), StructName);
    }

    return ST;
  }
}

// @cuCtxCreate_v2(%struct.CUctx_st**, i32, i32)
Function* hydra::gpgpu::getCuCtxCreate(Module* M) {
  auto CUctxPtrPtr = getCUctxTy(M)->getPointerTo()->getPointerTo();
  auto i32 = Type::getInt32Ty(M->getContext());

  return getCuFunction(M, "cuCtxCreate_v2", {CUctxPtrPtr, i32, i32});
}

// @cuCtxDestroy_v2(%struct.CUctx_st*)
Function* hydra::gpgpu::getCuCtxDestroy(Module* M) {
  auto CUctxPtr = getCUctxTy(M)->getPointerTo();

  return getCuFunction(M, "cuCtxDestroy_v2", CUctxPtr);
}

// @cuCtxSynchronize()
Function* hydra::gpgpu::getCuCtxSynchronize(Module* M) {
  return getCuFunction(M, "cuCtxSynchronize", None);
}

// @cuDeviceComputeCapability(i32*, i32*, i32)
Function* hydra::gpgpu::getCuDeviceComputeCapability(Module* M) {
  auto i32 = Type::getInt32Ty(M->getContext());
  auto i32p = i32->getPointerTo();

  return getCuFunction(M, "cuDeviceComputeCapability", {
    i32p, // major version
    i32p, // minor version
    i32   // handle of device to query
  });
}

// @cuDeviceGet(i32*, i32) // get handle to device with specified ID
Function* hydra::gpgpu::getCuDeviceGet(Module* M) {
  auto i32 = Type::getInt32Ty(M->getContext());
  auto i32p = i32->getPointerTo();

  return getCuFunction(M, "cuDeviceGet", {i32p, i32});

}

// @cuDeviceGetCount(i32*)
Function* hydra::gpgpu::getCuDeviceGetCount(Module* M) {
  auto i32p = Type::getInt32PtrTy(M->getContext());

  return getCuFunction(M, "cuDeviceGetCount", i32p);
}

// @cuDeviceGetName(i8*, i32, i32)
Function* hydra::gpgpu::getCuDeviceGetName(Module* M) {
  LLVMContext& C = M->getContext();
  auto i8p = Type::getInt8PtrTy(C);
  auto i32 = Type::getInt32Ty(C);

  return getCuFunction(M, "cuDeviceGetName", {i8p, i32, i32});
}

// @cuGetErrorName(i32, i8**)
Function* hydra::gpgpu::getCuGetErrorName(Module* M) {
  LLVMContext& C = M->getContext();
  auto i32 = Type::getInt32Ty(C);
  auto StringPtr = Type::getInt8PtrTy(C)->getPointerTo(); // i8 is char for this

  return getCuFunction(M, "cuGetErrorName", {i32, StringPtr});
}

// @cuGetErrorString(i32, i8**)
Function* hydra::gpgpu::getCuGetErrorString(Module* M) {
  LLVMContext& C = M->getContext();
  auto i32 = Type::getInt32Ty(C);
  auto StringPtr = Type::getInt8PtrTy(C)->getPointerTo(); // i8 is char for this

  return getCuFunction(M, "cuGetErrorString", {i32, StringPtr});
}

// @cuInit(i32)
Function* hydra::gpgpu::getCuInit(Module* M) {
  auto i32 = Type::getInt32Ty(M->getContext());

  return getCuFunction(M, "cuInit", i32);
}

// @cuLaunchKernel(%struct.CUfunc_st*, i32, i32, i32, i32, i32, i32, i32,
// %struct.CUstream_st*, i8**, i8**)
Function* hydra::gpgpu::getCuLaunchKernel(Module* M) {
  LLVMContext& C = M->getContext();
  auto CUfuncPtr = getCUfuncTy(M)->getPointerTo();
  auto CUstreamPtr = getCUstreamTy(M)->getPointerTo();
  auto i32 = Type::getInt32Ty(C);
  auto VoidPtrArray = Type::getInt8PtrTy(C)->getPointerTo(); // i8** as void*[]

  return getCuFunction(M, "cuLaunchKernel", {
    CUfuncPtr,     // kernel function to use
    i32, i32, i32, // grid dims (x, y, z)
    i32, i32, i32, // block dims (x, y, z)
    i32,           // shared memory size in bytes
    CUstreamPtr,   // associated stream, default null
    VoidPtrArray,  // array of pointers to args
    VoidPtrArray   // array of pointers to extras
  });
}

// @cuMemAllocManaged(CUdeviceptr*, i64, i32)
Function* hydra::gpgpu::getCuMemAllocManaged(Module* M) {
  LLVMContext& C = M->getContext();
  auto i32 = Type::getInt32Ty(C);
  auto i64 = Type::getInt64Ty(C);
  auto CUdeviceptrPtr = getCUdeviceptrTy(M)->getPointerTo();

  return getCuFunction(M, "cuMemAllocManaged", {
    CUdeviceptrPtr, // memory location to store the pointer to allocated memory
    i64,            // size of allocation in bytes
    i32             // flags (global vs. host-only [zero-copy] allocation)
  });
}

// @cuMemFree_v2(CUdeviceptr)
Function* hydra::gpgpu::getCuMemFree(Module* M) {
  auto CUdeviceptr = getCUdeviceptrTy(M);

  return getCuFunction(M, "cuMemFree_v2", CUdeviceptr);
}

// @cuModuleGetFunction(%struct.CUfunc_st**, %struct.CUmod_st*, i8*)
Function* hydra::gpgpu::getCuModuleGetFunction(Module* M) {
  auto CUfuncPtrPtr = getCUfuncTy(M)->getPointerTo()->getPointerTo();
  auto CUmodPtr = getCUmodTy(M)->getPointerTo();
  auto CharPtr = Type::getInt8PtrTy(M->getContext());

  return getCuFunction(M, "cuModuleGetFunction", {
    CUfuncPtrPtr, // pointer to location to store the function pointer
    CUmodPtr,     // module to get the kernel function from
    CharPtr       // function name
  });
}

// @cuModuleLoad(%struct.CUmod_st**, i8*)
Function* hydra::gpgpu::getCuModuleLoad(Module* M) {
  auto CUmodPtrPtr = getCUmodTy(M)->getPointerTo()->getPointerTo();
  auto CharPtr = Type::getInt8PtrTy(M->getContext());

  // location to store module pointer and name of file to load module from
  return getCuFunction(M, "cuModuleLoad", {CUmodPtrPtr, CharPtr});
}

// @cuModuleUnload(%struct.CUmod_st*)
Function* hydra::gpgpu::getCuModuleUnload(Module* M) {
  auto CUmodPtr = getCUmodTy(M)->getPointerTo();

  return getCuFunction(M, "cuModuleUnload", CUmodPtr);
}

// void @exit(i32) noreturn readnone
Function* hydra::gpgpu::getExit(Module* M) {
  LLVMContext& C = M->getContext();
  auto FT = FunctionType::get(Type::getVoidTy(C), Type::getInt32Ty(C), false);
  auto Attrs = AttributeSet::get(C, AttributeSet::FunctionIndex, {
    Attribute::NoReturn,
    Attribute::ReadNone
  });

  return cast<Function>(M->getOrInsertFunction("exit", FT, Attrs));
}

// i64 @strlen(i8*) readonly
Function* hydra::gpgpu::getStrLen(Module* M) {
  LLVMContext& C = M->getContext();
  auto SizeT = Type::getInt64Ty(C);
  auto i8p = Type::getInt8PtrTy(C);

  auto FT = FunctionType::get(SizeT, i8p, false);
  auto Attrs = AttributeSet::get(
    C,
    AttributeSet::FunctionIndex,
    Attribute::ReadOnly
  );

  return cast<Function>(M->getOrInsertFunction("strlen", FT, Attrs));
}

// i32 @puts(i8*)
Function* hydra::gpgpu::getPutS(Module* M) {
  LLVMContext& C = M->getContext();
  auto i8p = Type::getInt8PtrTy(C);
  auto FT = FunctionType::get(Type::getInt32Ty(C), i8p, false);

  return cast<Function>(M->getOrInsertFunction("puts", FT));
}

// i32 @putchar(i32)
Function* hydra::gpgpu::getPutChar(Module* M) {
  auto i32 = Type::getInt32Ty(M->getContext());
  auto FT = FunctionType::get(i32, i32, false);

  return cast<Function>(M->getOrInsertFunction("putchar", FT));
}

StructType* hydra::gpgpu::getCUctxTy(Module* M) {
  return structGetterBase(M, "struct.CUctx_st");
}

StructType* hydra::gpgpu::getCUfuncTy(Module* M) {
  return structGetterBase(M, "struct.CUfunc_st");
}

StructType* hydra::gpgpu::getCUmodTy(Module* M) {
  return structGetterBase(M, "struct.CUmod_st");
}

StructType* hydra::gpgpu::getCUstreamTy(Module* M) {
  return structGetterBase(M, "struct.CUstream_st");
}

// Currently CUdeviceptr is i64 on 64-bit systems
Type* hydra::gpgpu::getCUdeviceptrTy(Module* M) {
  return Type::getInt64Ty(M->getContext());
}

// Currently CUresult is i32 (actually cudaError_enum, but LLVM doesn't support
// enum types)
Type* hydra::gpgpu::getCUresultTy(Module* M) {
  return Type::getInt32Ty(M->getContext());
}

Constant* hydra::gpgpu::getCudaSuccess(Module* M) {
  return ConstantInt::get(getCUresultTy(M), CUDA_SUCCESS);
}
