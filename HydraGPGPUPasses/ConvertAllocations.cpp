//===- ConvertAllocations.cpp - Pass for allocation conversion ------------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Converts allocations supplied from a previous analysis pass to allocations
/// on the GPU.
///
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "convert-allocations"

#include "AllocationConverters.h"
#include "AllocConvAnalysis.h"
#include "HydraGPGPUConstants.h"

using namespace llvm;
using namespace hydra::gpgpu;

// (Simple) passes are self-contained, only need anonymous namespace for one
// file
namespace {
  class ConvertAllocations : public ModulePass {
  public:
    static char ID; // Pass identification

    ConvertAllocations();

    void getAnalysisUsage(AnalysisUsage& AU) const override;
    bool runOnModule(Module& M) override;
  };
}

// non-CFG non-analysis pass
static RegisterPass<ConvertAllocations> X(
  "convert-allocs",
  "Convert allocations supplied from a previous analysis pass to GPGPU ones",
  false,
  false
);

char ConvertAllocations::ID = 0;
ConvertAllocations::ConvertAllocations() : ModulePass(ID) {}

void ConvertAllocations::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<AllocConvAnalysis>();
}

bool ConvertAllocations::runOnModule(Module& M) {
  auto& ACAnalysis = getAnalysis<AllocConvAnalysis>();

  return AllocConverter(M).convert(ACAnalysis);
}
