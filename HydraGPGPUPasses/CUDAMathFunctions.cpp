//===- CUDAMathFunctions.cpp - CUDA device math function decl stuff -------===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains stuff for CUDA device math functions and the like.
///
//===----------------------------------------------------------------------===//

#include "CUDAMathFunctions.h"

#include "llvm/ADT/StringMap.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/TypeBuilder.h"

#include <utility>

namespace llvm {
namespace types {
  // LLVM provides `half` type and CUDA provides intrinsics for manipulating
  // them so it makes sense to actually have them here. (Though the basic math
  // functions don't actually do anything with the `half` type.)
  class ieee_half {};
}

  template<bool cross> class TypeBuilder<types::ieee_half, cross> {
    public:
      static Type* get(LLVMContext& C) { return Type::getHalfTy(C); }
  };
}

using namespace llvm;

// Shortcuts for all the types used by CUDA math functions
namespace {
  template<typename T> using TB = TypeBuilder<T, true>;

  using ied = types::ieee_double;
  using ief = types::ieee_float;
  using ieh = types::ieee_half;
  using i8 = types::i<8>;
  using i32 = types::i<32>;
  using i64 = types::i<64>;

  using d5 = TB<ied(ied, ied, ied, ied)>;
  using d4 = TB<ied(ied, ied, ied)>;
  using d3 = TB<ied(ied, ied)>;
  using d3ip1 = TB<ied(ied, ied, i32*)>;
  using d2 = TB<ied(ied)>;
  using d2dp1 = TB<ied(ied, ied*)>;
  using d2i1 = TB<ied(ied, i32)>;
  using d2ip1 = TB<ied(ied, i32*)>;
  using d1i1dp1 = TB<ied(i32, ied*)>;
  using d1i1d1 = TB<ied(i32, ied)>;
  using d1charp1 = TB<ied(i8*)>;
  using d1i1 = TB<ied(i32)>;

  using f5 = TB<ief(ief, ief, ief, ief)>;
  using f4 = TB<ief(ief, ief, ief)>;
  using f3 = TB<ief(ief, ief)>;
  using f3ip1 = TB<ief(ief, ief, i32*)>;
  using f2 = TB<ief(ief)>;
  using f2fp1 = TB<ief(ief, ief*)>;
  using f2i1 = TB<ief(ief, i32)>;
  using f2ip1 = TB<ief(ief, i32*)>;
  using f1i1fp1 = TB<ief(i32, ief*)>;
  using f1i1f1 = TB<ief(i32, ief)>;
  using f1charp1 = TB<ief(i8*)>;

  using i3 = TB<i32(i32, i32)>;
  using i2 = TB<i32(i32)>;
  using i1d1 = TB<i32(ied)>;
  using i1f1 = TB<i32(ief)>;

  using ill2 = TB<i64(i64)>;
  using ill3 = TB<i64(i64, i64)>;
  using ill1d1 = TB<i64(ied)>;
  using ill1f1 = TB<i64(ief)>;

  using v1d1dp2 = TB<void(ied, ied*, ied*)>;
  using v1f1fp2 = TB<void(ief, ief*, ief*)>;

  using GetterType = FunctionType* (*)(LLVMContext&);
  // works too (but NEEDS ampersand; `FunctionType* ()(LLVMContext&)` as well as
  // `decltype(d5::get)` are both valid function types, but you cannot actually
  // use those to pass functions around. You have to use the pointer-to-function
  // type, even though function names transparently decay to them under normal
  // circumstances.)
//  using GetterType = decltype(&d5::get);

  // There's surely a better/more maintainable way of doing this, but I'm not
  // really sure of the best way of doing so. Using the LLVM `.def` macro style
  // is one possible way...
  const StringMap<GetterType> TypeMap{
    {"norm4d",         d5::get},
    {"rnorm4d",        d5::get},
    {"norm4df",        f5::get},
    {"rnorm4df",       f5::get},
    {"fma",            d4::get},
    {"norm3d",         d4::get},
    {"rnorm3d",        d4::get},
    {"sincos",         v1d1dp2::get},
    {"sincospi",       v1d1dp2::get},
    {"fmaf",           f4::get},
    {"norm3df",        f4::get},
    {"rnorm3df",       f4::get},
    {"sincosf",        v1f1fp2::get},
    {"sincospif",      v1f1fp2::get},
    {"remquo",         d3ip1::get},
    {"remquof",        f3ip1::get},
    {"norm",           d1i1dp1::get},
    {"rnorm",          d1i1dp1::get},
    {"normf",          f1i1fp1::get},
    {"rnormf",         f1i1fp1::get},
    {"atan2",          d3::get},
    {"copysign",       d3::get},
    {"fdim",           d3::get},
    {"fmax",           d3::get},
    {"fmin",           d3::get},
    {"fmod",           d3::get},
    {"nextafter",      d3::get},
    {"pow",            d3::get},
    {"remainder",      d3::get},
    {"rhypot",         d3::get},
    {"jn",             d1i1d1::get},
    {"yn",             d1i1d1::get},
    {"modf",           d2dp1::get},
    {"atan2f",         f3::get},
    {"copysignf",      f3::get},
    {"fdimf",          f3::get},
    {"fdividef",       f3::get},
    {"fmaxf",          f3::get},
    {"fminf",          f3::get},
    {"fmodf",          f3::get},
    {"nextafterf",     f3::get},
    {"powf",           f3::get},
    {"remainderf",     f3::get},
    {"rhypotf",        f3::get},
    {"jnf",            f1i1f1::get},
    {"ynf",            f1i1f1::get},
    {"modff",          f2fp1::get},
    {"scalbn",         d2i1::get},
    {"ldexpf",         f2i1::get},
    {"scalbnf",        f2i1::get},
    {"frexp",          d2ip1::get},
    {"frexpf",         f2ip1::get},
    {"nan",            d1charp1::get},
    {"nanf",           f1charp1::get},
    {"acos",           d2::get},
    {"acosh",          d2::get},
    {"asin",           d2::get},
    {"asinh",          d2::get},
    {"atan",           d2::get},
    {"atanh",          d2::get},
    {"cbrt",           d2::get},
    {"ceil",           d2::get},
    {"cos",            d2::get},
    {"cosh",           d2::get},
    {"cospi",          d2::get},
    {"cyl_bessel_i0",  d2::get},
    {"cyl_bessel_i1",  d2::get},
    {"erf",            d2::get},
    {"erfc",           d2::get},
    {"erfcinv",        d2::get},
    {"erfcx",          d2::get},
    {"erfinv",         d2::get},
    {"exp",            d2::get},
    {"exp10",          d2::get},
    {"exp2",           d2::get},
    {"expm1",          d2::get},
    {"fabs",           d2::get},
    {"floor",          d2::get},
    {"j0",             d2::get},
    {"j1",             d2::get},
    {"lgamma",         d2::get},
    {"log",            d2::get},
    {"log10",          d2::get},
    {"log1p",          d2::get},
    {"log2",           d2::get},
    {"logb",           d2::get},
    {"nearbyint",      d2::get},
    {"normcdf",        d2::get},
    {"normcdfinv",     d2::get},
    {"rcbrt",          d2::get},
    {"rint",           d2::get},
    {"round",          d2::get},
    {"rsqrt",          d2::get},
    {"sin",            d2::get},
    {"sinh",           d2::get},
    {"sinpi",          d2::get},
    {"sqrt",           d2::get},
    {"tan",            d2::get},
    {"tanh",           d2::get},
    {"tgamma",         d2::get},
    {"trunc",          d2::get},
    {"y0",             d2::get},
    {"y1",             d2::get},
    {"ilogb",          i1d1::get},
    {"llrint",         ill1d1::get},
    {"llround",        ill1d1::get},
    {"acosf",          f2::get},
    {"acoshf",         f2::get},
    {"asinf",          f2::get},
    {"asinhf",         f2::get},
    {"atanf",          f2::get},
    {"atanhf",         f2::get},
    {"cbrtf",          f2::get},
    {"ceilf",          f2::get},
    {"cosf",           f2::get},
    {"coshf",          f2::get},
    {"cospif",         f2::get},
    {"cyl_bessel_i0f", f2::get},
    {"cyl_bessel_i1f", f2::get},
    {"erfcf",          f2::get},
    {"erfcinvf",       f2::get},
    {"erfcxf",         f2::get},
    {"erff",           f2::get},
    {"erfinvf",        f2::get},
    {"exp10f",         f2::get},
    {"exp2f",          f2::get},
    {"expf",           f2::get},
    {"expm1f",         f2::get},
    {"fabsf",          f2::get},
    {"floorf",         f2::get},
    {"j0f",            f2::get},
    {"j1f",            f2::get},
    {"lgammaf",        f2::get},
    {"log10f",         f2::get},
    {"log1pf",         f2::get},
    {"log2f",          f2::get},
    {"logbf",          f2::get},
    {"logf",           f2::get},
    {"nearbyintf",     f2::get},
    {"normcdff",       f2::get},
    {"normcdfinvf",    f2::get},
    {"rcbrtf",         f2::get},
    {"rintf",          f2::get},
    {"roundf",         f2::get},
    {"rsqrtf",         f2::get},
    {"sinf",           f2::get},
    {"sinhf",          f2::get},
    {"sinpif",         f2::get},
    {"sqrtf",          f2::get},
    {"tanf",           f2::get},
    {"tanhf",          f2::get},
    {"tgammaf",        f2::get},
    {"truncf",         f2::get},
    {"y0f",            f2::get},
    {"y1f",            f2::get},
    {"ilogbf",         i1f1::get},
    {"llrintf",        ill1f1::get},
    {"llroundf",       ill1f1::get},
    {"ldexp",          d1i1::get},
    {"hypot",          d3::get},
    {"hypotf",         f3::get},
    {"abs",            i2::get},
    {"llabs",          ill2::get},
    {"min",            i3::get},
    {"umin",           i3::get},
    {"llmin",          ill3::get},
    {"ullmin",         ill3::get},
    {"max",            i3::get},
    {"umax",           i3::get},
    {"llmax",          ill3::get},
    {"ullmax",         ill3::get},
    {"__isinff",       i1f1::get},
    {"__isnanf",       i1f1::get},
    {"__isfinited",    i1d1::get},
    {"__isfinitef",    i1f1::get},
    {"__signbitd",     i1d1::get},
    {"__isnand",       i1d1::get},
    {"__isinfd",       i1d1::get},
    {"__finite",       i1d1::get},
    {"__finitef",      i1f1::get},
    {"__signbit",      i1d1::get},
    {"__isnan",        i1d1::get},
    {"__isinf",        i1d1::get},
    {"__signbitf",     i1f1::get},
  };
}

FunctionType* hydra::gpgpu::getCUDAMathFunTy(
  const StringRef name,
  LLVMContext& C
) {
  const auto& it = TypeMap.find(name);
  if (it == TypeMap.end()) {
    return nullptr;
  }

  auto& getter = it->getValue();
  return getter(C);
}
