//===- DeviceInternalize.cpp - Internalize non-kernel device functions ----===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Set internal linkage for kernel module functions that are not listed in
/// NVVM metadata as kernels.
///
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "device-internalize"

#include "HydraGPGPUConstants.h"

#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"

using namespace llvm;

// (Simple) passes are self-contained, only need anonymous namespace for one
// file
namespace {
  using FunctionSet = SmallPtrSet<
    const Function*,
    hydra::gpgpu::BASE_SMALLSET_SIZE
  >;

  class DeviceInternalize : public ModulePass {
  public:
    static char ID; // Pass identification

    DeviceInternalize();

    bool runOnModule(Module& M) override;
    void getAnalysisUsage(AnalysisUsage& AU) const override;
  };

  // NVVM kernel annotations are of the form
  // `metadata !{<function-ref>, metadata !"kernel", i32 1}`; this function
  // returns the ref if the supplied node has the right format, otherwise it
  // returns null.
  Function* KernelFromNode(MDNode* Node) {
    LLVMContext& C = Node->getContext();
    auto KernelStr = MDString::get(C, "kernel");

    auto ConstOne = ConstantInt::get(Type::getInt32Ty(C), 1);
    auto One = ConstantAsMetadata::get(ConstOne);

    if (
      Node->getNumOperands() == 3 &&
      Node->getOperand(2) == One &&
      Node->getOperand(1) == KernelStr
    ) {
      if (auto ConstData = dyn_cast<ConstantAsMetadata>(Node->getOperand(0))) {
        return dyn_cast<Function>(ConstData->getValue());
      }
    }

    return nullptr;
  }

  FunctionSet getKernelFunctions(Module& M) {
    FunctionSet Kernels;

    NamedMDNode* NVVMAnnotations = M.getNamedMetadata(hydra::gpgpu::NVVM_META);
    assert(NVVMAnnotations && "Not a kernel module!");

    for (auto Node : NVVMAnnotations->operands()) {
      if (Function* F = KernelFromNode(Node)) {
        Kernels.insert(F);
      }
    }

    return Kernels;
  }
}

// non-CFG non-analysis pass
static RegisterPass<DeviceInternalize> X(
  "device-internalize",
  "Set internal linkage for non-kernel device functions",
  false,
  false
);

char DeviceInternalize::ID = 0;
DeviceInternalize::DeviceInternalize() : ModulePass(ID) {}

bool DeviceInternalize::runOnModule(Module& M) {
  bool Modified = false;
  auto& Funcs = M.getFunctionList();
  FunctionSet Kernels = getKernelFunctions(M);

  for (Function& F : Funcs) {
    // Original internalize pass doesn't operate on declarations either
    if (F.isDeclaration() || F.hasInternalLinkage() || Kernels.count(&F))
      continue;

    F.setLinkage(GlobalValue::InternalLinkage);
    Modified = true;
  }

  return Modified;
}

void DeviceInternalize::getAnalysisUsage(AnalysisUsage& /*AU*/) const {
  // We're changing function signatures, which could invalidate analyses
}
