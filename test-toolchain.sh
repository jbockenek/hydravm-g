#!/bin/bash

# The results of each step could be piped to the next rather than written to
# files, but using intermediate files allows us to examine them if anything goes
# wrong.

PASS_LIB=../lib/HydraGPGPUPasses.so
HOST_OPTS='-inline -scalarrepl -mem2reg -loop-simplify -dce -indvars -convert-allocs -extract-loops -simplifycfg -S'
KERNEL_OPTS='-mark -convert-loops -dce -simplifycfg -S'

# Don't want to continue build process if any steps fail
set -e

mkdir -p build

cd build
clang++ -std=c++11 -emit-llvm -S ../testing/loopx.cpp -o loopx.ll
opt -load $PASS_LIB $HOST_OPTS loopx.ll > loopx2.ll
opt -load $PASS_LIB $KERNEL_OPTS loopx.ll.kernel > loopx2.ll.kernel
llc -mcpu=sm_30 loopx2.ll.kernel -o loopx.ll.kernel.ptx
clang++ -std=c++11 -lcuda loopx2.ll -o loopx2
