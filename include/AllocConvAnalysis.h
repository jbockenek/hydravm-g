//===- AllocConvAnalysis.h - Alloc analysis for GPGPU params ----*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Handles the memory builtins supported in \c llvm/Analysis/MemoryBuiltins.h
/// (this unfortunately disallows things like strdupa, strndupa, wcsdup,
/// wcsndup, or posix_memalign, as those are not recognized as builtins by
/// TargetLibraryInfo, but in cases where they are implemented as macros rather
/// than functions we should be okay). Memory allocated in shared libraries
/// isn't handlable either.
///
/// Note that, when not overloaded, placement new is to some degree handled by
/// the front end; it gets converted into a call to the resolved constructor
/// (possibly inlined), which takes the memory location to use as \c this as its
/// first argument. Default new functions similarly, but first uses built-in
/// \c new (which may throw and isn't always reported as a builtin) to allocate
/// the right amount of memory. For constructors that are invoked rather than
/// called, we'll have to do a \c nullptr check on the result of \c new first
/// (the issue is identifying which instruction may be the constructor).
///
/// Overloaded new/delete in shared libraries probably won't be supported,
/// though.
///
/// Also, most allocas DO NOT need to be converted. Kernels that use the value
/// stored in the alloca'd location and nothing more can simply have the alloca
/// supplied as that parameter. Alloca'd static-length arrays may still need to
/// be converted, though, if cuLaunchKernel doesn't support passing known-length
/// arrays by value (such representations in LLVM IR may just be converted into
/// pointers by the backend).
///
/// Assuming atomics and volatiles are not initially in use as this is meant for
/// conversion of sequential programs that do not take advantage of
/// atomics/volatiles.
///
/// Warning: we do not currently handle proper alignment for multi-dimensional
/// arrays. While this may not be much of an issue on current GPUs with caches,
/// it can increase the number of needed memory accesses if coalescing is not
/// utilizable (see
/// http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#device-memory-accesses).
///
/// Apparently the new PassManager won't be supporting analysis groups, but I
/// can't find any documentation for the new PassManager methodology besides the
/// new-style passes themselves so we're sticking with the legacy style for now.
///
//===----------------------------------------------------------------------===//
//
// Another future avenue == determining when in-function allocations can be
// reused so we don't have to free them until after the function is guaranteed
// to no longer be called (e.g. if there is no path in the interprocedural CFG
// after a call to the function that could eventually lead to the function
// being called again); however, liberal use of function pointers and indirect
// calls may push the guaranteed "dead" state for most functions way down in the
// CFG, in which case it may be better in terms of total memory usage to just
// free at the end of the function after all.
//
// @llvm.lifetime.start and @llvm.lifetime.end may be useful
//

#ifndef HYDRA_GPGPU_ALLOC_CONV_ANALYSIS_H
#define HYDRA_GPGPU_ALLOC_CONV_ANALYSIS_H

#include "llvm/IR/CallSite.h"
#include "llvm/Support/Casting.h"

// forward declarations to avoid excess header file inclusion
namespace llvm {
  class AllocaInst;
  class AnalysisUsage;
  class Function;
  class GlobalVariable;
}

inline llvm::raw_ostream& operator<<(
    llvm::raw_ostream& O,
    const llvm::CallSite& CS
) {
  return O << *CS.getInstruction();
}

namespace hydra {
namespace gpgpu {
  using AllocaInstListType     = std::vector<llvm::AllocaInst*>;
  using CallSiteListType       = std::vector<llvm::CallSite>;
  using GlobalVariableListType = std::vector<llvm::GlobalVariable*>;

  class AllocConvAnalysis {
  public:
    /// Discriminator for LLVM-style RTTI (dyn_cast<> et al.)
    enum AllocConvAnalysisKind {
      ACAK_Basic
    };

    static char ID; // Pass group identification
  private:
    const AllocConvAnalysisKind Kind;
  protected:
    /// As with alias analysis, all allocation conversion analysis
    /// implementations other than the default should invoke this directly
    /// (using AllocConvAnalysis::getAnalysisUsage(AU)).
    virtual void getAnalysisUsage(llvm::AnalysisUsage& AU) const;
  public:
    AllocConvAnalysisKind getKind() const;

    AllocConvAnalysis(AllocConvAnalysisKind K);

    // Need virtual destructor for proper polymorphic behavior
    virtual ~AllocConvAnalysis();

    /// \brief Returns a list of all automatic allocations in a function that
    /// may be supplied to or used in kernel functions and thus need to be
    /// converted to managed memory allocations.
    ///
    /// Note that each new managed alloc will need a corresponding device free
    /// call at the end of the function they are called in. (Future work may
    /// involve determining when those allocations can be reused rather than
    /// freed, which would save lots of time spent allocating.)
    virtual AllocaInstListType& getAllocas(llvm::Function& F) = 0;

    /// \brief Returns a list of all basic dynamic allocations in a function
    /// that may be supplied to or used in kernel functions and thus need to be
    /// converted to managed memory allocations. While \c valloc is supported,
    /// we do not guarantee page alignment; we only have the memory alignment
    /// guaranteed by the GPU driver. This may or may not cause issues.
    ///
    /// Note that no-throw \c new is also included here for when we are able to
    /// properly handle it in the future.
    ///
    /// As malloced data is not automatically freed in the functions that create
    /// it, proper handling requires in-depth interprocedural analysis.
    virtual CallSiteListType& getMallocs(llvm::Function& F) = 0;

    /// \brief Returns a list of all calloc-style dynamic allocations in a
    /// function that may be supplied to or used in kernel functions and thus
    /// need to be converted to managed memory allocations.
    ///
    /// Unfortunately, the CUDA APIs have no calloc equivalent, which means we
    /// need to replace such uses with the following:
    /// 1) managed malloc (num * size) // this is fine as GPU alignment matches
    ///                                // type size, at least for 1D arrays
    /// 2) memset(managed, 0, num * size) (@llvm.memset.p0i*.i64)
    virtual CallSiteListType& getCallocs(llvm::Function& F) = 0;

    /// \brief Returns a list of all dynamic reallocations in a function that
    /// may be supplied to or used in kernel functions and thus need to be
    /// converted to managed memory allocations. (Does not include reallocf.)
    ///
    /// Unfortunately, the CUDA APIs have no realloc equivalent, which means we
    /// need to replace such uses with the following:
    ///   1.  cuMemAllocManaged w/ new size
    ///   2a. if newsize > oldsize, memcpy from oldptr to newptr up to oldsize
    ///       (@llvm.memcpy.p0i*.p0i*.i64)
    ///   2b. otherwise, memcpy from oldptr to newptr up to newsize
    ///   3.  cuMemFree old pointer only if allocation successful
    ///
    /// ... But how do we know the old size? The system libraries that handle
    /// memory allocation usually keep track of allocation sizes.
    virtual CallSiteListType& getReallocs(llvm::Function& F) = 0;

    /// \brief Returns a list of all uses of reallocf (a BSD library function)
    /// in a function that may be supplied to or used in kernel functions and
    /// thus need to be converted to managed memory allocations.
    ///
    /// Unfortunately, the CUDA APIs have no reallocf equivalent, which means we
    /// need to replace such uses with the following:
    ///   1.  cuMemAllocManaged w/ new size
    ///   2a. if newsize > oldsize, memcpy from oldptr to newptr up to oldsize
    ///   2b. otherwise, memcpy from oldptr to newptr up to newsize
    ///   3.  cuMemFree old pointer (reallocf always frees it)
    virtual CallSiteListType& getReallocfs(llvm::Function& F) = 0;

    /// \brief Returns a list of all results of (may-throw) operator new([]) in
    /// a function that may be supplied to or used in kernel functions and thus
    /// need to be converted to managed memory allocations.
    virtual CallSiteListType& getNews(llvm::Function& F) = 0;

    /// \brief Returns a list of all results of (nothrow) operator new([]) in a
    /// function that may be supplied to or used in kernel functions and thus
    /// need to be converted to managed memory allocations.
    virtual CallSiteListType& getNothrowNews(llvm::Function& F) = 0;

    /// \brief Returns a list of all uses of `free` on memory that may be
    /// supplied to or used in kernel functions, which will need to be converted
    /// to managed memory frees.
    virtual CallSiteListType& getFrees(llvm::Function& F) = 0;

    /// \brief Returns a list of all uses of (may-throw) operator delete([]) on
    /// memory that may be supplied to or used in kernel functions, which will
    /// need to be converted to managed memory frees (possibly with explicit
    /// destructor calls).
    virtual CallSiteListType& getDeletes(llvm::Function& F) = 0;

    /// \brief Returns a list of all uses of (nothrow) operator delete([]) on
    /// memory that may be supplied to or used in kernel functions, which will
    /// need to be converted to managed memory frees (possibly with explicit
    /// destructor calls).
    virtual CallSiteListType& getNothrowDeletes(llvm::Function& F) = 0;

    /// \brief Returns a list of all uses of strdup that may be supplied to or
    /// used in kernel functions, which will need to be converted to managed
    /// memory allocations.
    ///
    /// Replacement algorithm:
    /// 1) managed malloc (strlen + 1)
    /// 2) memcpy to managed (strlen + 1)
    virtual CallSiteListType& getStrdups(llvm::Function& F) = 0;

    /// \brief Returns a list of all uses of strndup that may be supplied to or
    /// used in kernel functions, which will need to be converted to managed
    /// memory allocations.
    ///
    /// Replacement algorithm:
    /// 1) managed malloc (n)
    /// 2) strncpy to managed
    virtual CallSiteListType& getStrndups(llvm::Function& F) = 0;

    /// \brief Returns a list of all globals that may be supplied to or used in
    /// kernel functions and thus need to be converted to managed memory
    /// allocations
    virtual GlobalVariableListType& getGlobals() = 0;

    /// \brief Returns a list of all constant globals that may be supplied to or
    /// used in kernel functions and thus need to be copied to the kernel module
    /// and updated inside the kernel functions
    virtual GlobalVariableListType& getConstGlobals() = 0;
  };
}
}

#endif // HYDRA_GPGPU_ALLOC_CONV_ANALYSIS_H
