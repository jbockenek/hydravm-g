//===- AllocationConverters.h - Tools for allocation conversion -*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Provides classes to assist in the allocation conversion process.
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_ALLOCATION_CONVERTERS_H
#define HYDRA_GPGPU_ALLOCATION_CONVERTERS_H

#include "AllocConvAnalysis.h"
#include "Utils/ConstantExprConverter.h"

#include "llvm/ADT/ArrayRef.h"
#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/IR/IRBuilder.h"

// Forward declarations to reduce header usage
namespace llvm {
  class AllocaInst;
  class CallInst;
  class CallSite;
  class ConstantInt;
  class DataLayout;
  class Function;
  class GlobalVariable;
  class InvokeInst;
  class LLVMContext;
  class Module;
  class PHINode;
  class Type;
  class Value;
}

namespace hydra {
namespace gpgpu {
  class AllocConverter {
    // Usage of friend increases coupling, but for now ConversionHelper is
    // coupled tightly with Converter anyway so it doesn't matter.
    friend class ConversionHelper;
    template<typename T> friend class AllocaGlobalHelperBase;

    llvm::Module* CurModule;
    llvm::LLVMContext& Context;
    llvm::IRBuilder<> Builder;
    llvm::TargetLibraryInfo LibraryInfo;
    bool AddErrorChecking;

    void convertAllocas(AllocaInstListType& Allocas);
    void convertMallocs(CallSiteListType& Mallocs);
    void convertCallocs(CallSiteListType& Callocs);
    void convertStrdups(CallSiteListType& Strdups);
    void convertStrndups(CallSiteListType& Strndups);
    void convertFrees(CallSiteListType& Frees);
    void convertGlobals(GlobalVariableListType& Globals);

    llvm::CallInst* createCall(
      llvm::Function* ToCall,
      llvm::ArrayRef<llvm::Value*> Args
    );
    llvm::InvokeInst* createInvoke(
      llvm::Function* ToInvoke,
      llvm::InvokeInst* OldInvoke,
      llvm::ArrayRef<llvm::Value*> Args
    );

    /// \brief Replaces the instruction for the supplied call site with the
    /// appropriate call/invoke for the supplied function + args.
    ///
    /// Also performs error checking if desired (or rather, it will once that's
    /// implemented).
    void convertToCUDACall(
      llvm::CallSite CS,
      llvm::Function* CUDAFunc,
      llvm::ArrayRef<llvm::Value*> Args
    );

    void convertToCUDACall(
      llvm::AllocaInst* Alloca,
      llvm::Function* CUDAFunc,
      llvm::ArrayRef<llvm::Value*> Args
    );

    /// \brief Inserts the CUDA call at the start of the module's main()
    void convertToCUDACall(
      llvm::Function* CUDAFunc,
      llvm::ArrayRef<llvm::Value*> Args
    );

    /// \brief As \c convertToCUDACall(), but also removes the instruction at
    /// the supplied call site.
    void convertToCUDACallWithRemove(
      llvm::CallSite CS,
      llvm::Function* CUDAFunc,
      llvm::ArrayRef<llvm::Value*> Args
    );

    void convertFree(llvm::CallSite Free);
  public:
    AllocConverter(llvm::Module& M, bool AddErrorChecking=true);

    bool convert(AllocConvAnalysis& ACAnalysis);
  };

  // Code for malloc/etc. shared behavior goes in this class; specific behavior
  // controlled by subclasses.
  //
  // We don't necessarily need to use `virtual` here as all the instances will
  // be of the appropriate class, but it doesn't hurt for future-proofing's sake
  class ConversionHelper {
  protected:
    AllocConverter* Conv;
    llvm::Function* CuMemAllocManaged;
    llvm::Type* CUdeviceptr;
    llvm::DataLayout ModuleDataLayout;

    /// \brief Determines the size to use for the converted allocation.
    ///
    /// Does not ensure allocation size is of the appropriate type; also, pure
    /// virtual as different allocation functions determine the total allocation
    /// size in different ways.
    virtual llvm::Value* getMemAllocSizeWithoutCast(llvm::CallSite CS) = 0;

    /// \brief Determines the size to use for the converted allocation and
    /// ensures the data type is appropriate.
    ///
    /// Not virtual as it calls through to getMemAllocSizeWithoutCast().
    llvm::Value* getMemAllocSize(llvm::CallSite CS);

    /// \brief Performs additional tasks after the actual allocation; defaults
    /// to nothing.
    ///
    /// e.g. calloc() initializes all data to 0, etc.
    virtual void postProcessing(
      llvm::CallSite CS,
      llvm::Value* DevicePtr,
      llvm::Value* MemAllocSize
    );

    /// \brief Enables access to the friend's builder even in subclasses
    llvm::IRBuilder<>& getBuilder();

    /// \brief Enables access to the friend's TLI even in subclasses
    const llvm::TargetLibraryInfo& getTargetLibraryInfo();

    /// \brief Enables access to the friend's module even in subclasses
    llvm::Module* getModule();

    llvm::Value* getStrLenWithoutNull(llvm::CallSite CS);
  public:
    ConversionHelper(AllocConverter* Conv);
    virtual ~ConversionHelper();

    void convert(llvm::CallSite CS);
  };

  /// \brief Base class for AllocaHelper and GlobalHelper
  template<typename T>
  class AllocaGlobalHelperBase {
  protected:
    AllocConverter* Conv;
    llvm::Function* CuMemAllocManaged;
    llvm::Type* CUdeviceptr;
    llvm::DataLayout ModuleDataLayout;

    // Should only be called by child classes
    AllocaGlobalHelperBase(AllocConverter* Conv);

    /// \brief Simplifies access to the friend's builder
    llvm::IRBuilder<>& getBuilder();

    /// \brief Simplifies access to the friend's TLI
    const llvm::TargetLibraryInfo& getTargetLibraryInfo();

    ///  Simplifies access to the friend's module
    llvm::Module* getModule();

    void convertToCUDACall(
      llvm::AllocaInst* OldAlloca,
      llvm::AllocaInst* DevicePtrPtr,
      llvm::Value* MemAllocSize
    );
    void convertToCUDACall(
      llvm::GlobalVariable* Dummy,
      llvm::GlobalVariable* DevicePtrPtr,
      llvm::Value* MemAllocSize
    );

    /// \brief Determines the size to use for the converted allocation and
    /// ensures the data type is appropriate.
    virtual llvm::Value* getMemAllocSize(T* Object) = 0;

    /// \brief the function to insert memory allocation/freeing in
    virtual llvm::Function* getFunction(T* Object) = 0;

    /// \brief Creates the new deviceptr ptr
    virtual T* createDevicePtrPtr(T* Object) = 0;

    virtual void setInsertPoint(T* Object) = 0;
    virtual void postProcessing(T* OldObject, T* DevicePtrPtr) = 0;
  public:
    virtual ~AllocaGlobalHelperBase();

    void convert(T* Object);
  };

  /// \brief Specialization for Alloca conversions
  class AllocaHelper : public AllocaGlobalHelperBase<llvm::AllocaInst> {
  protected:
    llvm::Value* getMemAllocSize(llvm::AllocaInst* Alloca) override;
    llvm::Function* getFunction(llvm::AllocaInst* Alloca) override;
    void setInsertPoint(llvm::AllocaInst* Alloca) override;

    /// \brief creates deviceptr ptr in current function
    llvm::AllocaInst* createDevicePtrPtr(llvm::AllocaInst* Alloca) override;

    void postProcessing(
      llvm::AllocaInst* OldAlloca,
      llvm::AllocaInst* DevicePtrPtr
    ) override;
  public:
    AllocaHelper(AllocConverter* Conv);
  };

  /// \brief Specialization for global variable conversions
  class GVHelper : public AllocaGlobalHelperBase<llvm::GlobalVariable> {
    ConstantExprConverter CEConverter;
  protected:
    llvm::Value* getMemAllocSize(llvm::GlobalVariable* GV) override;

    /// \brief Returns main() of the module containing the GV
    llvm::Function* getFunction(llvm::GlobalVariable* GV) override;

    void setInsertPoint(llvm::GlobalVariable* GV) override;

    /// \brief creates deviceptr ptr in current function
    llvm::GlobalVariable* createDevicePtrPtr(llvm::GlobalVariable* GV) override;

    /// \brief Visiting every use of the old global and replacing it with a load
    /// of the new
    void postProcessing(
      llvm::GlobalVariable* OldGlobalVariable,
      llvm::GlobalVariable* DevicePtrPtr
    ) override;

    // assumes builder is set to the right place
    llvm::Value* getDevicePtr(
      llvm::GlobalVariable* OldGlobalVariable,
      llvm::GlobalVariable* DevicePtrPtr
    );
  public:
    GVHelper(AllocConverter* Conv);
  };

  class MallocHelper : public ConversionHelper {
  protected:
    llvm::Value* getMemAllocSizeWithoutCast(llvm::CallSite Malloc) override;
  public:
    MallocHelper(AllocConverter* Conv) : ConversionHelper(Conv) {}
  };

  class CallocHelper : public ConversionHelper {
  protected:
    llvm::Value* getMemAllocSizeWithoutCast(llvm::CallSite Calloc) override;

    /// \brief Initializes allocated memory to all 0s
    void postProcessing(
      llvm::CallSite CS,
      llvm::Value* DevicePtr,
      llvm::Value* MemAllocSize
    ) override;
  public:
    CallocHelper(AllocConverter* Conv) : ConversionHelper(Conv) {}
  };

  class StrdupHelper : public ConversionHelper {
  protected:
    llvm::Value* getMemAllocSizeWithoutCast(llvm::CallSite Strdup) override;

    /// \brief Copies over the string
    void postProcessing(
      llvm::CallSite CS,
      llvm::Value* DevicePtr,
      llvm::Value* MemAllocSize
    ) override;
  public:
    StrdupHelper(AllocConverter* Conv) : ConversionHelper(Conv) {}
  };

  class StrndupHelper : public ConversionHelper {
    llvm::PHINode* getConditionalMemAllocSize(
      llvm::Value* StrLen,
      llvm::Value* N
    );
  protected:
    llvm::Value* getMemAllocSizeWithoutCast(llvm::CallSite Strndup) override;

    /// \brief Copies over the string up to MemAllocSize-1, which may be less
    /// than the string length.
    void postProcessing(
      llvm::CallSite CS,
      llvm::Value* DevicePtr,
      llvm::Value* MemAllocSize
    ) override;
  public:
    StrndupHelper(AllocConverter* Conv) : ConversionHelper(Conv) {}
  };
}
}

#endif // HYDRA_GPGPU_ALLOCATION_CONVERTERS_H
