//===- LoopCompatAnalysis.h - Analysis group for loop compat ----*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Provides a base class for the loop compatibility analysis group, which can
/// be used to determine what loops are suitable for GPGPU extraction. Providing
/// chaining may be useful to support multiple types of analysis together.
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_LOOP_COMPAT_ANALYSIS_H
#define HYDRA_GPGPU_LOOP_COMPAT_ANALYSIS_H

#include "HydraGPGPUConstants.h"

#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/ADT/SmallVector.h"

#include <utility>
#include <vector>

namespace llvm {
  class AnalysisUsage;
  class BasicBlock;
  class Function;
}

namespace hydra {
namespace gpgpu {
  using LoopHeaderListType =
    llvm::SmallVector<llvm::BasicBlock*, BASE_SMALLVECTOR_SIZE>;

  /// \brief Associating loop headers with the functions containing them
  ///
  /// This allows us to recreate each function pass once rather than for each
  /// header block.
  using FuncWithLoopHeadersListType =
    std::vector<std::pair<llvm::Function*, LoopHeaderListType>>;

  using FuncsToConvertType =
    llvm::SmallPtrSet<const llvm::Function*, BASE_FUNCS_TO_CONVERT_SIZE>;

  class LoopCompatAnalysis {
  public:
    /// Discriminator for LLVM-style RTTI (dyn_cast<> et al.)
    enum LoopCompatAnalysisKind {
      LCAK_Basic
    };

    static char ID; // Pass group identification
  private:
    const LoopCompatAnalysisKind Kind;
  protected:
    /// As with alias analysis, all loop compatiblity analysis implementations
    /// other than the default should invoke this directly (using
    /// LoopCompatAnalysis::getAnalysisUsage(AU)).
    virtual void getAnalysisUsage(llvm::AnalysisUsage& AU) const;
  public:
    LoopCompatAnalysisKind getKind() const;

    LoopCompatAnalysis(LoopCompatAnalysisKind K);

    // Need virtual destructor for proper polymorphic behavior
    virtual ~LoopCompatAnalysis();

    /// \brief Returns the list of compatible loops, grouped by function
    ///
    /// MAYBE: reduce to function pass group to reduce excess looping in the
    /// child passes + ExtractLoops (that would prevent interprocedural analysis
    /// of loop compatibility, but such analysis may not be all that useful
    /// post-inlining anyway)
    virtual FuncWithLoopHeadersListType& getExtractibleLoops() = 0;

    /// \brief Returns the set of functions to be converted entirely to kernel
    /// operations.
    ///
    /// As some of the included functions may call other functions in the set
    /// (and in fact those are the only functions they can call), we'll have to
    /// make copies of the functions first, store those in a map, and then
    /// update all callsites in the copied functions.
    virtual FuncsToConvertType& getFunctionsToConvert() = 0;

    /// \brief Returns the set of function declarations that can be added to the
    /// kernel module.
    ///
    /// This is mainly for those math functions with CUDA
    /// implementations/equivalents; will eventually include decls that aren't
    /// exactly the same but can be easily mapped.
    virtual FuncsToConvertType& getCompatFuncDecls() = 0;
  };
}
}

#endif // HYDRA_GPGPU_LOOP_COMPAT_ANALYSIS_H
