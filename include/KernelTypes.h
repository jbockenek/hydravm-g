//===- KernelTypes.h - CUDA kernel info & launch data types -----*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains declarations of various helper structs and classes
/// utilized for kernel characterization and launching.
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_KERNEL_TYPES_H
#define HYDRA_GPGPU_KERNEL_TYPES_H

// forward declarations to avoid excess header file inclusion
namespace llvm {
  class BranchInst;
  class CallInst;
  class Function;
  class Instruction;
  class ScalarEvolution;
  class SCEV;
  class SCEVExpander;
  class LLVMContext;
  class Module;
  class Type;
  class Value;
}

namespace hydra {
namespace gpgpu {
  struct KernelDimsSCEV {
    const llvm::SCEV* GridX;
    const llvm::SCEV* GridY;
    const llvm::SCEV* GridZ;
    const llvm::SCEV* BlockX;
    const llvm::SCEV* BlockY;
    const llvm::SCEV* BlockZ;

    KernelDimsSCEV(llvm::ScalarEvolution& SE);
  };

  class KernelDims {
    // Keeping track of the various context-dependent values here as instances
    // of this class won't last for significant periods of time and holding on
    // to these values as class members will reduce some of the boilerplate
    // needed by instance methods.
    llvm::SCEVExpander& Exp;
    llvm::Instruction* Pos;
    llvm::Module* M;
    llvm::LLVMContext& Context;
    llvm::Type* i32;

    llvm::Value* expandCodeFor(const llvm::SCEV* SH);

    // i1 @kernelDimsInBounds(i32, i32, i32, i32, i32, i32)
    llvm::Function* getBoundsCheckFunc();
  public:
    /// \brief Kernel limits
    ///
    /// Assuming nVidia GPU with CC >= 3.0.
    enum {
      GridXMax  = 2147483647, // 2^31 - 1
      GridYMax  = 65535,
      GridZMax  = 65535,
      BlockXMax = 1024,
      BlockYMax = 1024,
      BlockZMax = 64,
      PerBlockThreadCountMax = 1024
    };

    llvm::Value* GridX;
    llvm::Value* GridY;
    llvm::Value* GridZ;
    llvm::Value* BlockX;
    llvm::Value* BlockY;
    llvm::Value* BlockZ;

    KernelDims(
      const KernelDimsSCEV& Dims,
      llvm::SCEVExpander& Exp,
      llvm::Instruction* Pos
    );

    /// \brief Inserts a bounds check call before the instruction supplied to
    /// the class constructor
    ///
    /// \return the result of the bounds check
    llvm::Value* insertBoundsCheck();
  };

  struct KernelDimsData {
    llvm::Value* GridX;
    llvm::Value* GridY;
    llvm::Value* GridZ;
    llvm::Value* BlockX;
    llvm::Value* BlockY;
    llvm::Value* BlockZ;

    KernelDimsData(const KernelDims& Dims);
  };

  struct KernelMarker {
    llvm::Function* FallbackFunction;
    llvm::Function* Kernel;
    llvm::BranchInst* LaunchLocation;
    llvm::Value* ConditionForLaunch;
    KernelDimsData Dims;
  };
}
}

#endif // HYDRA_GPGPU_KERNEL_TYPES_H
