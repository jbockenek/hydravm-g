//===- BasicAllocConvAnalysis.h - Basic allocation analysis -----*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Basic allocation analysis reports every recognizable allocation in the
/// modules it operates on, dynamic or otherwise, as one that needs conversion
/// to a managed allocation regardless of if it actually does (with the
/// exception of global variables added by GPGPU transform passes).
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_BASIC_ALLOC_CONV_ANALYSIS_H
#define HYDRA_GPGPU_BASIC_ALLOC_CONV_ANALYSIS_H

#include "AllocConvAnalysis.h"

#include "llvm/Pass.h"

#include <vector>

// probably not necessary as they're already in AllocConvAnalysis.h, but jic
namespace llvm {
  class AnalysisUsage;
  class CallSite;
  class Function;
  class Module;
  class raw_ostream;
}

namespace hydra {
namespace gpgpu {
  class BasicACAFunctionPass;

  class BasicAllocConvAnalysis : public llvm::ModulePass, AllocConvAnalysis {
    GlobalVariableListType Globals;
    GlobalVariableListType ConstGlobals;

    /// Keeping track of the last function used so we don't call getAnalysis()
    /// unnecessarily.
    llvm::Function* LastFunction;

    /// Once set, this value should not change as the function pass object stays
    /// the same even when the function supplied to getAnalysis() changes.
    BasicACAFunctionPass* AnalysisResult;

    void updateAnalysisIfNeeded(llvm::Function& NextFunction);
  public:
    static char ID; // Pass identification

    static bool classof(const AllocConvAnalysis* ACA);

    BasicAllocConvAnalysis();

    /// This default does not chain to other analysis passes, but it can be
    /// chained to and in fact should as it provides the basic list of
    /// calls or invokes for other passes to trim down.
    void getAnalysisUsage(llvm::AnalysisUsage& AU) const override;
    void* getAdjustedAnalysisPointer(llvm::AnalysisID PI) override;
    bool runOnModule(llvm::Module& M) override;
    void print(llvm::raw_ostream& O, const llvm::Module* M) const override;

    /// \brief Resets the globals lists.
    ///
    /// This doesn't actually free the memory used, but does allow us to reuse
    /// it.
    void releaseMemory() override;

    AllocaInstListType& getAllocas(llvm::Function& F) override;
    CallSiteListType& getMallocs(llvm::Function& F) override;
    CallSiteListType& getCallocs(llvm::Function& F) override;
    CallSiteListType& getReallocs(llvm::Function& F) override;
    CallSiteListType& getReallocfs(llvm::Function& F) override;
    CallSiteListType& getNews(llvm::Function& F) override;
    CallSiteListType& getNothrowNews(llvm::Function& F) override;
    CallSiteListType& getFrees(llvm::Function& F) override;
    CallSiteListType& getDeletes(llvm::Function& F) override;
    CallSiteListType& getNothrowDeletes(llvm::Function& F) override;
    CallSiteListType& getStrdups(llvm::Function& F) override;
    CallSiteListType& getStrndups(llvm::Function& F) override;
    GlobalVariableListType& getGlobals() override;
    GlobalVariableListType& getConstGlobals() override;
  };
}
}

#endif // HYDRA_GPGPU_BASIC_ALLOC_CONV_ANALYSIS_H
