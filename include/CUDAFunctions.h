//===- CUDAFunctions.h - Getters for CUDA driver API functions --*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains declarations of functions to get declarations for
/// functions in the CUDA driver API (see
/// http://docs.nvidia.com/cuda/cuda-driver-api/index.html). Getters for named
/// (opaque) CUDA structs are also provided for function type clarity. Helpers
/// for functions that aren't actually part of the CUDA driver API but are still
/// necessary for us have also been included.
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_CUDA_FUNCTIONS_H
#define HYDRA_GPGPU_CUDA_FUNCTIONS_H

// forward declarations to avoid excess header file inclusion
namespace llvm {
  class Constant;
  class Function;
  class Module;
  class StructType;
  class Type;
}

namespace hydra {
namespace gpgpu {
  /// \brief declare i32 @cuCtxCreate_v2(%struct.CUctx_st**, i32, i32)
  llvm::Function* getCuCtxCreate(llvm::Module* M);

  /// \brief declare i32 @cuCtxDestroy_v2(%struct.CUctx_st*)
  llvm::Function* getCuCtxDestroy(llvm::Module* M);

  /// \brief declare i32 @cuCtxSynchronize()
  llvm::Function* getCuCtxSynchronize(llvm::Module* M);

  /// \brief declare i32 @cuDeviceComputeCapability(i32*, i32*, i32)
  llvm::Function* getCuDeviceComputeCapability(llvm::Module* M);

  /// \brief declare i32 @cuDeviceGet(i32*, i32)
  llvm::Function* getCuDeviceGet(llvm::Module* M);

  /// \brief declare i32 @cuDeviceGetCount(i32*)
  llvm::Function* getCuDeviceGetCount(llvm::Module* M);

  /// \brief declare i32 @cuDeviceGetName(i8*, i32, i32)
  llvm::Function* getCuDeviceGetName(llvm::Module* M);

  /// \brief declare i32 @cuGetErrorName(i32, i8**)
  llvm::Function* getCuGetErrorName(llvm::Module* M);

  /// \brief declare i32 @cuGetErrorString(i32, i8**)
  llvm::Function* getCuGetErrorString(llvm::Module* M);

  /// \brief declare i32 @cuInit(i32)
  llvm::Function* getCuInit(llvm::Module* M);

  /// \brief declare i32 @cuLaunchKernel(%struct.CUfunc_st*, i32, i32, i32, i32,
  /// i32, i32, i32, %struct.CUstream_st*, i8**, i8**)
  llvm::Function* getCuLaunchKernel(llvm::Module* M);

  /// \brief declare i32 @cuMemAllocManaged(CUdeviceptr*, i64, i32)
  llvm::Function* getCuMemAllocManaged(llvm::Module* M);

  /// \brief declare i32 @cuMemFree_v2(CUdeviceptr)
  llvm::Function* getCuMemFree(llvm::Module* M);

  /// \brief declare i32 @cuModuleGetFunction(%struct.CUfunc_st**,
  /// %struct.CUmod_st*, i8*)
  llvm::Function* getCuModuleGetFunction(llvm::Module* M);

  /// \brief declare i32 @cuModuleLoad(%struct.CUmod_st**, i8*)
  llvm::Function* getCuModuleLoad(llvm::Module* M);

  /// \brief declare i32 @cuModuleUnload(%struct.CUmod_st*)
  llvm::Function* getCuModuleUnload(llvm::Module* M);

  /// \brief declare void @exit(i32) noreturn readnone
  ///
  /// Not technically a CUDA function, but used alongside them. The linker
  /// automatically includes standard library stuff, so as long as we have the
  /// declarations in the program somewhere we don't need to worry about the
  /// implementation.
  llvm::Function* getExit(llvm::Module* M);

  /// \brief declare i64 @strlen(i8*) readonly (size_t == i64 for 64-bit
  /// systems)
  ///
  /// Also not a CUDA function, but used alongside them.
  llvm::Function* getStrLen(llvm::Module* M);

  /// \brief declare i32 @puts(i8*)
  ///
  /// Also not a CUDA function, but used alongside them.
  llvm::Function* getPutS(llvm::Module* M);

  /// \brief declare i32 @putchar(i32)
  ///
  /// Also not a CUDA function, but used alongside them.
  llvm::Function* getPutChar(llvm::Module* M);

  llvm::StructType* getCUctxTy(llvm::Module* M);
  llvm::StructType* getCUfuncTy(llvm::Module* M);
  llvm::StructType* getCUmodTy(llvm::Module* M);
  llvm::StructType* getCUstreamTy(llvm::Module* M);
  llvm::Type* getCUdeviceptrTy(llvm::Module* M);
  llvm::Type* getCUresultTy(llvm::Module* M);
  llvm::Constant* getCudaSuccess(llvm::Module* M);
}
}

#endif // HYDRA_GPGPU_CUDA_FUNCTIONS_H
