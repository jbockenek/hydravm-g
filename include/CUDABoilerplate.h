//===- CUDABoilerplate.h - CUDA driver API boilerplate ----------*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains declarations of functions and data structures for
/// inserting all the setup/teardown boilerplate and kernel launching calls
/// necessary for CUDA execution.
///
/// Note that 64-bit device pointers have type i64.
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_CUDA_BOILERPLATE_H
#define HYDRA_GPGPU_CUDA_BOILERPLATE_H

#include "HydraGPGPUConstants.h"
#include "KernelTypes.h"

#include "llvm/ADT/Twine.h" // for operator+ and default parameter
#include "llvm/IR/IRBuilder.h"

// forward declarations to avoid excess header file inclusion
namespace llvm {
  template<typename T> class ArrayRef;
  class AttributeSet;
  class Function;
  class GlobalVariable;
  class Instruction;
  class Module;
  class StringRef;
  class Type;
  class Value;
}

namespace hydra {
namespace gpgpu {
  /// \brief For keeping track of a thread context and kernel module
  struct GPGPUData {
    llvm::Value* Context;
    llvm::Value* Module;
  };

  class FailureExitInserter {
    /// \brief Returns the error-handling function specified by the class,
    /// creating it if not already existent
    llvm::Function* getErrorHandler();

    void fillErrorHandler(llvm::Function* Handler);
  protected:
    llvm::Module* Mod;
    llvm::IRBuilder<>& Builder;
    llvm::StringRef ErrFunName;

    virtual llvm::Type* getErrorHandlerParamType();
    virtual llvm::AttributeSet getErrorHandlerAttrs();
    virtual llvm::Value* getErrorHandlerCond(llvm::Value* ErrorVal);

    /// \brief Insert extra intructions within the failure block before exit()
    /// is called; does nothing for the base version.
    virtual void preExitHandling(llvm::Value* ErrorVal);

    /// \brief Subclass constructor to specify error function name
    FailureExitInserter(llvm::IRBuilder<>& Builder, llvm::StringRef ErrFunName);
  public:
    FailureExitInserter(llvm::IRBuilder<>& Builder);
    virtual ~FailureExitInserter() {}

    /// \brief Inserts a call to an error-checking function specified by the
    /// class that exits if ErrorVal is non-zero
    void insert(llvm::Value* ErrorVal);
  };

  /// \brief Creates a new, initially-null global variable with internal
  /// linkage. To avoid issues with allocation analysis passes, these new
  /// globals will be added to the metadata for globals to skip.
  ///
  /// Remember that, as with allocas, GVs have an extra layer of indirection on
  /// top of any in the type supplied to the GV constructor.
  ///
  /// \param M    The module to create the global variable in.
  /// \param Ty   The type of the new global variable.
  /// \param Name The name to use for the new global variable.
  llvm::GlobalVariable* createGlobalVariable(
    llvm::Module& M,
    llvm::Type* Ty,
    const llvm::Twine& Name=""
  );

  /// \brief Copies the supplied constant global variable to the kernel module
  ///
  /// This will use addrspace(4), the constant address space for globals in
  /// NVPTX. Such globals are also apparently internal, usually.
  llvm::GlobalVariable* createKernelConstGlobal(
    llvm::Module& KernelModule,
    llvm::GlobalVariable* HostGlobal
  );

  llvm::Function* createKernelFuncDecl(
    llvm::Module& KernelModule,
    const llvm::Function* HostFunction
  );

  /// \brief inserts CUDA error checking for the selected value.
  ///
  /// \param Builder    The inserter to use with the value to check.
  /// \param ValToCheck The value to add an error check for; must be i32.
  void insertCudaCheckErrorCode(
    llvm::IRBuilder<>& Builder,
    llvm::Value* ValToCheck
  );

  /// \brief Inserts all the GPGPU setup code (for CUDA, in this case)
  GPGPUData insertGPGPUSetup(
    llvm::Instruction* InsertionPoint,
    const llvm::Twine& KernelModuleName,
    llvm::ArrayRef<KernelMarker> KernelMarkers,
    bool AddErrorChecking=true
  );

  /// \brief Inserts all the GPGPU teardown code (for CUDA, in this case)
  void insertGPGPUTeardown(
    llvm::Function* Main,   // have to insert before each return in main
    GPGPUData Data,
    bool AddErrorChecking=true
  );
}
}

#endif // HYDRA_GPGPU_CUDA_BOILERPLATE_H
