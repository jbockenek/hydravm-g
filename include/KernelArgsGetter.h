//===- KernelArgsGetter.h - Getter class for kernel args --------*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains the declaration of a class for getting the arguments
/// necessary for a kernel function launch and handling them in the appropriate
/// format (which may involve lowering certain values to allocas so we can have
/// a memory address to supply as the kernel launcher requires all arguments be
/// supplied through pointers; this includes arguments that are already pointers
/// to [host/]device memory).
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_KERNEL_ARGS_GETTER_H
#define HYDRA_GPGPU_KERNEL_ARGS_GETTER_H

#include "llvm/IR/IRBuilder.h"

// forward declarations to avoid excess header file inclusion
namespace llvm {
  class Function;
  class Instruction;
  class Type;
  class Value;
  template <typename T> class ArrayRef;
}

namespace hydra {
namespace gpgpu {
  /// \brief extracts the values in the sole usage of the loop function
  ///
  /// Unfortunately, we need pointers for all of them so we have to trace back
  /// to their origins and lower them as necessary. Somehow.
  class KernelArgsGetter {
    llvm::Function* LoopFunction;
    llvm::IRBuilder<> Builder;
    llvm::Type* VoidPtrTy;

    /// \brief Create alloca for the argument (in entry block) and stores the
    /// argument in the alloca pointer.
    ///
    /// This is fine because any output args will be supplied as pointers anyway
    /// and a copy of a pointer still accesses the data the original pointer
    /// points to.
    llvm::Value* createArgAlloca(llvm::Value* Arg);

    llvm::Value* createArgArray(llvm::ArrayRef<llvm::Value*> Args);
  public:
    KernelArgsGetter(
      llvm::Function* LoopFunction,
      llvm::Instruction* InsertPoint
    );

    llvm::Value* getKernelArgs();
  };
}
}

#endif // HYDRA_GPGPU_KERNEL_ARGS_GETTER_H
