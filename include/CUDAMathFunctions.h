//===- CUDAMathFunctions.h - CUDA math function decl stuff ------*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains declarations for math functions available to CUDA devices
/// and also potentially usable on hosts.
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_CUDA_MATH_FUNCTIONS_H
#define HYDRA_GPGPU_CUDA_MATH_FUNCTIONS_H

#include "llvm/ADT/StringRef.h"

// forward decls to minimize header includes
namespace llvm {
  class FunctionType;
  class LLVMContext;
}

namespace hydra {
namespace gpgpu {
  /// \brief asdf
  ///
  /// asdf
  llvm::FunctionType* getCUDAMathFunTy(
    const llvm::StringRef name,
    llvm::LLVMContext& C
  );
}
}



#endif // HYDRA_GPGPU_CUDA_MATH_FUNCTIONS_H
