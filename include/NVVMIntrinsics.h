//===- NVVMIntrinsics.h - Getters for NVVM Intrinsics -----------*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains declarations for functions that obtain references to the
/// various intrinsics used in NVVM IR. (See
/// http://llvm.org/docs/NVPTXUsage.html#nvptx-intrinsics)
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_NVVM_INTRINSICS_H
#define HYDRA_GPGPU_NVVM_INTRINSICS_H

// forward declarations to avoid excess header file inclusion
namespace llvm {
  class Module;
  class Function;
}

namespace hydra {
namespace gpgpu {
  llvm::Function* getThreadIdXFunc(llvm::Module& M);
  llvm::Function* getThreadIdYFunc(llvm::Module& M);
  llvm::Function* getThreadIdZFunc(llvm::Module& M);

  llvm::Function* getBlockIdXFunc(llvm::Module& M);
  llvm::Function* getBlockIdYFunc(llvm::Module& M);
  llvm::Function* getBlockIdZFunc(llvm::Module& M);

  llvm::Function* getBlockDimXFunc(llvm::Module& M);
  llvm::Function* getBlockDimYFunc(llvm::Module& M);
  llvm::Function* getBlockDimZFunc(llvm::Module& M);

  llvm::Function* getGridDimXFunc(llvm::Module& M);
  llvm::Function* getGridDimYFunc(llvm::Module& M);
  llvm::Function* getGridDimZFunc(llvm::Module& M);

  llvm::Function* getWarpSizeFunc(llvm::Module& M);
}
}

#endif // HYDRA_GPGPU_NVVM_INTRINSICS_H
