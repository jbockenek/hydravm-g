//===- HydraGPGPU.h - Global-ish declarations/etc. --------------*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// [fill]
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_H
#define HYDRA_GPGPU_H

#include "HydraGPGPUConstants.h"

#include "llvm/ADT/Twine.h" // for default parameter
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Value.h"

// forward declarations to avoid excess header file inclusion
namespace llvm {
  class AllocaInst;
  class BasicBlock;
  class Function;
  class Instruction;
  class Type;
}

namespace hydra {
namespace gpgpu {
  /// \brief Creates a new alloca in the right place
  ///
  /// LLVM expects allocas to go in the entry block of a function for proper
  /// stack management and usage of various analyses/transformations.
  ///
  /// \return the result of the alloca
  llvm::AllocaInst* createAllocaInEntryBlock(
    llvm::Function* Func,
    llvm::Type* Ty,
    llvm::Value* ArraySize=nullptr,
    const llvm::Twine& Name=""
  );

  llvm::AllocaInst* createAllocaInEntryBlock(
    llvm::Function* Func,
    llvm::Type* Ty,
    const llvm::Twine& Name
  );

  /// \brief Creates a new alloca in the right place
  ///
  /// LLVM expects allocas to go in the entry block of a function for proper
  /// stack management and usage of various analyses/transformations.
  ///
  /// \return the result of the alloca
  llvm::AllocaInst* createAllocaInEntryBlock(
    llvm::IRBuilder<>& Builder,
    llvm::Type* Ty,
    llvm::Value* ArraySize=nullptr,
    const llvm::Twine& Name=""
  );

  llvm::AllocaInst* createAllocaInEntryBlock(
    llvm::IRBuilder<>& Builder,
    llvm::Type* Ty,
    const llvm::Twine& Name
  );

  /// \brief Creates a new alloca in the right place
  ///
  /// LLVM expects allocas to go in the entry block of a function for proper
  /// stack management and usage of various analyses/transformations.
  ///
  /// \return the result of the alloca
  llvm::AllocaInst* createAllocaInEntryBlock(
    llvm::Instruction* CurrentInstruction,
    llvm::Type* Ty,
    llvm::Value* ArraySize=nullptr,
    const llvm::Twine& Name=""
  );

  llvm::AllocaInst* createAllocaInEntryBlock(
    llvm::Instruction* CurrentInstruction,
    llvm::Type* Ty,
    const llvm::Twine& Name
  );

  /// \brief Gets the first insertion point for non-alloca insts in the
  /// entry block of the specified function
  llvm::Instruction* getFirstNonAllocaInsertionPt(llvm::Function* Func);

  /// \brief Gets the first insertion point for non-alloca insts in the
  /// specified basic block
  llvm::Instruction* getFirstNonAllocaInsertionPt(llvm::BasicBlock* Block);

  /// \brief Gets the first insertion point for non-alloca insts in parent block
  llvm::Instruction* getFirstNonAllocaInsertionPt(llvm::Instruction* Inst);
}
}

#endif // HYDRA_GPGPU_H
