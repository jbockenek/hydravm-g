//===- HydraGPGPUConstants.h - The name says it all -------------*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Moved most of the global constants here for ease of inclusion.
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_CONSTANTS_H
#define HYDRA_GPGPU_CONSTANTS_H

namespace hydra {
namespace gpgpu {
  /// \brief pointer + referenced value consts make more sense with this order
  char const * const HYDRA_FALLBACK_ATTR = "hydra-fallback";

  /// \brief globals to skip
  char const * const MD_GLOBALS = "hydra.gpgpu.globals";

  /// \brief used for instruction metadata to indicate inserted allocas we do
  /// not want to manipulate
  char const * const MD_ALLOCA = "hydra.gpgpu.alloca";

  /// \brief for NVVM metadata (identify kernel functions, etc.)
  char const * const NVVM_META = "nvvm.annotations";

  // From CUDA libraries/etc.
  const unsigned int CUDA_SUCCESS = 0;
  const unsigned int CU_MEM_ATTACH_GLOBAL = 1;
  const unsigned ADDRSPACE_GLOBAL = 4;

  // SmallVector/Set/etc. sizes
  const unsigned BASE_SMALLVECTOR_SIZE = 16;
  const unsigned BASE_SMALLSET_SIZE = 8;
  const unsigned BASE_FUNCS_TO_CONVERT_SIZE = BASE_SMALLSET_SIZE * 4;
  const unsigned MAX_LOOPS = 6;

  /// \brief We require Compute Capability 3.0+
  const int COMPUTE_CAPABILITY_MAJOR_VERSION_MIN = 3;
}
}

#endif // HYDRA_GPGPU_CONSTANTS_H
