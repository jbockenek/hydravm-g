//===- TemplatedUtils.h - Templated utility functions -----------*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// While it would possibly be most efficient in terms of compilation time to
/// give each templated utility function its own header file, for small/basic
/// ones that provide good use grouping them into a single file may be the best
/// solution.
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_TEMPLATED_UTILS_H
#define HYDRA_GPGPU_TEMPLATED_UTILS_H

#include "HydraGPGPUConstants.h"

#include "llvm/ADT/SmallVector.h"
#include "llvm/Analysis/LoopInfo.h"

#include <vector>

namespace llvm {
  class raw_ostream;
}

template<typename T> inline
llvm::raw_ostream& operator<<(llvm::raw_ostream& O, const std::vector<T>& V) {
  for (const T& Element : V) {
    O << Element << '\n';
  }
  return O;
}

namespace hydra {
namespace gpgpu {
  /// \brief Produces a SmallVector of the specified size for the given
  /// top-level loop holding the loop and its child loops if appropriate.
  ///
  /// Using loop class as template parameter to avoid const/non-const issues;
  /// isLoopSimplifyForm() will cause a compilation error for unsupported types
  /// anyway. The type of Loop should be deducible from the one function
  /// argument so we don't need to supply any.
  template<class Loop>
  llvm::SmallVector<Loop*, MAX_LOOPS> getNestedLoopVector(Loop* L) {
    llvm::SmallVector<Loop*, MAX_LOOPS> Loops;

    while (true) { // easiest way to implement this loop
      // sanity checks
      assert(L->isLoopSimplifyForm() && "Loop not in normal form");

      Loops.push_back(L);
      auto& SubLoops = L->getSubLoops();

      if (SubLoops.empty()) {
        break;
      }

      assert(SubLoops.size() == 1 && "Loop must have exactly one child loop");

      L = SubLoops[0];
    }

    assert(Loops.size() <= MAX_LOOPS && "Too many levels of nesting");

    return Loops;
  }
}
}

#endif // HYDRA_GPGPU_TEMPLATED_UTILS_H
