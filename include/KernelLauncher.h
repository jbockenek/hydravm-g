//===- KernelLauncher.h - CUDA kernel launch boilerplate --------*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains declarations for inserting all the kernel launching
/// boilerplate necessary for CUDA execution.
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_KERNEL_LAUNCHER_H
#define HYDRA_GPGPU_KERNEL_LAUNCHER_H

#include "KernelTypes.h"

#include "llvm/ADT/Twine.h" // for operator+ and default parameter
#include "llvm/IR/IRBuilder.h"

// forward declarations to avoid excess header file inclusion
namespace llvm {
  class BranchInst;
  class CallInst;
  class Function;
  class Instruction;
  class Module;
  class StringRef;
  class Value;
}

namespace hydra {
namespace gpgpu {
  /// \brief Encapsulates the functionality for inserting a kernel launch.
  ///
  /// Also provides for calling the fallback function instead when the launch
  /// predicate is false. As the fallback call already exists in the code, we
  /// only need to know what instruction branches to it (as that branch will
  /// reference the fallback call's block).
  class KernelLauncher {
    llvm::Module* M;
    llvm::Function* FallbackFunction;
    llvm::BranchInst* FallbackBranch;
    llvm::Value* KernelFunctionPointer;
    llvm::StringRef KernelFunctionName;
    const KernelDimsData& Dims;
    llvm::IRBuilder<> Builder; // simplifies construction and insertion a little

    // Optional
    llvm::Value* SharedMemorySize;
    llvm::Value* CUstream;
    llvm::Value* Extras;

    llvm::CallInst* launchResultHelper(const llvm::Twine& KernelNameStr);
  public:
    KernelLauncher(
      llvm::Function* FallbackFunction,
      llvm::BranchInst* FallbackBranch,
      llvm::Value* KernelFunctionPointer,
      llvm::StringRef KernelFunctionName,
      const KernelDimsData& Dims
    );

    /// \brief Sets available shared memory for the kernel launch; default value
    /// is none.
    ///
    /// \param SharedMemorySize size in bytes (i32).
    ///
    /// \return the kernel launcher (for chaining).
    KernelLauncher& setSharedMemorySize(llvm::Value* SharedMemorySize);

    /// \brief Sets associated CUDA stream for the kernel launch; default is
    /// the default stream.
    ///
    /// \param CUstream stream to associate (struct.CUstream_st*).
    ///
    /// \return the kernel launcher (for chaining).
    KernelLauncher& setCUstream(llvm::Value* CUstream);

    /// \brief Sets extras for the kernel launch; default is none.
    ///
    /// \param Extras array of extras to supply (i8**).
    ///
    /// \return the kernel launcher (for chaining).
    KernelLauncher& setExtras(llvm::Value* Extras);

    /// \brief Inserts the kernel launch and associated fallback handling.
    ///
    /// \param Predicate the determiner for kernel launch versus fallback
    ///                  execution.
    /// \param NameStr a name for the result of the kernel launch.
    ///
    /// \return the result value for the kernel launch (used with error
    ///         checking).
    llvm::Instruction* insert(
      llvm::Value* Predicate,
      const llvm::Twine& KernelNameStr=""
    );
  };
}
}

#endif // HYDRA_GPGPU_KERNEL_LAUNCHER_H

