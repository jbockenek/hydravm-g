//===- ConstantExprConverter.h - Converts ConstantExpr to Inst --*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_CONSTANT_EXPR_CONVERTER_H
#define HYDRA_GPGPU_CONSTANT_EXPR_CONVERTER_H

#include "HydraGPGPUConstants.h"
#include "Utils/UniquedContainer.h"

#include "llvm/ADT/SmallPtrSet.h"

#include <stack>

namespace llvm {
  class ConstantExpr;
  class User;
  class Value;
}

namespace hydra {
namespace gpgpu {
  /// \brief Converts ConstantExprs to Instructions
  ///
  /// As a chain of ConstantExprs can form a graph rather than a tree, it seemed
  /// that depth-first recursion to convert all of them to instructions was not
  /// a good option and that maintaining a container go through, iteratively
  /// replacing those that are only used by instructions, was the appropriate
  /// choice. However, it turns out that, while the graph should be a directed
  /// acyclic graph, certain transformation passes can induce cycles; generally,
  /// this only happens in code that has become dead, but as we do not have a
  /// simple way to check for dead code and ignore it, we instead need to break
  /// those back edges, and the simplest way of doing so is by a DFS combined
  /// with checking next users against those in the current search path and then
  /// undeffing the use. Once a leaf node is reached (a ConstantExpr with no
  /// other ConstantExprs as users), we can replace it with however many
  /// Instructions are required (as ConstantExprs are global while Instructions
  /// have to live within Basic Blocks).
  ///
  /// Note that this should not be used with Values that are used by Constants
  /// that are NOT ConstantExprs, whatever those may be.
  class ConstantExprConverter {
    UniquedContainer<
      std::stack<llvm::ConstantExpr*>,
      llvm::SmallPtrSet<llvm::ConstantExpr*, BASE_SMALLSET_SIZE>
    > Exprs;

    llvm::SmallPtrSet<llvm::ConstantExpr*, BASE_SMALLSET_SIZE> EliminatedExprs;
    llvm::SmallPtrSet<llvm::User*, BASE_SMALLSET_SIZE> UniqueUsers;

    /// \brief adds using ConstantExprs to the stack
    ///
    /// \return true if Exprs were added, false otherwise
    bool addExprsToStack(llvm::Value* Val);
    void replaceUses(llvm::ConstantExpr* CE);

    /// \brief resets the helper objects
    void reset();
  public:
    void convertUsers(llvm::Value* RootValue);
  };
}
}

#endif // HYDRA_GPGPU_CONSTANT_EXPR_CONVERTER_H
