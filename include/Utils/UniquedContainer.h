//===- UniquedContainer.h - Wrapper to unique container items ---*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Provides a class very similar to LLVM's SetVector, but with some subtle
/// differences, the main one being that SetVector supports iteration over its
/// elements and assumes the order of elements is the order of insertion.
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_UNIQUED_CONTAINER_H
#define HYDRA_GPGPU_UNIQUED_CONTAINER_H

#include <type_traits>

namespace hydra {
namespace gpgpu {
  /// \brief A wrapper for queues, stacks, etc. that ensures only one instance
  /// of an item exists in the underlying container at any time
  ///
  /// This class requires that \c Container and \c Set are default-constructible
  /// and have compatible value types, that \c Container provides \c push(),
  /// \c pop(), either \c top() or \c front(), \c empty(), and \c size(), and
  /// that \c Set provides \c insert() and \c erase(). Due to the fact that
  /// inserted items are stored in both container and set, this class template
  /// is not usable when \c value_type is not trivially-copiable (though the
  /// error message is a bit misleading and indicates primary failure here
  /// rather than at the point of declaration, clang at least includes the point
  /// of failed declaration in the error message as well).
  ///
  /// A "clear" operation is supported, but may be inefficient for some types
  /// that lack a clear of their own. The alternative method also only works
  /// when the type supports \c pop() instead. (This will also fail if the
  /// container has both \c clear() and \c pop().)
  template<typename Container, typename Set, typename = typename std::enable_if<
    std::is_trivially_copyable<typename Container::value_type>::value
  >::type>
  class UniquedContainer {
    using reference = typename Container::reference;
    using const_reference = typename Container::const_reference;
    using value_type = typename Container::value_type;
    using size_type = typename Container::size_type;

    Container C;
    Set S;

    /// \brief wrapper for std::stack/priority_queue-style top-element reporting
    ///
    /// The extra template parameter is needed to take advantage of SFINAE.
    template<typename T>
    auto element_to_pop(T& C) -> decltype(C.top()) {
      return C.top();
    }

    /// \brief wrapper for std::stack/priority_queue-style top-element reporting
    template<typename T>
    auto element_to_pop(const T& C) const -> decltype(C.top()) {
      return C.top();
    }

    /// \brief wrapper for std::queue-style top-element reporting
    template<typename T>
    auto element_to_pop(T& C) -> decltype(C.front()) {
      return C.front();
    }

    /// \brief wrapper for std::queue-style top-element reporting
    template<typename T>
    auto element_to_pop(const T& C) const -> decltype(C.front()) {
      return C.front();
    }

    /// \brief clear for containers with \c clear()
    template<typename T>
    auto clear_container(T& C) -> decltype(C.clear()) {
      C.clear();
    }

    /// \brief clear for containers without \c clear() but with \c pop()
    template<typename T>
    auto clear_container(T& C) -> decltype(C.pop()) {
      while (!C.empty())
        C.pop();
    }

  public:
    bool empty() const {
      return C.empty();
    }

    size_type size() const {
      return C.size();
    }

    bool contains(const_reference Item) const {
      return S.count(Item) > 0;
    }

    /// \brief Clears the contents of the internal containers
    ///
    /// Does not explicitly perform any shrinking as the anticipation is that
    /// the memory will be reused anyway (though some may happen depending on
    /// the underlying types). Otherwise, just move-assign with a
    /// default-constructed object. (The compiler generates that all for us,
    /// too!)
    void clear() {
      S.clear();
      clear_container(C);
    }

    /// \brief next element to be popped
    ///
    /// \return a copy of the top value; we cannot allow modification in place
    ///         due to the usage of multiple containers internally.
    value_type top() const {
      return element_to_pop(C);
    }

    /// \brief inserts an item into the container if not already present
    ///
    /// \return \c true if inserted, \c false if not
    bool push(value_type Item) {
      if (S.count(Item))
        return false;

      C.push(Item);
      S.insert(Item);
      return true;
    }

    /// \brief removes the top item from the container
    ///
    /// To mimic the behavior of \c std::deque, calling this on an empty
    /// container is undefined.
    void pop() {
      reference Item = element_to_pop(C);
      C.pop();
      S.erase(Item);
    }
  };
}
}

#endif // HYDRA_GPGPU_UNIQUED_CONTAINER_H
