//===- BasicACAFunctionPass.h - Function pass for Basic ACA -----*- C++ -*-===//
//
//                     HydraVM GPGPU Work
//
// [license?]
//
//===----------------------------------------------------------------------===//
///
/// \file
/// As Basic ACA does no interprocedural analysis, we only need a module pass to
/// process globals; a function pass works just fine for stack and dynamic
/// allocations.
///
/// Technically, as the basic analysis is flow- and context-insensitive, we
/// could do it on the basic block level, but that's a little too fine-grained.
///
/// Also, be careful that you don't intermix gets for different functions, the
/// pass manager reuses the same pass object when a module queries a function
/// pass (this also means you should store a pointer or reference to the result
/// of the analysis for each operation on a function rather than calling
/// getAnalysis() multiple times for the same function to avoid performing
/// needless work). The new pass manager apparently handles that situation more
/// optimally, but as there does not seem to be very good documentation on it
/// we're sticking with the legacy one for now.
///
//===----------------------------------------------------------------------===//

#ifndef HYDRA_GPGPU_BASIC_ACA_FUNCTION_PASS_H
#define HYDRA_GPGPU_BASIC_ACA_FUNCTION_PASS_H

#include "AllocConvAnalysis.h"

#include "llvm/Pass.h"

namespace llvm {
  class raw_ostream;
}

namespace hydra {
namespace gpgpu {
  class BasicACAFunctionPass : public llvm::FunctionPass {
    AllocaInstListType Allocas;
    CallSiteListType Mallocs;
    CallSiteListType Callocs;
    CallSiteListType Reallocs;
    CallSiteListType Reallocfs;
    CallSiteListType News;
    CallSiteListType NothrowNews;
    CallSiteListType Frees;
    CallSiteListType Deletes;
    CallSiteListType NothrowDeletes;
    CallSiteListType Strdups;
    CallSiteListType Strndups;
  public:
    static char ID; // Pass identification

    BasicACAFunctionPass();

    void getAnalysisUsage(llvm::AnalysisUsage& AU) const override;
    bool runOnFunction(llvm::Function& F) override;
    void print(llvm::raw_ostream& O, const llvm::Module* M) const override;

    /// \brief Resets the lists.
    ///
    /// This doesn't actually free the memory used, but does allow us to reuse
    /// it.
    void releaseMemory() override;

    AllocaInstListType& getAllocas();
    CallSiteListType& getMallocs();
    CallSiteListType& getCallocs();
    CallSiteListType& getReallocs();
    CallSiteListType& getReallocfs();
    CallSiteListType& getNews();
    CallSiteListType& getNothrowNews();
    CallSiteListType& getFrees();
    CallSiteListType& getDeletes();
    CallSiteListType& getNothrowDeletes();
    CallSiteListType& getStrdups();
    CallSiteListType& getStrndups();
  };
}
}

#endif // HYDRA_GPGPU_BASIC_ACA_FUNCTION_PASS_H
